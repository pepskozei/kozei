/*
* File:   ParametresTest.cpp
* Author: gononl
*
* Created on March 12, 2017, 1:01 PM
*/

#include "gtest/gtest.h"

#include "ParametresTest.hpp"
#include "Simulation.hpp"
#include "EstimationParam.hpp"
#include "Kozei.hpp"
#include "Portefeuille.hpp"
#include "RetrieveData.hpp"

ParametresTest::ParametresTest() {}

TEST(DISABLED_ParametresTest, test_volatilite)
{
	int nbTimeSteps = 252;
	int size = 3;
	double maturity = 1;
	double r = 0.1;
	double rho = 0.3;
	int nbSamples = 5;
	PnlVect *spot = pnl_vect_create_from_scalar(size, 100);
	PnlVect *sigma = pnl_vect_create_from_scalar(size, 0.67);
	PnlVect * sigma_taux = pnl_vect_create_from_scalar(size, 0);

	Simulation *sim = new Simulation(nbSamples, nbTimeSteps,
		size, maturity, r, rho, spot, sigma, sigma_taux);

	PnlMat *path = pnl_mat_create(nbTimeSteps + 1, size);
	sim->asset(path);
	
	// Transformation PnlMat en vector 
	vector<vector<DataStructure>*> data = vector<vector<DataStructure>*>();
	for (int j = 0; j < path->n; j++)
	{
		vector<DataStructure>* dataAsset = new vector<DataStructure>();
		for (int i = 0; i < path->m; i++)
		{
			DataStructure dataStruct = DataStructure(0, 0, 0, MGET(path, i, j), "", Currency::EUR);
			dataAsset->push_back(dataStruct);
		}
		data.push_back(dataAsset);
	}

	EstimationParam* estim = new EstimationParam();

	PnlVect* volatilite = pnl_vect_new();
	PnlMat* matriceCov = pnl_mat_new();
	
	int windowSize = 150;
	int nbDays = (int)(nbTimeSteps/maturity);
	
	estim->getCovMatrix(matriceCov, data, windowSize, nbDays);
	estim->getVolatility(volatilite, matriceCov);

	std::cout << "VOLATILITY : " << std::endl;
	for (int i = 0; i < volatilite->size; i++) {
		std::cout << GET(volatilite, i) << std::endl;
	}

	EXPECT_EQ(1, 1);

	pnl_vect_free(&volatilite);
	pnl_mat_free(&matriceCov);
}



TEST(DISABLED_ParametresTest, test_correlation_matrix) {
	int nbTimeSteps = 1000;
	int size = 3;
	double maturity = 3;
	double r =0.1;
	double rho = 0.35;
	int nbSamples = 5;
	PnlVect *spot = pnl_vect_create_from_scalar(size, 100);
	PnlVect *sigma = pnl_vect_create_from_scalar(size, 0.3);
	PnlVect * sigma_taux = pnl_vect_create_from_scalar(size, 0);

	Simulation *sim = new Simulation(nbSamples, nbTimeSteps,
		size, maturity, r, rho, spot, sigma, sigma_taux);

	PnlMat *path = pnl_mat_create(nbTimeSteps + 1, size);
	sim->asset(path);
	
	cout << "=========================" << endl;
	
	// Transformation PnlMat en vector 
	vector<vector<DataStructure>*> data = vector<vector<DataStructure>*>();
	for (int j = 0; j < path->n; j++) {
		vector<DataStructure>* dataAsset = new vector<DataStructure>();
		for (int i = 0; i < path->m; i++) {
			DataStructure dataStruct = DataStructure(0, 0, 0, MGET(path, i, j), "", Currency::EUR);
			dataAsset->push_back(dataStruct);
		}
		data.push_back(dataAsset);
	}

	EstimationParam* estim = new EstimationParam();
	PnlMat* matrixCov = pnl_mat_new();
	PnlMat* matrixCorr = pnl_mat_new();
	
	int windowSize = 400;
	int nbDays = (int)(nbTimeSteps / maturity);

	estim->getCovMatrix(matrixCov, data, windowSize, nbDays);
	estim->getCorrelationMatrix(matrixCorr, matrixCov);

	cout << "============= MATRICE DE CORRELATION ===============" <<endl;
	for (int i = 0; i < data.size(); i++) {
		for (int j = 0; j < data.size(); j++) {

			cout <<"["<< MGET(matrixCorr, i, j) <<"]    ";

		}
		cout << endl;
	}
	
	EXPECT_EQ(1, 1);

	pnl_mat_free(&matrixCov);
	pnl_mat_free(&matrixCorr);
}

TEST(DISABLED_Parametres_Test, backtest)
{
	// Objet pour stocker les donn�es historiques
	vector<vector<DataStructure>*> spots = vector<vector<DataStructure>*>();
	RetrieveData* retrieveD = new RetrieveData();
	retrieveD->getData(spots);

	EstimationParam* estim = new EstimationParam();
	PnlMat* matrixCov = pnl_mat_new();
	PnlMat* matrixCorr = pnl_mat_new();
	PnlVect* volatilite = pnl_vect_new();

	estim->getCovMatrix(matrixCov, spots, 200, 252);
	estim->getVolatility(volatilite, matrixCov);
	estim->getCorrelationMatrix(matrixCorr, matrixCov);

	cout << "VOLATILITY DATA HISTORIQUE : " <<endl;

	for (int i = 0; i < volatilite->size; i++) {
		std::cout << GET(volatilite, i) << std::endl;
	}
	
	cout << "Matrice correlation data historique " << endl;
	for (int i = 0; i < spots.size(); i++) {
		for (int j = 0; j < spots.size(); j++) {

			cout << "[" << MGET(matrixCorr, i, j) << "]\t";

		}
		cout << endl;
	}
	EXPECT_EQ(1, 1);
	
	pnl_vect_free(&volatilite);
	pnl_mat_free(&matrixCov);
	pnl_mat_free(&matrixCorr);
}