/*
 * File:   PriceDistribution_test.cpp
 * Author: gononl
 *
 * Created on January 11, 2017, 5:23 PM
 */

#include "PriceDistribution_test.hpp"

PriceDistribution_test::PriceDistribution_test() {}

TEST(DISABLED_PriceDistributionTest, gaussianDistribution)
{
    // Paramètres du marché
    int nbTimeSteps = 16;
    int size = 30;
    double maturity = 8;
    double r = 0.04879;
    double rho = 0.3;
    int nbSamples = 100;
    PnlVect *spot = pnl_vect_create_from_scalar(size, 100);
    PnlVect *sigma = pnl_vect_create_from_scalar(size, 0.2);

    Simulation *data = new Simulation(nbSamples, nbTimeSteps,
                            size, maturity, r, rho, spot, sigma);
    
    double fdStep = 0.01;
    
    double initialInvestment = 100;
    Kozei *kozei = new Kozei(data, initialInvestment);
    
    int nbTimeStepsH = 64;
    Portefeuille *pf = new Portefeuille(kozei, nbTimeStepsH, fdStep);
    
    PnlVect *price = pnl_vect_create(pf->nbTimeStepH + 1);
    PnlVect *ic_0 = pnl_vect_create(pf->nbTimeStepH + 1);
    int nbCalculPrice = 500;
    double erreur_couverture = 0;
	
    // Ouverture du fichier csv
    ofstream myfile;
    myfile.open ("priceDistribution.csv");
	
    for (int i = 0; i < nbCalculPrice; i++)
	{
        pf->calcul_pf(erreur_couverture, price, ic_0);
        myfile << pnl_vect_get(price, 0) << endl;
    }    
	
    myfile.close();
    
    pnl_vect_free(&price);
    pnl_vect_free(&spot);
    pnl_vect_free(&sigma);
    ASSERT_EQ(1,1);
}