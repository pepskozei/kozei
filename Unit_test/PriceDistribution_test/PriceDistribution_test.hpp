/* 
 * File:   PriceDistribution_test.hpp
 * Author: gononl
 *
 * Created on January 18, 2017, 11:54 AM
 */

#ifndef PRICEDISTRIBUTION_TEST_HPP
#define	PRICEDISTRIBUTION_TEST_HPP

#include "gtest/gtest.h"

#include <iostream>
#include <fstream>

#include "Portefeuille.hpp"
#include "Simulation.hpp"
#include "Kozei.hpp"

class PriceDistribution_test : public testing::Test
{
public :
    PriceDistribution_test();
private :
};

#endif	/* PRICEDISTRIBUTION_TEST_HPP */

