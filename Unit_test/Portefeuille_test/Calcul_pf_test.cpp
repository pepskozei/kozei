/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   Calcul_pf_test.cpp
 * Author: paviotch
 *
 * Created on January 11, 2017, 5:23 PM
 */

#include "Calcul_pf_test.hpp"

Calcul_pf_test::Calcul_pf_test() {}

TEST(DISABLED_PortefeuilleTest, calcul_pf_basket)
{
    int nbTimeSteps = 1;
    int nbSamples = 5000;
    double fdStep = 0.1;
    int nbTimeStepH = 104;
    int size = 10;
    double strike = 100;
    double maturity = 2;
    double r = 0.04879;
    double rho = 0.3;
    double lambda = 0.1;
    const PnlVect *spot = pnl_vect_create_from_scalar(size, 100);
    const PnlVect *sigma = pnl_vect_create_from_scalar(size, 0.2);
	PnlVect * sigma_taux = pnl_vect_create_from_scalar(size, 0);

    Data *data = new Simulation(nbSamples, nbTimeSteps, size, maturity, r, rho, spot, sigma, sigma_taux);
    Basket *basket = new Basket(data, 0, strike, lambda);
    Portefeuille *pf = new Portefeuille(basket, nbTimeStepH, fdStep);
    double erreur_couverture;
    PnlVect *price = pnl_vect_create(pf->nbTimeStepH + 1);
    PnlVect *ic = pnl_vect_create(pf->nbTimeStepH + 1);
    pf->calcul_pf(erreur_couverture, price, ic);

    ASSERT_GT(pnl_vect_get(price, 0), 12.21 - pnl_vect_get(ic, 0));
    ASSERT_LT(pnl_vect_get(price, 0), 12.21 + pnl_vect_get(ic, 0));

    pnl_vect_free(&price);
}

TEST(DISABLED_PortefeuilleTest, calcul_pf_asiatique)
{
    int nbTimeSteps = 26;
    int nbSamples = 5000;
    double fdstep = 0.1;
    int nbTimeStepH = 104;
    int size = 1;
    double strike = 100;
    double maturity = 2;
    double r = 0.03;
    double rho = 0.3;
    double lambda = 1;
    const PnlVect *spot = pnl_vect_create_from_scalar(size, 100);
    const PnlVect *sigma = pnl_vect_create_from_scalar(size, 0.2);
	PnlVect * sigma_taux = pnl_vect_create_from_scalar(size, 0);

    Data *data = new Simulation(nbSamples, nbTimeSteps, size, maturity, r, rho, spot, sigma, sigma_taux);
    Asiatique *asian = new Asiatique(data, 0, strike, lambda);
    Portefeuille *pf = new Portefeuille(asian, nbTimeStepH, fdstep);

    double erreur_couverture;
    PnlVect *price = pnl_vect_create(pf->nbTimeStepH + 1);
    PnlVect *ic = pnl_vect_create(pf->nbTimeStepH + 1);
    pf->calcul_pf(erreur_couverture, price, ic);
	
    ASSERT_GT(pnl_vect_get(price, 0), 7.67972 - pnl_vect_get(ic, 0));
    ASSERT_LT(pnl_vect_get(price, 0), 7.67972 + pnl_vect_get(ic, 0));
	
    pnl_vect_free(&price);
}

TEST(DISABLED_PortefeuilleTest, calcul_pf_call)
{
    int nbTimeSteps = 1;
    int nbSamples = 5000;
    double fdstep = 0.1;
    int nbTimeStepH = 104;
    int size = 1;
    double strike = 100;
    double maturity = 2;
    double r = 0.05;
    double rho = 0.0;
    double lambda = 1;
    PnlVect *spot = pnl_vect_create_from_scalar(size, 100);
    PnlVect *sigma = pnl_vect_create_from_scalar(size, 0.2);
	PnlVect * sigma_taux = pnl_vect_create_from_scalar(size, 0);

    Data *data = new Simulation(nbSamples, nbTimeSteps, size, maturity, r, rho, spot, sigma, sigma_taux);

    Call *call = new Call(data, 0, strike);
    Portefeuille *pf = new Portefeuille(call, nbTimeStepH, fdstep);
    double erreur_couverture;
    PnlVect *price = pnl_vect_create(pf->nbTimeStepH + 1);
    PnlVect *ic = pnl_vect_create(pf->nbTimeStepH + 1);
    pf->calcul_pf(erreur_couverture, price, ic);
	
    ASSERT_GT(pnl_vect_get(price, 0), 16.1905 - pnl_vect_get(ic, 0));
    ASSERT_LT(pnl_vect_get(price, 0), 16.1905 + pnl_vect_get(ic, 0));
	
    pnl_vect_free(&price);
    pnl_vect_free(&spot);
    pnl_vect_free(&sigma);
}


TEST(DISABLED_PortefeuilleTest, comparaison_tendance_prix_theorique_val_pf_1)
{
    int nbTimeSteps = 16;
    int nbSamples = 5000;
    double fdstep = 0.1;
    int nbTimeStepH = 64;
    int size = 30;
    double I = 100;
    double maturity = 8;
    double r = 0.05;
    double rho = 0.3;
    const PnlVect *spot = pnl_vect_create_from_scalar(size, 100);
    const PnlVect *sigma = pnl_vect_create_from_scalar(size, 0.2);
	PnlVect * sigma_taux = pnl_vect_create_from_scalar(size, 0);

    Data *data = new Simulation(nbSamples, nbTimeSteps, size, maturity, r, rho, spot, sigma, sigma_taux);
    Kozei *kozei = new Kozei(data, I);
    Portefeuille *pf = new Portefeuille(kozei, nbTimeStepH, fdstep);
    double erreur_couverture;
    PnlVect *price = pnl_vect_create(pf->nbTimeStepH + 1);
    PnlVect *ic = pnl_vect_create(pf->nbTimeStepH + 1);
    pf->calcul_pf(erreur_couverture, price, ic);
}

TEST(DISABLED_PortefeuilleTest, comparaison_tendance_prix_theorique_val_pf_2)
{
    int nbTimeSteps = 16;
    int nbSamples = 5000;
    double fdstep = 0.1;
    int nbTimeStepH = 256;
    int size = 30;
    double I = 100;
    double maturity = 8;
    double r = 0.05;
    double rho = 0.3;
    const PnlVect *spot = pnl_vect_create_from_scalar(size, 100);
    const PnlVect *sigma = pnl_vect_create_from_scalar(size, 0.2);
	PnlVect * sigma_taux = pnl_vect_create_from_scalar(size, 0);

    Data *data = new Simulation(nbSamples, nbTimeSteps, size, maturity, r, rho, spot, sigma, sigma_taux);
    Kozei *kozei = new Kozei(data, I);
    Portefeuille *pf = new Portefeuille(kozei, nbTimeStepH, fdstep);
    PnlVect *price = pnl_vect_create(pf->nbTimeStepH + 1);
    PnlVect *ic = pnl_vect_create(pf->nbTimeStepH + 1);
    double erreur_couverture;
	
    pf->calcul_pf(erreur_couverture, ic, price);
}

TEST(DISABLED_PortefeuilleTest, comparaison_tendance_prix_theorique_val_pf_3) {
    int nbTimeSteps = 16;
    int nbSamples = 5000;
    double fdstep = 0.01;
    int nbTimeStepH = 64;
    int size = 30;
    double I = 100;
    double maturity = 8;
    double r = 0.05;
    double rho = 0.3;
    const PnlVect *spot = pnl_vect_create_from_scalar(size, 100);
    const PnlVect *sigma = pnl_vect_create_from_scalar(size, 0.2);
	PnlVect * sigma_taux = pnl_vect_create_from_scalar(size, 0);

    Data *data = new Simulation(nbSamples, nbTimeSteps, size, maturity, r, rho, spot, sigma, sigma_taux);
    Kozei *kozei = new Kozei(data, I);
    Portefeuille *pf = new Portefeuille(kozei, nbTimeStepH, fdstep);
    PnlVect *price = pnl_vect_create(pf->nbTimeStepH + 1);
    PnlVect *ic = pnl_vect_create(pf->nbTimeStepH + 1);
    double erreur_couverture;
	
    pf->calcul_pf(erreur_couverture, ic, price);
}

TEST(DISABLED_PortefeuilleTest, comparaison_tendance_prix_theorique_val_pf_4)
{
    int nbTimeSteps = 16;
    int nbSamples = 5000;
    double fdstep = 0.1;
    int nbTimeStepH = 64;
    int size = 30;
    double I = 100;
    double maturity = 8;
    double r = 0.05;
    double rho = 0.5;
    const PnlVect *spot = pnl_vect_create_from_scalar(size, 100);
    const PnlVect *sigma = pnl_vect_create_from_scalar(size, 0.2);
	PnlVect * sigma_taux = pnl_vect_create_from_scalar(size, 0);

    Data *data = new Simulation(nbSamples, nbTimeSteps, size, maturity, r, rho, spot, sigma, sigma_taux);
    Kozei *kozei = new Kozei(data, I);
    Portefeuille *pf = new Portefeuille(kozei, nbTimeStepH, fdstep);
    PnlVect *price = pnl_vect_create(pf->nbTimeStepH + 1);
    PnlVect *ic = pnl_vect_create(pf->nbTimeStepH + 1);
    double erreur_couverture;
	
    pf->calcul_pf(erreur_couverture, ic, price);
}

TEST(DISABLED_PortefeuilleTest, comparaison_tendance_prix_theorique_val_pf_5)
{
    int nbTimeSteps = 16;
    int nbSamples = 5000;
    double fdstep = 0.1;
    int nbTimeStepH = 64;
    int size = 30;
    double I = 100;
    double maturity = 8;
    double r = 0.05;
    double rho = 0.3;
    const PnlVect *spot = pnl_vect_create_from_scalar(size, 100);
    const PnlVect *sigma = pnl_vect_create_from_scalar(size, 0.4);
	PnlVect * sigma_taux = pnl_vect_create_from_scalar(size, 0);

    Data *data = new Simulation(nbSamples, nbTimeSteps, size, maturity, r, rho, spot, sigma, sigma_taux);
    Kozei *kozei = new Kozei(data, I);
    Portefeuille *pf = new Portefeuille(kozei, nbTimeStepH, fdstep);
    PnlVect *price = pnl_vect_create(pf->nbTimeStepH + 1);
    PnlVect *ic = pnl_vect_create(pf->nbTimeStepH + 1);
    double erreur_couverture;
	
    pf->calcul_pf(erreur_couverture, ic, price);
}

TEST(DISABLED_PortefeuilleTest, fonction_rep_N_0_1)
{
    int nbTimeSteps = 26;
    int nbSamples = 500;
    int size = 1;
    double strike = 100;
    double maturity = 2;
    double r = 0.05;
    double rho = 0.0;
    const PnlVect *spot = pnl_vect_create_from_scalar(size, 100);
    const PnlVect *sigma = pnl_vect_create_from_scalar(size, 0.2);
	PnlVect * sigma_taux = pnl_vect_create_from_scalar(size, 0);

    Data *data = new Simulation(nbSamples, nbTimeSteps, size, maturity, r, rho, spot, sigma, sigma_taux);
    Call *c = new Call(data, 0, strike);

    ASSERT_GT(c->N(1.24), 0.8925 - 0.0001);
    ASSERT_LT(c->N(1.24), 0.8925 + 0.0001);

    ASSERT_GT(c->N(1.5), 0.9332 - 0.0001);
    ASSERT_LT(c->N(1.5), 0.9332 + 0.0001);

    ASSERT_GT(c->N(1.92), 0.9726 - 0.0001);
    ASSERT_LT(c->N(1.92), 0.9726 + 0.0001);

    ASSERT_GT(c->N(3.06), 0.99889 - 0.0001);
    ASSERT_LT(c->N(3.06), 0.99889 + 0.0001);
}

TEST(DISABLED_PortefeuilleTest, prix_call_theorique)
{

    /*Pour les calculs theoriques des prix d'option : http://www.iotafinance.com/Calculateur-Options-Black-Scholes.html*/
    /*Comme on a C(t,T,K,St)=C(0,T-t,K,St) il sufit de tester en 0*/
    /*Test pour des prix en t = 0*/

    double nbTimeSteps = 26, nbSamples = 500, size = 1, strike = 100, maturity = 2, r = 0.05, rho = 0.0;
    const PnlVect *spot1 = pnl_vect_create_from_scalar(size, 100);
    const PnlVect *sigma1 = pnl_vect_create_from_scalar(size, 0.2);
	PnlVect * sigma_taux = pnl_vect_create_from_scalar(size, 0);

    Data *data1 = new Simulation(nbSamples, nbTimeSteps, size, maturity, r, rho, spot1, sigma1, sigma_taux);
    Call *c1 = new Call(data1, 0, strike);
    ASSERT_GT(c1->PriceCall(0.0, pnl_vect_get(spot1, 0)), 16.12678 - 0.00001);
    ASSERT_LT(c1->PriceCall(0.0, pnl_vect_get(spot1, 0)), 16.12678 + 0.00001);

    nbTimeSteps = 26;
    nbSamples = 500;
    size = 1;
    strike = 80.0;
    maturity = 1;
    r = 0.1;
    rho = 0.0;
    const PnlVect *spot2 = pnl_vect_create_from_scalar(size, 110);
    const PnlVect *sigma2 = pnl_vect_create_from_scalar(size, 0.3);

    Data *data2 = new Simulation(nbSamples, nbTimeSteps, size, maturity, r, rho, spot2, sigma2, sigma_taux);
    Call *c2 = new Call(data2, 0, strike);
    ASSERT_GT(c2->PriceCall(0.0, pnl_vect_get(spot2, 0)), 38.59795 - 0.00001);
    ASSERT_LT(c2->PriceCall(0.0, pnl_vect_get(spot2, 0)), 38.59795 + 0.00001);

    /*Test pour des prix en t = t ou t*/
    //t=0.5  (on est sur une maturité de 2 )
    nbTimeSteps = 26;
    nbSamples = 500;
    size = 1;
    strike = 80.0;
    maturity = 2;
    r = 0.1;
    rho = 0.0;
    const PnlVect *spot3 = pnl_vect_create_from_scalar(size, 110);
    const PnlVect *sigma3 = pnl_vect_create_from_scalar(size, 0.3);

    Data *data3 = new Simulation(nbSamples, nbTimeSteps, size, maturity, r, rho, spot3, sigma3, sigma_taux);
    Call *c3 = new Call(data3, 0, strike);
    ASSERT_GT(c3->PriceCall(0.5, 115), 47.37037 - 0.1);
    ASSERT_LT(c3->PriceCall(0.5, 115), 47.37037 + 0.1);

    //t= 1.6 (on est sur une maturité de 2 )
    nbTimeSteps = 26;
    nbSamples = 500;
    size = 1;
    strike = 80.0;
    maturity = 2;
    r = 0.1;
    rho = 0.0;
    const PnlVect *spot4 = pnl_vect_create_from_scalar(size, 110);
    const PnlVect *sigma4 = pnl_vect_create_from_scalar(size, 0.3);

    Data *data4 = new Simulation(nbSamples, nbTimeSteps, size, maturity, r, rho, spot4, sigma4, sigma_taux);
    Call *c4 = new Call(data3, 0, strike);
    ASSERT_GT(c4->PriceCall(1.6, 123), 46.19832 - 0.1);
    ASSERT_LT(c4->PriceCall(1.6, 123), 46.19832 + 0.1);
}


TEST(DISABLED_PortefeuilleTest, comparaison_prix_theorique_monteCarlo)
{
    int nbTimeSteps = 26;
    int nbSamples = 100000;
    double fdstep = 0.1;
    int nbTimeStepH = 104;
    int size = 1;
    double strike = 100;
    double maturity = 2;
    double r = 0.05;
    double rho = 0.0;
    double lambda = 1;
    const PnlVect *spot = pnl_vect_create_from_scalar(size, 100);
    const PnlVect *sigma = pnl_vect_create_from_scalar(size, 0.2);
	PnlVect * sigma_taux = pnl_vect_create_from_scalar(size, 0);

    Data *data = new Simulation(nbSamples, nbTimeSteps, size, maturity, r, rho, spot, sigma, sigma_taux);
    Call *call = new Call(data, 0, strike);
    Portefeuille *pf = new Portefeuille(call, nbTimeStepH, fdstep);

    double prix_theorique = call->PriceCall(0.0, pnl_vect_get(spot, 0));
    double prix_monteCarlo(0.0), ic(0.0);
    double sum = 0.0;
    int nbTirage = 2000;

    for (int i = 0; i < nbTirage; i++)
	{
        call->price(prix_monteCarlo, ic);
        sum += prix_monteCarlo;
    }

    ASSERT_EQ(1,1);
}
