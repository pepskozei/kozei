/*
* File:   SimulationUnitTest.cpp
* Author: gononl
*
* Created on January 9, 2017, 3:26 PM
*/

#include "Historic_test.hpp"
#include "gtest/gtest.h"
#include "Data.hpp"
#include "RetrieveData.hpp"
#include <sstream>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>

using namespace std;

Historic_test::Historic_test() {}

TEST(HistoricTest, getHistoricalData)
{
	RetrieveData * data = new RetrieveData();
	vector<vector<DataStructure>*> listAssets;
	data->getData(listAssets);
	ASSERT_EQ(1, 1);
}

TEST(HistoricTest, productExchange)
{
	RetrieveData* rd = new RetrieveData();

	vector<DataStructure> spots = vector<DataStructure>();
	vector<DataStructure> rates = vector<DataStructure>();

	spots.push_back(DataStructure(14, 7, 2014, 100, "spots", Currency::USD));
	spots.push_back(DataStructure(15, 7, 2014, 100, "spots", Currency::USD));
	spots.push_back(DataStructure(16, 7, 2014, 100, "spots", Currency::USD));

	rates.push_back(DataStructure(14, 7, 2014, 1, "rate", Currency::USD));
	rates.push_back(DataStructure(15, 7, 2014, 2, "rate", Currency::USD));
	rates.push_back(DataStructure(16, 7, 2014, 3, "rate", Currency::USD));

	rd->productExchange(spots, rates);

	ASSERT_EQ(spots.size(), 3);
	ASSERT_EQ(spots[0].cours_, 100);
	ASSERT_EQ(spots[1].cours_, 200);
	ASSERT_EQ(spots[2].cours_, 300);
}

TEST(HistoricTest, productAllExchange)
{
	RetrieveData* rd = new RetrieveData();

	vector<vector<DataStructure>*> allSpots = vector<vector<DataStructure>*>();
	vector<vector<DataStructure>*> allRates = vector<vector<DataStructure>*>();

	vector<DataStructure> spots1 = vector<DataStructure>();
	vector<DataStructure> spots2 = vector<DataStructure>();
	vector<DataStructure> spots3 = vector<DataStructure>();

	vector<DataStructure> rates1 = vector<DataStructure>();
	vector<DataStructure> rates2 = vector<DataStructure>();

	spots1.push_back(DataStructure(14, 7, 2014, 100, "spots1", Currency::USD));
	spots1.push_back(DataStructure(15, 7, 2014, 100, "spots1", Currency::USD));
	spots1.push_back(DataStructure(16, 7, 2014, 100, "spots1", Currency::USD));

	spots2.push_back(DataStructure(14, 7, 2014, 10, "spots2", Currency::JPY));
	spots2.push_back(DataStructure(15, 7, 2014, 10, "spots2", Currency::JPY));
	spots2.push_back(DataStructure(16, 7, 2014, 10, "spots2", Currency::JPY));

	spots3.push_back(DataStructure(14, 7, 2014, 1, "spots3", Currency::EUR));
	spots3.push_back(DataStructure(15, 7, 2014, 1, "spots3", Currency::EUR));
	spots3.push_back(DataStructure(16, 7, 2014, 1, "spots3", Currency::EUR));

	rates1.push_back(DataStructure(14, 7, 2014, 1, "rates1", Currency::USD));
	rates1.push_back(DataStructure(15, 7, 2014, 2, "rates1", Currency::USD));
	rates1.push_back(DataStructure(16, 7, 2014, 3, "rates1", Currency::USD));

	rates2.push_back(DataStructure(14, 7, 2014, 4, "rates2", Currency::JPY));
	rates2.push_back(DataStructure(15, 7, 2014, 6, "rates2", Currency::JPY));
	rates2.push_back(DataStructure(16, 7, 2014, 8, "rates2", Currency::JPY));

	allSpots.push_back(&spots2);
	allSpots.push_back(&spots1);
	allSpots.push_back(&spots3);

	allRates.push_back(&rates1);
	allRates.push_back(&rates2);

	ASSERT_EQ(allSpots.size(), 3);
	ASSERT_EQ(allRates.size(), 2);

	rd->productAllExchange(allSpots, allRates);

	ASSERT_EQ(allSpots[0]->at(0).cours_, 40);
	ASSERT_EQ(allSpots[0]->at(1).cours_, 60);
	ASSERT_EQ(allSpots[0]->at(2).cours_, 80);

	ASSERT_EQ(allSpots[1]->at(0).cours_, 100);
	ASSERT_EQ(allSpots[1]->at(1).cours_, 200);
	ASSERT_EQ(allSpots[1]->at(2).cours_, 300);

	ASSERT_EQ(allSpots[2]->at(0).cours_, 1);
	ASSERT_EQ(allSpots[2]->at(1).cours_, 1);
	ASSERT_EQ(allSpots[2]->at(2).cours_, 1);
}

TEST(HistoricTest, fillAllVects)
{
	RetrieveData* rd = new RetrieveData();

	vector<vector<DataStructure>*> allSpots = vector<vector<DataStructure>*>();
	vector<vector<DataStructure>*> allSpotsFilled = vector<vector<DataStructure>*>();

	vector<DataStructure> spots1 = vector<DataStructure>();
	vector<DataStructure> spots2 = vector<DataStructure>();
	vector<DataStructure> spots3 = vector<DataStructure>();

	spots1.push_back(DataStructure(11, 7, 2014, 100, "spots1", Currency::USD));
	spots1.push_back(DataStructure(12, 7, 2014, 120, "spots1", Currency::USD));
	spots1.push_back(DataStructure(14, 7, 2014, 130, "spots1", Currency::USD));

	spots2.push_back(DataStructure(11, 7, 2014, 10, "spots2", Currency::JPY));
	spots2.push_back(DataStructure(16, 7, 2014, 11, "spots2", Currency::JPY));

	spots3.push_back(DataStructure(11, 7, 2014, 1, "spots3", Currency::EUR));
	spots3.push_back(DataStructure(12, 7, 2014, 2, "spots3", Currency::EUR));
	spots3.push_back(DataStructure(15, 7, 2014, 3, "spots3", Currency::EUR));

	allSpots.push_back(&spots1);
	allSpots.push_back(&spots2);
	allSpots.push_back(&spots3);

	rd->fillAllVects(allSpotsFilled, allSpots);

	ASSERT_EQ(allSpotsFilled[0]->size(), 6);
	ASSERT_EQ(allSpotsFilled[1]->size(), 6);
	ASSERT_EQ(allSpotsFilled[2]->size(), 6);

	ASSERT_EQ(allSpotsFilled[0]->at(0).cours_, 100);
	ASSERT_EQ(allSpotsFilled[0]->at(1).cours_, 120);
	ASSERT_EQ(allSpotsFilled[0]->at(2).cours_, 120);
	ASSERT_EQ(allSpotsFilled[0]->at(3).cours_, 130);
	ASSERT_EQ(allSpotsFilled[0]->at(4).cours_, 130);
	ASSERT_EQ(allSpotsFilled[0]->at(5).cours_, 130);

	ASSERT_EQ(allSpotsFilled[1]->at(0).cours_, 10);
	ASSERT_EQ(allSpotsFilled[1]->at(1).cours_, 10);
	ASSERT_EQ(allSpotsFilled[1]->at(2).cours_, 10);
	ASSERT_EQ(allSpotsFilled[1]->at(3).cours_, 10);
	ASSERT_EQ(allSpotsFilled[1]->at(4).cours_, 10);
	ASSERT_EQ(allSpotsFilled[1]->at(5).cours_, 11);

	ASSERT_EQ(allSpotsFilled[2]->at(0).cours_, 1);
	ASSERT_EQ(allSpotsFilled[2]->at(1).cours_, 2);
	ASSERT_EQ(allSpotsFilled[2]->at(2).cours_, 2);
	ASSERT_EQ(allSpotsFilled[2]->at(3).cours_, 2);
	ASSERT_EQ(allSpotsFilled[2]->at(4).cours_, 3);
	ASSERT_EQ(allSpotsFilled[2]->at(5).cours_, 3);
}

TEST(HistoricTest, fillVect)
{
	RetrieveData* rd = new RetrieveData();
	vector<DataStructure> toFill = vector<DataStructure>();
	vector<DataStructure> vect = vector<DataStructure>();
	vect.push_back(DataStructure(11, 7, 2014, 100, "test", Currency::EUR));
	vect.push_back(DataStructure(14, 7, 2014, 230, "test", Currency::EUR));

	rd->fillVect(toFill, vect, Date(16 , 7, 2014));

	ASSERT_EQ(toFill.size(), 6);

	for (int i = 0; i < 3; i++)
	{
		ASSERT_EQ(toFill[i].cours_, 100);
	}

	for (int i = 3; i < 6; i++)
	{
		ASSERT_EQ(toFill[i].cours_, 230);
	}
}

TEST(HistoricTest, findByDate)
{
	RetrieveData* rd = new RetrieveData();
	Date d1 = Date(11, 8, 2014);
	Date d2 = Date(12, 8, 2014);
	Date d3 = Date(13, 8, 2014);
	vector<DataStructure> v = vector<DataStructure>();
	v.push_back(DataStructure(d2, 230, "test", Currency::EUR));
	double tmp = 0;

	ASSERT_EQ(rd->findByDate(tmp, d1, v), false);
	ASSERT_EQ(tmp, 0);
	ASSERT_EQ(rd->findByDate(tmp, d2, v), true);
	ASSERT_EQ(tmp, 230);
	ASSERT_EQ(rd->findByDate(tmp, d3, v), false);
}

TEST(HistoricTest, dateSuiv)
{
	RetrieveData* rd = new RetrieveData();
	Date d = Date(11, 7, 2014);

	for (int i = 0; i < 2922; i++)
	{
		rd->dateSuiv(d);
	}

	ASSERT_EQ(d.day_, 11);
	ASSERT_EQ(d.month_, 7);
	ASSERT_EQ(d.year_, 2022);
}

TEST(HistoricTest, getExchangeRate) {
	RetrieveData * data = new RetrieveData();

	vector<vector<DataStructure>*> listAssets;
	data->getExchangeRate(listAssets);
	
	ASSERT_EQ(1, 1);
}

TEST(HistoricTest, lastDate)
{
	RetrieveData* rd = new RetrieveData();
	Date d1 = Date(11, 8, 2014);
	Date d2 = Date(12, 8, 2015);
	Date d3 = Date(13, 8, 2016);
	Date res;

	vector<vector<DataStructure>*> allSpots = vector<vector<DataStructure>*>();

	vector<DataStructure> spots1 = vector<DataStructure>();
	vector<DataStructure> spots2 = vector<DataStructure>();
	vector<DataStructure> spots3 = vector<DataStructure>();

	spots1.push_back(DataStructure(d2, 100, "spots1", Currency::USD));

	spots2.push_back(DataStructure(d1, 10, "spots2", Currency::JPY));
	spots2.push_back(DataStructure(d3, 10, "spots2", Currency::JPY));

	spots3.push_back(DataStructure(d1, 1, "spots3", Currency::EUR));
	spots3.push_back(DataStructure(d2, 1, "spots3", Currency::EUR));

	allSpots.push_back(&spots1);
	allSpots.push_back(&spots2);
	allSpots.push_back(&spots3);

	rd->lastDate(res, allSpots);
	ASSERT_EQ((res == d3), true);
}
