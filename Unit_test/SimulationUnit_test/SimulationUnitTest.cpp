/*
 * File:   SimulationUnitTest.cpp
 * Author: gononl
 *
 * Created on January 9, 2017, 3:26 PM
 */

#include "SimulationUnitTest.hpp"

#include "gtest/gtest.h"

SimulationUnitTest::SimulationUnitTest() {}

TEST(SimulationUnitTest, size_matrix)
{
    int nbTimeSteps = 10;
    int size = 9;
    double maturity = 2;
    double r = 0.1;
    double rho = 0.3;
    int nbSamples = 5;
    PnlVect *spot = pnl_vect_create_from_scalar(size, 12);
    PnlVect *sigma = pnl_vect_create_from_scalar(size, 0.2);
	PnlVect * sigma_taux = pnl_vect_create_from_scalar(size, 0);

    Simulation *sim = new Simulation(nbSamples, nbTimeSteps,
			size, maturity, r, rho, spot, sigma, sigma_taux);

    PnlMat *path = pnl_mat_create(nbTimeSteps+1, size);

    sim->asset(path);

    EXPECT_EQ(path->m, nbTimeSteps + 1);
    EXPECT_EQ(path->n, size);

	pnl_vect_free(&spot);
	pnl_vect_free(&sigma);
	pnl_mat_free(&path);
}

TEST(SimulationUnitTest, asset_deterministe_unidimensionnel)
{
    int nbTimeSteps = 1;
    int size = 1;
    double maturity = 1;
    double r = 0.1;
    double rho = 1;
    PnlVect *sigma = pnl_vect_create_from_scalar(size, 1);
    PnlVect *spot = pnl_vect_create_from_scalar(size, 100);
    int nbSamples = 5;
	PnlVect * sigma_taux = pnl_vect_create_from_scalar(size, 0);

    Simulation *sim = new Simulation(nbSamples, nbTimeSteps,
			size, maturity, r, rho, spot, sigma, sigma_taux);

    PnlMat *path = pnl_mat_create(nbTimeSteps+1, size);

    // On remplace provisoirement le RNG par un vecteur pré-déterminé afin de vérifier les calculs
    PnlVect *vect = pnl_vect_create_from_scalar(size, 0.5);
    sim->assetDeterministe(path, vect);
    
    EXPECT_EQ(MGET(path, 0, 0), 100);  
    EXPECT_EQ(floor(MGET(path, 1, 0) * 1000), floor(100*exp(0.1)* 1000)); 

	pnl_vect_free(&vect);
	pnl_mat_free(&path);
    pnl_vect_free(&spot);
	pnl_vect_free(&sigma);
}

TEST(SimulationUnitTest, assetDeterministeEnT)
{
    // On fixe les paramètres du modèle 
    int size = 1;
    double r = 0.1;
    double rho = 0;
    PnlVect *sigma = pnl_vect_create_from_scalar(size, 0.3);
    PnlVect *spot = pnl_vect_create_from_scalar(size, 100);
    
    // Paramètres de la fonction asset à partir de t
    int nbSamples = 100;
    double t = 1;
    double maturity = 2;
    int nbTimeSteps = 2;
    PnlMat *path = pnl_mat_create(nbTimeSteps+1, size);
    PnlMat *past = pnl_mat_create_from_scalar(2, 1, 100);
    PnlVect *vect = pnl_vect_create_from_scalar(size, 0.5);
	PnlVect * sigma_taux = pnl_vect_create_from_scalar(size, 0);

    Simulation *sim = new Simulation(nbSamples, nbTimeSteps, size, maturity, r, rho, spot, sigma, sigma_taux);
    
    sim->assetDeterministe(path, t, past, vect);
    
    PnlMat *verif = pnl_mat_create_from_scalar(3, 1, 100);
    pnl_mat_set(verif, 2, 0, 100*exp(0.205));

    EXPECT_EQ(MGET(path, 0 , 0), MGET(verif, 0, 0));
    EXPECT_EQ(MGET(path, 1 , 0), MGET(verif, 1, 0));
    EXPECT_EQ(MGET(path, 2 , 0), MGET(verif, 2, 0));
}