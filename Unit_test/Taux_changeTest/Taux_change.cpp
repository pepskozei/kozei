/*
* File:   KozeiUnitTest.cpp
* Author: mignotju
*
* Created on January 9, 2017, 3:26 PM
*/

#include "Taux_change.hpp"
#include "Simulation.hpp"

#include "gtest/gtest.h"

using namespace std;

Taux_change_UnitTest::Taux_change_UnitTest() {}

// si la premiere ligne de path est �gale � 0,
// cela pose probleme car on va diviser par 0

TEST(DISABLED_Taux_change_UnitTest, Simulation_SX_un_dim)
{
	double X_0 = 1.05;
	//S : actif �tranger
	double S_0 = 100;
	int nbSamples = 10000;
	int nbTimeSteps = 10;
	int size = 1;
	double maturity = 2;
	double r = 0.05;
	double rho = 0.3;
	PnlVect * spot = pnl_vect_create_from_scalar(size,S_0*X_0);
	PnlVect * sigma = pnl_vect_create_from_scalar(size, 0.2);
	PnlVect * sigma_taux = pnl_vect_create_from_scalar(size, 0.4);

	Data * data = new Data( nbSamples,
		 nbTimeSteps,
		 size,
		 maturity,
		 r,
		 rho,
		 spot,
		 sigma,
		 sigma_taux);

	PnlMat * path = pnl_mat_create(nbTimeSteps + 1, size);
	PnlVect * alea_vect = pnl_vect_create_from_scalar(size, 0.5);
	data->assetDeterministe(path, alea_vect);

	pnl_mat_print(path);
		
	EXPECT_NEAR(GET(path, 1),116.994,0.001);
	EXPECT_NEAR(GET(path, 2), 130.359, 0.001);
	EXPECT_NEAR(GET(path, 3), 145.249, 0.001);
	EXPECT_NEAR(GET(path, 4), 161.841, 0.001);
	EXPECT_NEAR(GET(path, 5), 180.329, 0.001);
	EXPECT_NEAR(GET(path, 6), 200.928, 0.001);
	EXPECT_NEAR(GET(path, 7), 223.88, 0.001);
	EXPECT_NEAR(GET(path, 8), 249.454, 0.001);
	EXPECT_NEAR(GET(path, 9), 277.949, 0.001);
	EXPECT_NEAR(GET(path, 10), 309.699, 0.001);	
}

TEST(DISABLED_Taux_change_UnitTest, Simulation_SX_multi_dim)
{
	double X_0 = 1.05; //exple : USD
	double X_1 = 136.25; //exple : JPY
	//S : actif �tranger
	double S_0 = 100;
	double S_1 = 80;
	double S_2 = 70;
	int nbSamples = 10000;
	int nbTimeSteps = 10;
	int size = 3;
	double maturity = 2;
	double r = 0.05;
	double rho = 0;
	PnlVect * spot = pnl_vect_create(size);
	LET(spot, 0) = X_0*S_0;
	LET(spot, 1) = X_1*S_1;
	LET(spot, 2) = X_0*S_2;
	PnlVect * sigma = pnl_vect_create_from_scalar(size, 0.2);
	PnlVect * sigma_taux = pnl_vect_create(size);
	LET(sigma_taux, 0) = 0.4;
	LET(sigma_taux, 1) = 0.8;
	LET(sigma_taux, 2) = 0.4;

	Data * data = new Data(nbSamples,
		nbTimeSteps,
		size,
		maturity,
		r,
		rho,
		spot,
		sigma,
		sigma_taux);

	PnlMat * path = pnl_mat_create(nbTimeSteps + 1, size);
	PnlVect * alea_vect = pnl_vect_create_from_scalar(size, 0.5);

	data->assetDeterministe(path, alea_vect);
}

TEST(DISABLED_Taux_change_UnitTest, asset_taux_change)
{
	int nbDevises = 5;
	int nbSamples = 10000;
	int nbTimeSteps = 10;
	int size = 30;
	double maturity = 2;
	double r = 0.05;
	double rho = 0;
	PnlVect * spot = pnl_vect_create_from_scalar(size, 100);
	PnlVect * sigma = pnl_vect_create_from_scalar(size, 0.2);
	PnlVect * sigma_taux = pnl_vect_create(nbDevises);
	LET(sigma_taux, 0) = 0.1;//USD
	LET(sigma_taux, 1) = 0.2;//JPY
	LET(sigma_taux, 2) = 0.3;//GBP
	LET(sigma_taux, 3) = 0.4;//CAD
	LET(sigma_taux, 4) = 0;//EUR
	PnlVect * taux_sans_risque_etranger = pnl_vect_create(nbDevises);
	PnlVect * spot_change = pnl_vect_create(nbDevises);

	LET(spot_change, 0) = 0.93;//USD
	LET(spot_change, 1) = 0.008;//JPY
	LET(spot_change, 2) = 1.14;//GBP
	LET(spot_change, 3) = 0.69;//CAD
	LET(spot_change, 4) = 1;//EUR

	Data * d = new Data(nbSamples, nbTimeSteps, size, maturity, r, rho, spot, sigma, 
		sigma_taux, taux_sans_risque_etranger, nbDevises, spot_change);

	PnlMat* path = pnl_mat_new();
	PnlVect* vect = pnl_vect_create_from_scalar(d->size_,0.1);
	d->assetDeterministeTaux(path, vect);
}