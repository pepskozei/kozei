#include "BackTest_UnitTest.hpp"
#include "Data.hpp"
#include "EstimationParam.hpp"
#include "RetrieveData.hpp"
#include "DataStructure.hpp"

#include <iostream>
#include <vector>
#include "Portefeuille.hpp"
#include "Kozei.hpp"
#include "KozeiChange.hpp"

#include <fstream>
#include <string>

using namespace std;

BackTest_UnitTest::BackTest_UnitTest() {}

TEST(DISABLED_BackTest_UnitTest, back_test_with_estim_parameters)
{
	RetrieveData* retrieve_data = new RetrieveData();

	vector<vector<DataStructure>*> vect = vector<vector<DataStructure>*>();
	vector<vector<DataStructure>*> vectToFill_cours = vector<vector<DataStructure>*>();
	retrieve_data->getData(vect);

	EstimationParam *estim = new EstimationParam();
	double NbDays = 252;
	int windowSize = 200;
	PnlMat* mat_covariance = pnl_mat_new();
	PnlMat* corr = pnl_mat_new();
	PnlVect* volatility = pnl_vect_new();

	estim->getCovMatrix(mat_covariance, vect, windowSize, NbDays);
	estim->getVolatility(volatility, mat_covariance);
	estim->getCorrelationMatrix(corr, mat_covariance);

	PnlMat* past = pnl_mat_new();
	retrieve_data->fillAllVects(vectToFill_cours, vect);

	retrieve_data->convertDataStructToPnl(past, vectToFill_cours);

	PnlVect* spot = pnl_vect_new();
	pnl_mat_get_row(spot, past, 0);

	int nbTimeSteps = 2922;
	int size = 30;
	double maturity = 8;
	double r = 0.01;
	double rho = 0.35;
	int nbSamples = 1000;
	
	PnlVect* sigma_taux = pnl_vect_create_from_scalar(size, 0);

	Data * data = new Data(nbSamples, nbTimeSteps, size, maturity, r, corr, spot, volatility, sigma_taux);

	double I = 100;
	double fdstep = 0.01;
	Produit *produit = new Kozei(data, I);
	Portefeuille *pf = new Portefeuille(produit, 0, fdstep);
	double erreur_couverture;
	PnlVect *price = pnl_vect_create(past->m);
	PnlVect *ic = pnl_vect_create(past->m);
	PnlVect* val_pf = pnl_vect_create(past->m);

	pf->calcul_pf_ToNow(erreur_couverture, price, ic, past, val_pf);

	string mon_fichier = "Portefeuille.csv";  // je stocke dans la cha�ne mon_fichier le nom du fichier � ouvrir
	ofstream fichier(mon_fichier.c_str(), ios::out | ios::app);
	if (fichier)  // si l'ouverture a r�ussi
	{
		// instructions
		fichier << "PRICE : " << endl;
		for (int i = 0; i < price->size; i++){
			fichier << GET(price, i) << ";";
		}
		fichier << "\n" << endl;
		fichier << "VAL_PF : " << endl;
		for (int i = 0; i < val_pf->size; i++)
			fichier << GET(val_pf, i) << ";";
		fichier.close();  // je referme le fichier
	}

	else  // sinon
	{
		cerr << "Erreur � l'ouverture !" << endl;
	}
}

TEST(BackTest_UnitTest, back_test_with_estim_parameters_taux_change)
{
	RetrieveData* retrieve_data = new RetrieveData();

	vector<vector<DataStructure>*> vect_cours = vector<vector<DataStructure>*>();
	vector<vector<DataStructure>*> vect_change = vector<vector<DataStructure>*>();

	vector<vector<DataStructure>*> vectToFill_cours = vector<vector<DataStructure>*>();
	vector<vector<DataStructure>*> vectToFill_change = vector<vector<DataStructure>*>();

	// r�cuperation du vector de vector de cours
	retrieve_data->getData(vect_cours);

	//Recuperation du vector de vector de change
	retrieve_data->getExchangeRate(vect_change);

	EstimationParam *estim = new EstimationParam();
	double NbDays = 252;
	int windowSize = 200;
	PnlMat* mat_covariance = pnl_mat_new();
	PnlMat* corr = pnl_mat_new();
	PnlVect* volatility = pnl_vect_new();

	vector<vector<DataStructure>*> result_concat = vector<vector<DataStructure>*>();
	retrieve_data->concatVect(result_concat, vect_cours, vect_change);
	estim->getCovMatrix(mat_covariance, result_concat, windowSize, NbDays);
	estim->getVolatility(volatility, mat_covariance);
	estim->getCorrelationMatrix(corr, mat_covariance);

	PnlMat* past = pnl_mat_new();

	//on met les matrices de cours et change � la m�me taille
	retrieve_data->fillAllVectsChange(vectToFill_cours, vectToFill_change, vect_cours, vect_change);

	//on calcule le vector de vector des SX, on r�cup�re dans vectoFill_cours
	retrieve_data->productAllExchange(vectToFill_cours, vectToFill_change);

	int nbDevises = 4;
	PnlVect * taux_sans_risque_etranger = pnl_vect_create(nbDevises);
	LET(taux_sans_risque_etranger, 0) = 0.0068667;
	LET(taux_sans_risque_etranger, 1) = 0.0002429;
	LET(taux_sans_risque_etranger, 2) = 0.0022500;
	LET(taux_sans_risque_etranger, 3) = 0.01891;

	//on calcule le vector de vector des RX, on r�cup�re dans vectToFill_change
	retrieve_data->domesticRates(vectToFill_change, taux_sans_risque_etranger);

	//concat�nation des SX et des RX
	retrieve_data->concatVect(result_concat, vectToFill_cours, vectToFill_change);

	//on r�cup�re une PnlMat past
	retrieve_data->convertDataStructToPnl(past, result_concat);

	PnlVect* spot = pnl_vect_new();
	pnl_mat_get_row(spot, past, 0);

	int nbTimeSteps = 16;
	int size = 30; 
	double maturity = 8.0;
	double r = 0.01039; //taux sans risque domestique (EUR)
	int nbSamples = 1000;

	PnlVect * sigma_taux = pnl_vect_create(nbDevises+1);
	PnlVect * tmp = pnl_vect_new();
	PnlVect * sigma = pnl_vect_new();
	pnl_vect_extract_subvect(sigma, volatility, 0, 30);
	pnl_vect_print(sigma);
	pnl_vect_extract_subvect(tmp, volatility, 30, 4);
	pnl_vect_print(tmp);

	for (int i = 0; i < tmp->size; i++)
	{
		LET(sigma_taux, i) = GET(tmp, i);
	}
	
	LET(sigma_taux, 4) = 0;
	pnl_vect_print(sigma_taux);
	PnlVect * spot_taux = pnl_vect_new();

	Data * data = new Data(nbSamples, nbTimeSteps, size, maturity, r, corr, spot, volatility, sigma_taux, 
		taux_sans_risque_etranger, nbDevises, spot_taux);

	double I = 100;
	double fdstep = 0.01;
	KozeiChange *produit = new KozeiChange(data, I);
	Portefeuille *pf = new Portefeuille(produit, 0, fdstep);
	double erreur_couverture;
	PnlVect *price = pnl_vect_create(past->m);
	PnlVect *ic = pnl_vect_create(past->m);
	PnlVect* val_pf = pnl_vect_create(past->m);
	PnlVect* val_pfR = pnl_vect_create(past->m);

	pf->calcul_pf_ToNow_change(erreur_couverture, price, ic, past, val_pf, val_pfR);

	string mon_fichier = "Portefeuille_pf.csv";  // je stocke dans la cha�ne mon_fichier le nom du fichier � ouvrir
	ofstream fichier(mon_fichier.c_str(), ios::out | ios::app);
	
	if (fichier)  // si l'ouverture a r�ussi
	{
		// instructions
		fichier << "PRICE : " << endl;
		for (int i = 0; i < price->size; i++){
			fichier << GET(price, i) << ";";
		}
		fichier << "\n" << endl;
		fichier << "VAL_PF : " << endl;
		for (int i = 0; i < val_pf->size; i++)
			fichier << GET(val_pf, i) << ";";
		fichier << "\n" << endl;
		fichier << "VAL_PFR : " << endl;
		for (int i = 0; i < val_pfR->size; i++)
			fichier << GET(val_pfR, i) << ";";
		fichier.close();  // je referme le fichier
	}

	else  // sinon
	{
		cerr << "Erreur � l'ouverture !" << endl;
	}
}

TEST(DISABLED_BackTest_UnitTest, back_test_with_estim_parameters_taux_change)
{
	RetrieveData* retrieve_data = new RetrieveData();

	vector<vector<DataStructure>*> vect_cours = vector<vector<DataStructure>*>();
	vector<vector<DataStructure>*> vect_change = vector<vector<DataStructure>*>();

	vector<vector<DataStructure>*> vectToFill_cours = vector<vector<DataStructure>*>();
	vector<vector<DataStructure>*> vectToFill_change = vector<vector<DataStructure>*>();

	// r�cuperation du vector de vector de cours
	retrieve_data->getData(vect_cours);

	//Recuperation du vector de vector de change
	retrieve_data->getExchangeRate(vect_change);

	EstimationParam *estim = new EstimationParam();
	double NbDays = 252;
	int windowSize = 200;
	PnlMat* mat_covariance = pnl_mat_new();
	PnlMat* corr = pnl_mat_new();
	PnlVect* volatility = pnl_vect_new();

	vector<vector<DataStructure>*> result_concat = vector<vector<DataStructure>*>();
	retrieve_data->concatVect(result_concat, vect_cours, vect_change);
	estim->getCovMatrix(mat_covariance, result_concat, windowSize, NbDays);
	estim->getVolatility(volatility, mat_covariance);
	estim->getCorrelationMatrix(corr, mat_covariance);

	PnlMat* past = pnl_mat_new();

	//on met les matrices de cours et change � la m�me taille
	retrieve_data->fillAllVectsChange(vectToFill_cours, vectToFill_change, vect_cours, vect_change);

	//on calcule le vector de vector des SX, on r�cup�re dans vectoFill_cours
	retrieve_data->productAllExchange(vectToFill_cours, vectToFill_change);

	int nbDevises = 4;
	PnlVect * taux_sans_risque_etranger = pnl_vect_create(nbDevises);
	LET(taux_sans_risque_etranger, 0) = 0.0068667;
	LET(taux_sans_risque_etranger, 1) = 0.0002429;
	LET(taux_sans_risque_etranger, 2) = 0.0022500;
	LET(taux_sans_risque_etranger, 3) = 0.01891;
	
	//on calcule le vector de vector des RX, on r�cup�re dans vectToFill_change
	retrieve_data->domesticRates(vectToFill_change, taux_sans_risque_etranger);

	//concat�nation des SX et des RX
	retrieve_data->concatVect(result_concat, vectToFill_cours, vectToFill_change);

	//on r�cup�re une PnlMat past
	retrieve_data->convertDataStructToPnl(past, result_concat);

	PnlVect* spot = pnl_vect_new();
	pnl_mat_get_row(spot, past, 0);

	int nbTimeSteps = 2922;
	int size = 30;
	double maturity = 8.0;
	double r = 0.01039; //taux sans risque domestique (EUR)
	int nbSamples = 100;

	PnlVect * sigma_taux = pnl_vect_create(nbDevises + 1);
	PnlVect * tmp = pnl_vect_new();
	PnlVect * sigma = pnl_vect_new();
	pnl_vect_extract_subvect(sigma, volatility, 0, 30);
	pnl_vect_print(sigma);
	pnl_vect_extract_subvect(tmp, volatility, 30, 4);
	pnl_vect_print(tmp);

	for (int i = 0; i < tmp->size; i++)
	{
		LET(sigma_taux, i) = GET(tmp, i);
	}
	
	LET(sigma_taux, 4) = 0;
	pnl_vect_print(sigma_taux);
	PnlVect * spot_taux = pnl_vect_new();

	Data * data = new Data(nbSamples, nbTimeSteps, size, maturity, r, corr, spot, volatility, sigma_taux,
		taux_sans_risque_etranger, nbDevises, spot_taux);

	double I = 100;
	double fdstep = 0.01;
	Produit *produit = new KozeiChange(data, I);
	Portefeuille *pf = new Portefeuille(produit, 0, fdstep);
	double erreur_couverture;
	PnlVect *price = pnl_vect_create(past->m);
	PnlVect *ic = pnl_vect_create(past->m);
	PnlVect* val_pf = pnl_vect_create(past->m);
	PnlVect* val_pfR = pnl_vect_create(past->m);

	PnlVect* deltaT = pnl_vect_create(34);
	pf->calcul_delta_en_t(10,past, deltaT);

	string mon_fichier = "delta2.csv";  // je stocke dans la cha�ne mon_fichier le nom du fichier � ouvrir
	ofstream fichier(mon_fichier.c_str(), ios::out | ios::app);
	if (fichier)  // si l'ouverture a r�ussi
	{
		// instructions
		for (int i = 0; i < deltaT->size; i++){
			fichier << GET(deltaT, i) << ";";
		}
		fichier.close();  // je referme le fichier
	}

	else  // sinon
	{
		cerr << "Erreur � l'ouverture !" << endl;
	}
}