/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   ShiftAsset_test.hpp
 * Author: paviotch
 *
 * Created on January 9, 2017, 3:26 PM
 */

#ifndef SHIFTASSET_TEST_HPP
#define SHIFTASSET_TEST_HPP

#include "gtest/gtest.h"

#include "CouvertureDelta.hpp"
#include "Simulation.hpp"
#include "Produit.hpp"
#include "Kozei.hpp"

class ShiftAsset_test : public testing::Test
{
public:
    ShiftAsset_test();
    ShiftAsset_test(const ShiftAsset_test& orig);
    virtual ~ShiftAsset_test();
private:
};

#endif /* SHIFTASSET_TEST_HPP */
