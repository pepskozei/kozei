/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   ShiftAsset_test.cpp
 * Author: paviotch
 *
 * Created on January 9, 2017, 3:26 PM
 */

#include "ShiftAsset_test.hpp"

#include "pnl/pnl_random.h"

ShiftAsset_test::ShiftAsset_test() {}

TEST(ShiftAssetTest, ShiftAssetMatrix)
{
	double h = 0.01;
    int nbTimeSteps = 10;
    int size = 9;
    double maturity = 2;
    double r = 0.1;
    double rho = 0.3;
    int nbSamples = 5;
    PnlVect *spot = pnl_vect_create_from_scalar(size, 12);
    PnlVect *sigma = pnl_vect_create_from_scalar(size, 0.2);
	PnlVect * sigma_taux = pnl_vect_create_from_scalar(size, 0);

    Simulation *data = new Simulation(nbSamples, nbTimeSteps,
        size, maturity, r, rho, spot, sigma,sigma_taux);
		
	Produit *produit = new Kozei(data, 0);
	CouvertureDelta *c = new CouvertureDelta(h, produit);

	PnlMat* path = pnl_mat_create_from_scalar(10, 10, 5);
	PnlMat* shift_path = pnl_mat_create(10, 10);
	PnlMat* shift_path_compare = pnl_mat_create_from_scalar(10, 10, 5);
	PnlVect *V = pnl_vect_create_from_scalar(10, 5.05);
	pnl_mat_set_col(shift_path_compare,V,6);
	MLET(shift_path_compare, 0, 6) = 5;

	int indice_actif = 6;
	double t = 0.5;
	double timestep = 1;
	c->shiftAsset(shift_path, path, indice_actif, h, t, timestep);

	ASSERT_EQ(shift_path->m, shift_path_compare->m);
	ASSERT_EQ(shift_path->n, shift_path_compare->n);

	for (int i = 0; i < shift_path->m; i++)
	{
		for (int j = 0; j < shift_path->n; j++)
		{
			ASSERT_EQ(MGET(shift_path, i, j), MGET(shift_path_compare, i, j));
		}
	}

    pnl_vect_free(&V);
	pnl_mat_free(&path);
	pnl_mat_free(&shift_path);
	pnl_mat_free(&shift_path_compare);
}

ShiftAsset_test::ShiftAsset_test(const ShiftAsset_test& orig) {}

ShiftAsset_test::~ShiftAsset_test() {}
