/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   CalculDelta_test.cpp
 * Author: duclaur
 *
 * Created on January 11, 2017, 9:29 PM
 */
 
 #include "pnl/pnl_random.h"

#include "CalculDelta_test.hpp"
#include "CouvertureDelta.hpp"
#include "Simulation.hpp"
#include "Kozei.hpp"
#include "Call.hpp"

using namespace std;

CalculDelta_test::CalculDelta_test() {}

TEST(DISABLED_CalculDeltaTest, delta_deterministe_unidimensionnel_kozei)
{
    double fdStep = 0.1;
    int nbSamples = 1;
    int nbTimeSteps = 16;
    int size = 1;
    double maturity = 8;
    double r = 0.1;
    double rho = 1;
        
    PnlVect *sigma = pnl_vect_create_from_scalar(size, 1);
	PnlVect * sigma_taux = pnl_vect_create_from_scalar(size, 0);
    PnlVect *spot = pnl_vect_create_from_scalar(size, 100);

    PnlVect* delta = pnl_vect_create_from_zero(size);

    Simulation *data = new Simulation(nbSamples, nbTimeSteps, size, maturity, r, rho, spot, sigma, sigma_taux);

	PnlMat *past = pnl_mat_create_from_scalar(1, size, 100);


    //On remplace provisoirement le RNG par un vecteur pré-déterminé afin de vérifier les calculs
    PnlVect *vect = pnl_vect_create_from_scalar(size, 0.5);

	Produit *produit = new Kozei(data, 100);
	CouvertureDelta *c = new CouvertureDelta(fdStep, produit);
        
	c->calculDeltaDeterministe(delta, past, 0.5, vect);
        
	EXPECT_EQ(ceil(GET(delta, 0)*10000)/10000, 1.3276);
    
	pnl_vect_free(&vect);
	pnl_mat_free(&past);
    pnl_vect_free(&spot);
	pnl_vect_free(&sigma);
    pnl_vect_free(&delta);
}

TEST(DISABLED_CalculDeltaTest, test_delta_call)
{
	int nbTimeSteps = 16;
    double strike = 80;
    int nbSamples = 100000;
    double fdstep = 0.01;
	int nbTimeStepH = 64;
	int size = 1;
	double I = 100;
	double maturity = 1;
	double r = 0.05;
	double rho = 0.0;
	double lambda = 1;
	const PnlVect *spot = pnl_vect_create_from_scalar(size, 100);
	const PnlVect *sigma = pnl_vect_create_from_scalar(size, 0.2);
	PnlVect * sigma_taux = pnl_vect_create_from_scalar(size, 0);
  
	Data * data = new Simulation(nbSamples, nbTimeSteps, size, maturity, r, rho, spot, sigma, sigma_taux);
	Call *call=new Call(data,0,strike);
	PnlMat * path = pnl_mat_create(nbTimeSteps+1,1);
	data->asset(path);
	CouvertureDelta *c = new CouvertureDelta(fdstep, call);
	PnlVect *delta = pnl_vect_create(size);
	PnlMat* past= pnl_mat_create(1, size);
	PnlVect *row_to_add = pnl_vect_new();
	pnl_mat_set_row(past, data->spot_, 0);

	double step = (maturity/nbTimeSteps);

	double erreurCum = 0;
	double ecart = 0;
  
	for (double t = 0; t < maturity; t += step)
	{
		pnl_mat_get_row(row_to_add, path, t/step);

		if (t != 0)
		{	
			pnl_mat_add_row(past, past->m,row_to_add);
		}

		c->calculDelta(delta,past,t);
    
		int pas = (int)(t / step);
		double d1 = (log(MGET(path,pas,0)/strike) +
			((r + pow(GET(sigma,0),2)/2 )*(maturity-t)))/(GET(sigma,0)*sqrt(maturity-t));
	}
}


CalculDelta_test::CalculDelta_test(const CalculDelta_test& orig) {}

CalculDelta_test::~CalculDelta_test() {}