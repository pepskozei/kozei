/*
 * File:   KozeiUnitTest.cpp
 * Author: mignotju
 *
 * Created on January 9, 2017, 3:26 PM
 */

#include "KozeiUnitTest.hpp"
#include "Simulation.hpp"
#include "gtest/gtest.h"

KozeiUnitTest::KozeiUnitTest() {}

// si la premiere ligne de path est égale à 0,
// cela pose probleme car on va diviser par 0

TEST(KozeiUnitTest, payoff_avec_matrice_path_valeurs_identiques)
{
	double I = 100;
	PnlMat* path = pnl_mat_create_from_scalar(2923, 30, 1);
	Kozei* kozei = new Kozei(NULL, I);
	double p = kozei->payoff(path);

	double renta = 0;
	double attendu = I*(1+renta);

	EXPECT_EQ(p, attendu);
	pnl_mat_free(&path);
}

TEST(DISABLED_KozeiUnitTest, payoff_avec_path_une_petite_matrice_avec_que_des_termes_positifs)
{
	double I = 100;
	PnlMat* path = pnl_mat_create_from_scalar(17, 3, 1);

	for (int i = 0; i < path->m; i++)
	{
		for (int j = 0; j < path->n; j++)
		{
			MLET(path,i,j) = i+1;
		}
	}
	Kozei* kozei = new Kozei(NULL, I);
	double p = kozei->payoff(path);

	double renta = 0.3*17;
	double attendu = I*(1+renta);
	EXPECT_EQ(p, attendu);
	pnl_mat_free(&path);
}

TEST(KozeiUnitTest, payoff_path_deux_colonnes)
{
	double I = 100;
	Kozei* kozei = new Kozei(NULL, I);
	PnlMat* path = pnl_mat_create_from_zero(2923, 2);

	for (int i = 0; i < kozei->init_.size(); i++)
	{
		MLET(path, kozei->init_[i], 0) = 1;
		MLET(path, kozei->init_[i], 1) = 10;
	}
	
	for (int i = 0; i < kozei->constat_.size(); i++)
	{
		MLET(path, kozei->constat_[i], 0) = i+2;
		MLET(path, kozei->constat_[i], 1) = i+11;
	}

	double p = kozei->payoff(path);

	double renta = (0.6*0.55*17.0)/2.0;
	double attendu = I*(1+renta);

	EXPECT_EQ(p, attendu);
	pnl_mat_free(&path);
}

TEST(DISABLED_KozeiUnitTest, payoff_KozeiChange_RX_constants)
{
	int nbDevises = 4;
	PnlVect* tsre = pnl_vect_create_from_zero(nbDevises);
	double I = 100;

	Data* data = new Data(0, 30, 0.0, 8.0, 0.0, 0.0, pnl_vect_new(), pnl_vect_new(),
		pnl_vect_new(), tsre, 4, pnl_vect_new());

	KozeiChange* kozeiChange = new KozeiChange(data, I);
	PnlMat* path = pnl_mat_create_from_double(2923, 34, 100);

	for (int i = 0; i < kozeiChange->init_.size(); i++)
	{
		MLET(path, kozeiChange->init_[i], 0) = 1;
		MLET(path, kozeiChange->init_[i], 22) = 1;
		MLET(path, kozeiChange->init_[i], 23) = 1;
		MLET(path, kozeiChange->init_[i], 24) = 1;
		MLET(path, kozeiChange->init_[i], 25) = 1;

		for (int j = 30; j < 34; j++)
		{
			MLET(path, kozeiChange->init_[i], j) = j - 29;
		}
	}

	for (int i = 0; i < kozeiChange->constat_.size(); i++)
	{
		MLET(path, kozeiChange->constat_[i], 0) = i + 2;
		MLET(path, kozeiChange->constat_[i], 22) = i + 2;
		MLET(path, kozeiChange->constat_[i], 23) = i + 2;
		MLET(path, kozeiChange->constat_[i], 24) = i + 2;
		MLET(path, kozeiChange->constat_[i], 25) = i + 2;

		for (int j = 30; j < 34; j++)
		{
			MLET(path, kozeiChange->constat_[i], j) = j - 29;
		}
	}

	double p = kozeiChange->payoff(path);

	double renta = (0.6*17.0)/12.0;
	double attendu = I*(1 + renta);

	EXPECT_EQ(p, attendu);
	pnl_mat_free(&path);
}

TEST(KozeiUnitTest, payoff_KozeiChange_R_nuls)
{
	int nbDevises = 4;
	PnlVect* tsre = pnl_vect_create_from_zero(nbDevises);
	double I = 100;

	Data* data = new Data(0, 0, 30, 8.0, 0.0, 0.0, pnl_vect_new(), pnl_vect_new(),
		pnl_vect_new(), tsre, 4, pnl_vect_new());

	KozeiChange* kozeiChange = new KozeiChange(data, I);
	PnlMat* path = pnl_mat_create_from_double(2923, 34, 100);

	for (int i = 0; i < kozeiChange->init_.size(); i++)
	{
		MLET(path, kozeiChange->init_[i], 0) = 1;
		MLET(path, kozeiChange->init_[i], 22) = 1;
		MLET(path, kozeiChange->init_[i], 23) = 1;
		MLET(path, kozeiChange->init_[i], 24) = 1;
		MLET(path, kozeiChange->init_[i], 25) = 1;

		for (int j = 30; j < 34; j++)
		{
			MLET(path, kozeiChange->init_[i], j) = j - 29;
		}
	}

	for (int i = 0; i < kozeiChange->constat_.size(); i++)
	{
		MLET(path, kozeiChange->constat_[i], 0) = i + 2;
		MLET(path, kozeiChange->constat_[i], 22) = i + 2;
		MLET(path, kozeiChange->constat_[i], 23) = i + 2;
		MLET(path, kozeiChange->constat_[i], 24) = i + 2;
		MLET(path, kozeiChange->constat_[i], 25) = i + 2;

		for (int j = 30; j < 34; j++)
		{
			MLET(path, kozeiChange->constat_[i], j) = j - 29 + 0.1*(i + 1);
		}
	}

	double p = kozeiChange->payoff(path);

	double renta = (0.6*17.0) / 12.0;
	double attendu = I*(1 + renta);

	EXPECT_EQ(p, attendu);
	pnl_mat_free(&path);
}

TEST(DISABLED_KozeiUnitTest, payoff_KozeiChange_SX_constants)
{
	double I = 100;
	Kozei* kozei = new Kozei(NULL, I);
	PnlMat* path = pnl_mat_create_from_zero(2923, 2);

	for (int i = 0; i < kozei->init_.size(); i++)
	{
		MLET(path, kozei->init_[i], 0) = 1;
		MLET(path, kozei->init_[i], 1) = 10;
	}

	for (int i = 0; i < kozei->constat_.size(); i++)
	{
		MLET(path, kozei->constat_[i], 0) = i + 2;
		MLET(path, kozei->constat_[i], 1) = i + 11;
	}

	double p = kozei->payoff(path);

	double renta = (0.6*0.55*17.0) / 2.0;
	double attendu = I*(1 + renta);

	EXPECT_EQ(p, attendu);
	pnl_mat_free(&path);
}

TEST(KozeiUnitTest, init)
{
	double I = 100;

	int nbTimeSteps = 32;
	int size = 3;
	double maturity = 8;
	double r = 0.04879;
	double rho = 0.3;
	int nbSamples = 100;
	PnlVect *spot = pnl_vect_create_from_scalar(size, 100);
	PnlVect *sigma = pnl_vect_create_from_scalar(size, 0.2);
	PnlVect * sigma_taux = pnl_vect_create_from_scalar(size, 0);

	Simulation *data = new Simulation(nbSamples, nbTimeSteps,
		size, maturity, r, rho, spot, sigma, sigma_taux);

	Kozei *kozei = new Kozei(data, I);

	PnlVect* cours0 = pnl_vect_new();
	PnlMat* path = pnl_mat_create_from_zero(7, 3);
	
	MLET(path, 0, 0) = 0;
	MLET(path, 0, 1) = 20;
	MLET(path, 0, 2) = 100;

	MLET(path, 4, 0) = 1;
	MLET(path, 4, 1) = 60;
	MLET(path, 4, 2) = 100;

	MLET(path, 5, 0) = 2;
	MLET(path, 5, 1) = 40;
	MLET(path, 5, 2) = 100;

	kozei->init(cours0, path);

	EXPECT_EQ(cours0->size, 3);
	EXPECT_EQ(GET(cours0, 0), 1);
	EXPECT_EQ(GET(cours0, 1), 40);
	EXPECT_EQ(GET(cours0, 2), 100);

	pnl_vect_free(&cours0);
	pnl_vect_free(&spot);
	pnl_vect_free(&sigma);
	pnl_vect_free(&sigma_taux);

	pnl_mat_free(&path);
}
