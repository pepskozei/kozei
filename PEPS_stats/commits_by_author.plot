set terminal png transparent size 640,240
set size 1.0,1.0

set terminal png transparent size 640,480
set output 'commits_by_author.png'
set key left top
set yrange [0:]
set xdata time
set timefmt "%s"
set format x "%Y-%m-%d"
set grid y
set ylabel "Commits"
set xtics rotate
set bmargin 6
plot 'commits_by_author.dat' using 1:2 title "Justine Mignot" w lines, 'commits_by_author.dat' using 1:3 title "Charlene Paviot" w lines, 'commits_by_author.dat' using 1:4 title "Rémi Duclau" w lines, 'commits_by_author.dat' using 1:5 title "mignotju" w lines, 'commits_by_author.dat' using 1:6 title "bakkalis" w lines, 'commits_by_author.dat' using 1:7 title "Charlène Paviot" w lines, 'commits_by_author.dat' using 1:8 title "Remi" w lines, 'commits_by_author.dat' using 1:9 title "gononl" w lines, 'commits_by_author.dat' using 1:10 title "leo Weschler" w lines, 'commits_by_author.dat' using 1:11 title "Gonon Lucas" w lines, 'commits_by_author.dat' using 1:12 title "Remi Duclau" w lines, 'commits_by_author.dat' using 1:13 title "grunenwflorian" w lines, 'commits_by_author.dat' using 1:14 title "unknown" w lines, 'commits_by_author.dat' using 1:15 title "Selim" w lines
