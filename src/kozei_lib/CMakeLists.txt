cmake_minimum_required(VERSION 2.8)

project(Source)


set(SOURCE_FILES Historical.cpp CouvertureDelta.cpp Data.cpp Simulation.cpp Portefeuille.cpp Produit.cpp Kozei.cpp Basket.cpp Call.cpp Asiatique.cpp Computations.cpp KozeiChange.cpp)
set(HEADERS Historical.hpp CouvertureDelta.hpp Data.hpp Simulation.hpp Portefeuille.hpp Produit.hpp Kozei.hpp Basket.hpp Call.hpp Asiatique.hpp Computations.hpp KozeiChange.hpp)


add_library(kozei_lib SHARED ${SOURCE_FILES} ${HEADERS})
target_link_libraries(kozei_lib traitement_lib ${LIBS})

find_package(Doxygen)
if(DOXYGEN_FOUND)
configure_file(${CMAKE_CURRENT_SOURCE_DIR}/docu/Doxyfile ${CMAKE_CURRENT_SOURCE_DIR}/docu/Doxyfile @ONLY)
add_custom_target(doc
${DOXYGEN_EXECUTABLE} ${CMAKE_CURRENT_SOURCE_DIR}/docu/Doxyfile
WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/docu
COMMENT "Generating API documentation with Doxygen" VERBATIM
)
endif(DOXYGEN_FOUND)