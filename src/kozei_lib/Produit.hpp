/*
 * File:   Produit.hpp
 * Author: lucas
 *
 * Created on December 12, 2016, 3:39 PM
 */

#ifndef PRODUIT_HPP
#define	PRODUIT_HPP

#include "pnl/pnl_random.h"

#include <list>

#include "Action.hpp"
#include "Data.hpp"

#define DLLEXP   __declspec( dllexport )

namespace Product
{
	class Produit
	{
	public:
		Data *data; /*!< Market date */
		list<Action> actions; /*!< List of shares related to the product */
		double I_; /*!< Price paid by the client for the product */
		PnlMat *path_; /*!< Matrix of spots */

		/*!
		 * \brief Constructor
		 * \param[in] data : market data used in the evaluation process
		 * \param[in] I : initial investment
		 */
		DLLEXP Produit(Data* data, double I);

		/*!
		 * \brief Destructor
		 */
		~Produit();

		/*!
		 * \brief Payoff
		 * \param[in] path : matrix of underlying spots to compute the payoff
		 */
		DLLEXP virtual double payoff(PnlMat* path) = 0;

		/*!
		 * \brief Price at 0
		 * \param[out] prix : price at 0
		 * \param[out] ic : confidence range
		 */
		DLLEXP void price(double &prix, double &ic);

		DLLEXP void price_change(double &prix, double &ic);

		/*!
		 * \brief Price at 0
		 * \param[in] past : matrix of past underlying spots
		 * \param[in] t : time to compute the price
		 * \param[out] prix : price at 0
		 * \param[out] ic : confidence range
		 */
		DLLEXP void price(const PnlMat *past, double t, double &prix, double &ic);

		DLLEXP virtual void price_change(const PnlMat *past, double t, double &prix, double &ic);

		/*!
		 * \brief Compute the width of the confidence range
		 * \param[in] variance : variance of our price
		 * \return width of the confidence range
		 */
		DLLEXP double getIntervalleConfiance(double variance);

		/*!
		 * \brief Compute the price from a sum of payoff updated to a past date
		 * \param[in] sum : sum of MonteCarlo payoffs
		 * \param[in] t : past date to update
		 * \return price updated
		 */
		DLLEXP double getPrice(double sum, double t);

		/*!
		 * \brief Compute the variance of our price
		 * \param[in] sum : sum of MonteCarlo payoffs
		 * \param[in] sum_square : sum of MonteCarlo squared payoffs
		 * \param[in] t : past date to update
		 * \return computed variance
		 */
		DLLEXP double getVariance(double sum, double sum_square, double t);
	};

}
#endif	/* PRODUIT_HPP */
