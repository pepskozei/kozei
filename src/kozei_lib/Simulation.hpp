/* 
 * File:   Simulation.hpp
 * Author: charlene
 *
 * Created on 23 décembre 2016, 16:23
 */

#ifndef SIMULATION_HPP
#define SIMULATION_HPP

#include "Data.hpp"

#define DLLEXP   __declspec( dllexport )

class Simulation : public Data
{
	public:
		/**
		 * \brief Default constructor
		 */
		DLLEXP Simulation();

		/**
		 * \brief Constructor with parameters
		 *
                 * \param[in] nbSamples number of MonteCarlo draws
		 * @param[in] nbTimeSteps  number of steps between t=0 and t=T (maturity)
		 * @param[in] size  number of underlying assets (equel to 30 in our case)
		 * @param[in] maturity  maturity date
		 * @param[in] r interest rate
		 * @param[in] rho  correlation parameter
		 * @param[in] spot pointer to a vector containing the initial prices of the underlying assets
		 * @param[in] sigma pointer to a vector containing the volatility of underlying assets
		 */
		DLLEXP Simulation(int nbSamples,
				int nbTimeSteps,
				int size,
				double maturity,
				double r,
				double rho,
				const PnlVect *spot,
				const PnlVect *sigma,
				const PnlVect * sigma_taux);

		/**
		 * \brief Destructor
		 */
		~Simulation();
		
	private:
		PnlMat* market_; /*! Market's spots */ 
};

#endif	/* SIMULATION_HPP */