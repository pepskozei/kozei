/*
 * File:   Asiatique.hpp
 * Author: mignotju
 *
 * Created on 12 janvier 2017, 16:20
 */

#ifndef ASIATIQUE_HPP
#define	ASIATIQUE_HPP

#include "pnl/pnl_random.h"

#include "Produit.hpp"

#include <list>

#define DLLEXP   __declspec( dllexport )

using namespace std;
using namespace Product;
class Asiatique : public Produit {
	public:
		double K_; /*!< Strike of the option */ 
		PnlVect* lambda; /*!< Composition of the basket */
		PnlVect* sommeStd; /*!< Vector used to compute the payoff */

                /*!
                 * \brief Constructor 
                 * \param[in] data : market data used in the evaluation process
                 * \param[in] I : initial investment
                 * \param[in] K : option's strike
                 * \param[in] lambda : composition of the option's basket
                 */
		DLLEXP Asiatique(Data* data, double I, double K, double lambda);
                
                /*!
                 * \brief Destructor
                 */
		~Asiatique();

                /*!
                 * \brief Payoff
                 * \param[in] path : matrix of underlying spots to compute the payoff
                 */
		DLLEXP double payoff(PnlMat* path);
};

#endif	/* ASIATIQUE_HPP */
