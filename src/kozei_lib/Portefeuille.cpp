#include "Portefeuille.hpp"

#include <cstdlib>
#include <iostream>

using namespace std;

Portefeuille::Portefeuille(Produit* produit, int nbTimeH, double fdStep)
{
    this->produit = produit;
    couvertureDelta = new CouvertureDelta(fdStep, produit);
    temps = 0;
    val_pf = pnl_vect_create(nbTimeH + 1);
    nbTimeStepH = nbTimeH;
}

Portefeuille::~Portefeuille()
{
	pnl_vect_free(&val_pf);
}

void Portefeuille::calcul_pf(double &erreur_couverture, PnlVect *price, PnlVect *ic)
{
    Data *data = produit->data;
    PnlVect * V = pnl_vect_create(nbTimeStepH + 1);

    double T = data->T_;
    int N = data->nbTimeSteps_;

    PnlMat *simul_market = pnl_mat_create(nbTimeStepH + 1, data->size_);

    data->asset(simul_market);

    //initialisation : calcul de V0
    double prix = 0;
    double intervalle_confiance = 0;
    PnlVect *delta = pnl_vect_create(data->size_);
    PnlVect *delta_prec = pnl_vect_create(data->size_);

    //Calcul de p0 dans prix
    produit->price(prix, intervalle_confiance);

    LET(price,0) = prix;
    LET(val_pf,0) = prix;
    LET(ic, 0) = intervalle_confiance;

    //Calcul de delta0
    PnlMat *past = pnl_mat_create(1, data->size_);
    pnl_mat_set_row(past, data->spot_, 0);

    couvertureDelta->calculDelta(delta, past, 0);
    pnl_vect_clone(delta_prec, delta);

    //Calcul du portefeuille de couverture en 0
    double V0 = prix - pnl_vect_scalar_prod(delta, data->spot_);

    LET(V, 0) = V0;

    PnlVect *cours_date = pnl_vect_new();
    PnlVect *copy_delta = pnl_vect_new();
    PnlVect *row_to_add = pnl_vect_new();

    for (int i = 1; i < nbTimeStepH + 1; i++)
	{
        if (data->nbTimeSteps_ != 0)
		{
            pnl_mat_get_row(row_to_add, simul_market, i);

            if (!((i * data->nbTimeSteps_) % nbTimeStepH == 0))
			{
                if ((((i - 1) * data->nbTimeSteps_) % nbTimeStepH == 0))
				{
                    pnl_mat_add_row(past, past->m, row_to_add);
                }
				else
				{
                    pnl_mat_set_row(past, row_to_add, past->m - 1);
                }
            }
			else
			{
                pnl_mat_set_row(past, row_to_add, past->m - 1);
            }

            //A décommenter si on veut remplir le vecteur de prix théoriques de l'option
            produit->price(past, i*T/nbTimeStepH, prix, intervalle_confiance);
            LET(price,i) = prix;
            LET(ic, i) = intervalle_confiance;
             
            //déterminer s'il y a rebalancement ou pas
            couvertureDelta->calculDelta(delta, past, i * T / nbTimeStepH);
            pnl_vect_clone(copy_delta, delta);
            pnl_vect_minus_vect(copy_delta, delta_prec);

            pnl_mat_get_row(cours_date, simul_market, i);

            LET(V, i) = GET(V, i - 1) * exp((data->r_ * T) / nbTimeStepH) -
                    pnl_vect_scalar_prod(copy_delta, cours_date);

            LET(val_pf, i) = GET(V, i) + pnl_vect_scalar_prod(delta, cours_date);

            pnl_vect_clone(delta_prec, delta);
        }
    }

    erreur_couverture = GET(V, nbTimeStepH) +
            pnl_vect_scalar_prod(delta, cours_date) - produit->payoff(simul_market);

    pnl_vect_free(&V);
    pnl_mat_free(&simul_market);
    pnl_vect_free(&delta);
    pnl_vect_free(&delta_prec);
    pnl_mat_free(&past);
    pnl_vect_free(&cours_date);
    pnl_vect_free(&copy_delta);
    pnl_vect_free(&row_to_add);
}

void Portefeuille::calcul_pf_ToNow(double &erreur_couverture, PnlVect *price, PnlVect *ic, PnlMat* past_ref, PnlVect* val_pf)
{
	Data *data = produit->data;
	PnlVect * V = pnl_vect_create(past_ref->m);

	double T = data->T_;
	PnlMat* past = pnl_mat_create(1, data->size_);

	//initialisation : calcul de V0
	double prix = 0;
	double intervalle_confiance = 0;
	PnlVect *delta = pnl_vect_create(data->size_);
	PnlVect *delta_prec = pnl_vect_create(data->size_);

	//Calcul de p0 dans prix
	produit->price(prix, intervalle_confiance);

	LET(price, 0) = prix;
	LET(val_pf, 0) = prix;
	LET(ic, 0) = intervalle_confiance;

	//Calcul de delta0
	PnlVect * spot = pnl_vect_create(past_ref->n);
	pnl_mat_get_row(spot, past_ref, 0);
	pnl_mat_set_row(past, spot, 0);

	couvertureDelta->calculDelta(delta, past, 0);

	pnl_vect_clone(delta_prec, delta);

	//Calcul du portefeuille de couverture en 0
	double V0 = prix - pnl_vect_scalar_prod(delta, spot);

	LET(V, 0) = V0;

	PnlVect *cours_date = pnl_vect_new();
	PnlVect *copy_delta = pnl_vect_new();
	PnlVect *row_to_add = pnl_vect_new();

	for (int i = 1; i < past_ref->m; i++)
	{
		if (data->nbTimeSteps_ != 0)
		{
			PnlVect * vect_to_add = pnl_vect_create(past_ref->n);
			pnl_mat_get_row(vect_to_add, past_ref, i);
			pnl_mat_add_row(past, i, vect_to_add);

			//A décommenter si on veut remplir le vecteur de prix théoriques de l'option
			produit->price(past, i*T / data->nbTimeSteps_, prix, intervalle_confiance);
			LET(price, i) = prix;
			LET(ic, i) = intervalle_confiance;

			//déterminer s'il y a rebalancement ou pas
			if (i % 10 == 0)
			{
				couvertureDelta->calculDelta(delta, past, i*T / data->nbTimeSteps_);
			}
			
			pnl_vect_clone(copy_delta, delta);
			pnl_vect_minus_vect(copy_delta, delta_prec);

			pnl_mat_get_row(cours_date, past_ref, i);

			LET(V, i) = GET(V, i - 1) * exp((data->r_ * T) / (past_ref->m - 1)) -
				pnl_vect_scalar_prod(copy_delta, cours_date);

			LET(val_pf, i) = GET(V, i) + pnl_vect_scalar_prod(delta, cours_date);

			pnl_vect_clone(delta_prec, delta);
		}
	}
}

void Portefeuille::calcul_pf_ToNow_change(double &erreur_couverture, PnlVect *price, PnlVect *ic, PnlMat* past_ref, PnlVect* val_pf, PnlVect* val_pfR)
{
	Data *data = produit->data;

	PnlVect * V = pnl_vect_create(past_ref->m);
	PnlVect * VR = pnl_vect_create(past_ref->m);

	double T = data->T_;
	int N = data->nbTimeSteps_;
	
	PnlMat* past = pnl_mat_create(1, data->size_);

	//initialisation : calcul de V0
	double prix = 0;
	double intervalle_confiance = 0;
	PnlVect *delta = pnl_vect_create_from_zero(data->size_);
	PnlVect *deltaR = pnl_vect_create_from_zero(data->size_);
	PnlVect *delta_prec = pnl_vect_create(data->size_);
	PnlVect *deltaR_prec = pnl_vect_create(data->size_);

	//Calcul de p0 dans prix
	produit->price_change(prix, intervalle_confiance);

	LET(price, 0) = prix;
	LET(val_pf, 0) = prix;
	LET(val_pfR, 0) = prix;
	LET(ic, 0) = intervalle_confiance;

	//Calcul de delta0
	PnlVect * spot = pnl_vect_create(data->size_);
	pnl_mat_get_row(spot, past_ref, 0);
	pnl_mat_set_row(past, spot, 0);

	couvertureDelta->calculDelta_change(delta, past, 0, data->size_ - data->nbDevises_);
	couvertureDelta->calculDelta_change(deltaR, past, 0, data->size_);

	pnl_vect_clone(delta_prec, delta);
	pnl_vect_clone(deltaR_prec, deltaR);

	//Calcul du portefeuille de couverture en 0
	double V0 = prix - pnl_vect_scalar_prod(delta, spot);
	double V0R = prix - pnl_vect_scalar_prod(deltaR, spot);

	LET(V, 0) = V0;
	LET(VR, 0) = V0R;

	PnlVect *cours_date = pnl_vect_new();
	PnlVect *copy_delta = pnl_vect_new();
	PnlVect *copy_deltaR = pnl_vect_new();
	PnlVect *row_to_add = pnl_vect_new();

	for (int i = 1; i < past_ref->m; i++)
	{
		if (data->nbTimeSteps_ != 0)
		{
			PnlVect * vect_to_add = pnl_vect_create(past_ref->n);
			pnl_mat_get_row(vect_to_add, past_ref, i);
			pnl_mat_add_row(past, i, vect_to_add);
			
			produit->price_change(past, i/365.0, prix, intervalle_confiance);

			LET(price, i) = prix;
			LET(ic, i) = intervalle_confiance;

			//déterminer s'il y a rebalancement ou pas
			if (i % 20 == 0)
			{
				couvertureDelta->calculDelta_change(delta, past, i/365.0, data->size_ - data->nbDevises_);
				couvertureDelta->calculDelta_change(deltaR, past, i / 365.0, data->size_);
			}

			pnl_vect_clone(copy_delta, delta);
			pnl_vect_minus_vect(copy_delta, delta_prec);
			pnl_vect_clone(copy_deltaR, deltaR);
			pnl_vect_minus_vect(copy_deltaR, deltaR_prec);

			pnl_mat_get_row(cours_date, past_ref, i);

			LET(V, i) = GET(V, i - 1) * exp((data->r_ * T) / (past_ref->m - 1)) -
				pnl_vect_scalar_prod(copy_delta, cours_date);
			LET(VR, i) = GET(VR, i - 1) * exp((data->r_ * T) / (past_ref->m - 1)) -
				pnl_vect_scalar_prod(copy_deltaR, cours_date);

			LET(val_pf, i) = GET(V, i) + pnl_vect_scalar_prod(delta, cours_date);
			LET(val_pfR, i) = GET(VR, i) + pnl_vect_scalar_prod(deltaR, cours_date);

			pnl_vect_clone(delta_prec, delta);
			pnl_vect_clone(deltaR_prec, deltaR);
		}
	}
}


void Portefeuille::calcul_prix_now_avec_change(int numJourDeltaConnu, double lastPartSansRisque,
	double lastPrice, double* delta, double &erreur_couverture, double &price, double &ic,
	PnlMat* past_ref, double &val_pf, double &val_pf_new)
{
	Data *data = produit->data;

	double T = data->T_;
	int N = data->nbTimeSteps_;

	double prix = 0;
	double intervalle_confiance = 0;

	if (data->nbTimeSteps_ != 0)
	{
		int indice_cour = past_ref->m - 1;

		produit->price_change(past_ref, indice_cour / 365.0, prix, intervalle_confiance);

		price = prix;
		ic = intervalle_confiance;

		PnlVect* cours_date = pnl_vect_create(past_ref->n);
		pnl_mat_get_row(cours_date, past_ref, indice_cour);

		PnlVect* lastSpots = pnl_vect_create(past_ref->n);
		pnl_mat_get_row(lastSpots, past_ref, numJourDeltaConnu);
		PnlVect* delta_prec = pnl_vect_create_from_ptr(past_ref->n, delta);

		double Vprec = lastPartSansRisque;

		//Sans rebalancement
		double Vcour = Vprec* exp(data->r_ * (indice_cour - numJourDeltaConnu) / 365.0);

		val_pf = Vprec + pnl_vect_scalar_prod(delta_prec, lastSpots);
		val_pf_new = Vcour + pnl_vect_scalar_prod(delta_prec, cours_date);
	}
}

void Portefeuille::calcul_delta_now_avec_change(int numJourDeltaConnu, double lastPartSansRisque,
	double lastPrice, double* delta, double &erreur_couverture, 
	PnlMat* past_ref, double* newDelta, int& newNumJourDernierRebal,
	double &newPartSansRisqueAvecRebal)
{
	Data *data = produit->data;

	double T = data->T_;
	int N = data->nbTimeSteps_;

	double prix = 0;
	double intervalle_confiance = 0;

	if (data->nbTimeSteps_ != 0)
	{
		int indice_cour = past_ref->m - 1;

		PnlVect* cours_date = pnl_vect_create(past_ref->n);
		pnl_mat_get_row(cours_date, past_ref, indice_cour);

		PnlVect* lastSpots = pnl_vect_create(past_ref->n);
		pnl_mat_get_row(lastSpots, past_ref, numJourDeltaConnu);
		PnlVect* delta_prec = pnl_vect_create_from_ptr(past_ref->n, delta);

		double Vprec = lastPartSansRisque;

		//Avec rebalancement
		PnlVect *deltaR = pnl_vect_create_from_zero(data->size_);
		couvertureDelta->calculDelta_change(deltaR, past_ref, indice_cour / 365.0, data->size_);

		PnlVect *copy_delta = pnl_vect_new();
		pnl_vect_clone(copy_delta, deltaR);
		pnl_vect_minus_vect(copy_delta, delta_prec);
		double Vcour_new = Vprec* exp(data->r_ * (indice_cour - numJourDeltaConnu) / 365.0) - pnl_vect_scalar_prod(copy_delta, cours_date);

		for (int i = 0; i < deltaR->size; i++)
		{
			newDelta[i] = GET(deltaR, i);
		}

		newNumJourDernierRebal = indice_cour;
		newPartSansRisqueAvecRebal = Vcour_new;
	}
}



void Portefeuille::calcul_delta_en_t(int nbRebal, PnlMat* past_ref, PnlVect* delta)
{
	Data *data = produit->data;

	double T = data->T_;
	int N = data->nbTimeSteps_;

	if (data->nbTimeSteps_ != 0)
	{
		int i = past_ref->m - 1;

		int j;
		if (i % nbRebal == 0)
		{
			j = i - nbRebal;
		}
		else
		{
			j = i / nbRebal;
			j = j * nbRebal;
		}
		
		couvertureDelta->calculDelta_change(delta, past_ref, j*365.0, data->size_);
	}
}