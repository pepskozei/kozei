#include "Historical.hpp"

#include <fstream>
#include <iostream>
#include <string>
#include <sstream>
#include <algorithm>

using namespace std;

Historical::Historical()
{
	market_ = pnl_mat_new();
}

Historical::Historical(int nbSamples, int nbTimeSteps, int size,
	double maturity, double r, double rho, const PnlVect *spot,
	const PnlVect *sigma, const PnlVect * sigma_taux) : Data(nbSamples, nbTimeSteps, size,
	maturity, r, rho, spot, sigma, sigma_taux)
{
	//Simulation of the market
	market_ = pnl_mat_create(nbTimeSteps + 1, size);
	asset(market_);
}

vector<PnlVect *> Historical::getHistoricalData()
{
	vector<PnlVect *> listAssets = vector<PnlVect *>();
	vector<double> listPrices = vector<double>();
	
	ifstream fichier("..\\..\\peps\\application\\bin\\x64\\Debug\\Test_yahoo\\cours.csv", ios::in);  // on ouvre le fichier en lecture
	
	if (fichier)  // si l'ouverture a r�ussi
	{
		string contenu;  // d�claration d'une cha�ne qui contiendra la ligne lue
		string cours_string;
		
		getline(fichier, contenu);

		int i = 0;
		int j = 1;
		PnlVect * vectCours;
		string cours_name = "DHR";
		while (getline(fichier, contenu))
		{
			istringstream iss(contenu);
			getline(iss, cours_string, ';');

			if (cours_string != cours_name)
			{
				j++;
				vectCours = pnl_vect_create(listPrices.size());
				for (int k = 0; k < listPrices.size(); k++)
				{
					LET(vectCours, k) = listPrices[k];
				}
				pnl_vect_reverse(vectCours);
				listAssets.push_back(vectCours);
				listPrices.clear();
			}
			cours_name = cours_string;
	
			getline(iss, cours_string, ';');
			
			while (getline(iss, cours_string, ';'))
			{
				replace(cours_string.begin(), cours_string.end(), ',', '.');
				double cours = stod(cours_string);
				listPrices.push_back(cours);
			}
		}

		vectCours = pnl_vect_create(listPrices.size());
		
		for (int k = 0; k < listPrices.size(); k++)
		{
			LET(vectCours, k) = listPrices[k];
		}
		
		pnl_vect_reverse(vectCours);
		listAssets.push_back(vectCours);

		fichier.close();  // on ferme le fichier		
	}
	else  // sinon
	{
		cerr << "Impossible d'ouvrir le fichier !" << endl;
	}

	return listAssets;
}

void Historical::getHistoricalExchangeRate(PnlMat * exchange_mat)
{
	ifstream file("..\\..\\peps\\application\\bin\\x64\\Debug\\Test_yahoo\\recupFxTop_2014.csv", ios::in);

	if (file)  // si l'ouverture a r�ussi
	{
		string contenu;  // d�claration d'une cha�ne qui contiendra la ligne lue
		int nb_lignes = 0;
		int n = 0;

		getline(file, contenu);
		while (getline(file, contenu)){  // on met dans "contenu" la ligne
			n = count(contenu.begin(), contenu.end(), ';');
			nb_lignes++;
		}

		string cours_string;

		pnl_mat_resize(exchange_mat, nb_lignes, n-1);
		int i = 0;

		file.clear();
		file.seekg(0, ios::beg);

		getline(file, contenu);
		int j = nb_lignes-1;
		while (getline(file, contenu)) // on met dans "contenu" la ligne
		{ 
			istringstream iss(contenu);
			
			getline(iss, cours_string, ';');
			getline(iss, cours_string, ';');

			i = 0;
			
			while (getline(iss, cours_string, ';'))
			{
				replace(cours_string.begin(), cours_string.end(), ',', '.');
				double cours = stod(cours_string);
				MLET(exchange_mat, j, i) = cours;
				i++;
			}
			j--;
		}

		file.close();  // on ferme le fichier
	}
	else  // sinon
	{
		cerr << "Impossible d'ouvrir le fichier !" << endl;
	}
}

Historical::~Historical()
{
	pnl_mat_free(&market_);
}