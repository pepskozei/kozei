#include "KozeiChange.hpp"
#include "pnl/pnl_random.h"

using namespace Product;
KozeiChange::KozeiChange(Data* data, double I) : Produit(data, I)
{
	// Cours en 0 pour calculer performance
	cours0_ = pnl_vect_new();
	perfTmp_ = pnl_vect_new();

	constat_.clear();
	constat_.push_back(185);
	constat_.push_back(367);
	constat_.push_back(549);
	constat_.push_back(731);
	constat_.push_back(915);
	constat_.push_back(1096);
	constat_.push_back(1280);
	constat_.push_back(1461);
	constat_.push_back(1645);
	constat_.push_back(1826);
	constat_.push_back(2012);
	constat_.push_back(2194);
	constat_.push_back(2376);
	constat_.push_back(2558);
	constat_.push_back(2741);
	constat_.push_back(2922);

	init_.clear();
	init_.push_back(0);
	init_.push_back(4);
	init_.push_back(5);
}

KozeiChange::~KozeiChange()
{
	pnl_vect_free(&cours0_);
	pnl_vect_free(&perfTmp_);
}

//on consid�re que la matrice que l'on re�oit est exactement de taille 17
double KozeiChange::payoff(PnlMat* path)
{
	int nbActifs = 30;
	double step = data->T_/data->nbTimeSteps_;

	pnl_vect_resize(perfTmp_, path->n);
	double avgGlobal = 0;

	pnl_mat_get_row(cours0_, path, 0);

	for (int i = 1; i < path->m; i++)
	{
		double t = i*step;
		pnl_mat_get_row(perfTmp_, path, i);

		//perfTmp_ contient les SiXi puis les riXi

		pnl_vect_div_vect_term(perfTmp_, cours0_);

		//perfTmp_ contient les SiXi/S0X0 puis les riXi/r0X0
		
		for (int j = 0; j < nbActifs; j++)
		{
			int currency_index;
			data->getDevise(currency_index, j);

			if (currency_index != 4)
			{
				LET(perfTmp_, j) /= GET(perfTmp_, nbActifs + currency_index);
				LET(perfTmp_, j) *= exp(GET(data->taux_sans_risque_etranger_, currency_index)*t);
			}

			LET(perfTmp_, j) -= 1;
		}

		//perfTmp_ contient (Si/S0) - 1 puis les riXi/r0X0

		// On ajoute � la performance totale, la moyenne des performances des titres a chaque date de constatation
		// Seulement si c'est positif
		double avgPerf = 0;

		for (int j = 0; j < nbActifs; j++)
		{
			avgPerf += GET(perfTmp_, j);
		}

		avgPerf /= nbActifs;

		if (avgPerf > 0)
		{
			avgGlobal += avgPerf;
		}
	}

	avgGlobal /= data->nbTimeSteps_;

	double renta = 0;

	if (avgGlobal > 0)
	{
		renta = avgGlobal;
	}

	renta *= 0.6;

	return (I_*(1 + renta));
}

void KozeiChange::init(double & dt, PnlMat* path, const PnlMat* past)
{
	double step = data->T_ / data->nbTimeSteps_;

	pnl_mat_resize(path,1,data->size_);

	PnlVect* last_row = pnl_vect_create(past->n);
	PnlVect* tmpCours = pnl_vect_create(past->n);
	pnl_mat_get_row(last_row, past, (past->m) - 1);
	
	//remplissage de la ligne 0
	for (int j = 0; j < past->n; j++)
	{
		if (past->m <= init_.back())
		{
			MLET(path, 0, j) = MGET(past, 0, j);
		}
		else
		{
			for (int i = 0; i < init_.size(); i++)
			{
				MLET(path, 0, j) += MGET(past, init_[i], j);
			}
			MLET(path, 0, j) /= 3;
		}
	}

	//remplissage des dates de constatations pass�es
	for (int i = 0; i < constat_.size(); i++)
	{
		if (past->m > constat_[i])
		{
			for (int j = 0; j < past->n; j++)
			{
				LET(tmpCours, j) = MGET(past, constat_[i], j);
			}

			pnl_mat_add_row(path, path->m, tmpCours);

			if (past->m == (constat_[i] + 1))
			{
				dt = 0;

				pnl_vect_free(&last_row);
				pnl_vect_free(&tmpCours);
				return;
			}
		} 
		else
		{
			dt = ((double) (constat_[i] - (past->m - 1)))/ 365.0;
			pnl_mat_add_row(path, path->m, last_row);

			pnl_vect_free(&last_row);
			pnl_vect_free(&tmpCours);
			return;
		}
	}
}

void KozeiChange::price_change(const PnlMat *past, double t, double &prix, double &ic)
{
	double sum = 0;
	double tmp = 0;
	double sum_square = 0;
	double variance = 0;
	double payoff_res = 0;

	PnlMat* myPast = pnl_mat_create(data->nbTimeSteps_ + 1, data->size_);
	PnlMat* path = pnl_mat_create(data->nbTimeSteps_ + 1, data->size_);
	double dt;

	init(dt, myPast, past);

	for (int i = 0; i < data->nbSamples_; i++)
	{
		data->assetTauxChangeDt(path, dt, myPast);

		payoff_res = payoff(path);

		sum += payoff_res;
		sum_square += pow(payoff_res, 2);
	}

	pnl_mat_free(&path);

	tmp = sum;
	variance = getVariance(tmp, sum_square, t);
	prix = getPrice(sum, t);
	ic = getIntervalleConfiance(variance);
}