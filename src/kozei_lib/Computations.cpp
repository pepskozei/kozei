#include "Computations.hpp"
#include <iostream>
#include <time.h>

using namespace std;

void Computations::retrieveCoursActifs(int numJourDernierRebal, double* coursActifsCourant, 
	double* coursActifsDernierRebal, int nbActifPlusDevise,
	RetrieveData* retrieve_data, vector<vector<DataStructure>*>* vect_cours,
	vector<vector<DataStructure>*>* vect_change)
{
	vector<vector<DataStructure>*> vectToFill_cours = vector<vector<DataStructure>*>();
	vector<vector<DataStructure>*> vectToFill_change = vector<vector<DataStructure>*>();
	vector<vector<DataStructure>*> result_concat = vector<vector<DataStructure>*>();

	PnlMat* past = pnl_mat_new();

	//on met les matrices de cours et change � la m�me taille
	retrieve_data->fillAllVectsChange(vectToFill_cours, vectToFill_change, *vect_cours, *vect_change);

	//on calcule le vector de vector des SX, on r�cup�re dans vectoFill_cours
	retrieve_data->productAllExchange(vectToFill_cours, vectToFill_change);

	int nbDevises = 4;
	PnlVect * taux_sans_risque_etranger = pnl_vect_create(nbDevises);
	LET(taux_sans_risque_etranger, 0) = 0.0068667;
	LET(taux_sans_risque_etranger, 1) = 0.0002429;
	LET(taux_sans_risque_etranger, 2) = 0.0022500;
	LET(taux_sans_risque_etranger, 3) = 0.01891;

	//on calcule le vector de vector des RX, on r�cup�re dans vectToFill_change
	retrieve_data->domesticRates(vectToFill_change, taux_sans_risque_etranger);

	//concat�nation des SX et des RX
	retrieve_data->concatVect(result_concat, vectToFill_cours, vectToFill_change);

	//on r�cup�re une PnlMat past
	retrieve_data->convertDataStructToPnl(past, result_concat);

	PnlVect* coursActifsCour = pnl_vect_create(nbActifPlusDevise);
	pnl_mat_get_row(coursActifsCour, past, past->m - 1);

	for (int i = 0; i < coursActifsCour->size; i++)
	{
		coursActifsCourant[i] = GET(coursActifsCour, i);
	}

	PnlVect* coursActifsDernierReb = pnl_vect_create(nbActifPlusDevise);
	pnl_mat_get_row(coursActifsDernierReb, past, numJourDernierRebal);

	for (int i = 0; i < coursActifsDernierReb->size; i++)
	{
		coursActifsDernierRebal[i] = GET(coursActifsDernierReb, i);
	}
}

void Computations::pricer(int numJourDernierRebal, double lastPartSansRisque, double lastPrice, 
	double* delta, PnlVect* volatility, PnlMat* corr, PnlMat* past, int nbDevises, 
	PnlVect* taux_sans_risque_etranger, double& price, double& ic, double& val_pf, double& val_pf_new)
{
	PnlVect* spot = pnl_vect_new();
	pnl_mat_get_row(spot, past, 0);

	int nbTimeSteps = 16;
	int size = 30;
	double maturity = 8.0;
	double r = 0.01039; //taux sans risque domestique (EUR)
	int nbSamples = 1000;

	PnlVect * sigma_taux = pnl_vect_create(nbDevises + 1);
	PnlVect * tmp = pnl_vect_new();
	PnlVect * sigma = pnl_vect_new();
	pnl_vect_extract_subvect(sigma, volatility, 0, 30);
	pnl_vect_print(sigma);
	pnl_vect_extract_subvect(tmp, volatility, 30, 4);
	pnl_vect_print(tmp);

	for (int i = 0; i < tmp->size; i++)
	{
		LET(sigma_taux, i) = GET(tmp, i);
	}
	LET(sigma_taux, 4) = 0;
	pnl_vect_print(sigma_taux);
	PnlVect * spot_taux = pnl_vect_new();

	Data * data = new Data(nbSamples, nbTimeSteps, size, maturity, r, corr, spot, volatility, sigma_taux,
		taux_sans_risque_etranger, nbDevises, spot_taux);

	double I = 100;
	double fdstep = 0.01;
	Produit *produit = new KozeiChange(data, I);
	Portefeuille *pf = new Portefeuille(produit, 0, fdstep);
	double erreur_couverture;

	pf->calcul_prix_now_avec_change(numJourDernierRebal, lastPartSansRisque, lastPrice, delta, erreur_couverture, price, ic, past, val_pf, val_pf_new);
}


void Computations::pricerProduit(int numJourDernierRebal, double lastPartSansRisque, double lastPrice, 
	double* delta, double& prix, double& ic, double& val_pf, double& val_pf_new, double* coursActifs,
	int nbActifPlusDevise,
	RetrieveData* retrieve_data, vector<vector<DataStructure>*>* vect_cours,
	vector<vector<DataStructure>*>* vect_change, double* volatilites, double** mat_corr)
{
	vector<vector<DataStructure>*> vectToFill_cours = vector<vector<DataStructure>*>();
	vector<vector<DataStructure>*> vectToFill_change = vector<vector<DataStructure>*>();
	vector<vector<DataStructure>*> result_concat = vector<vector<DataStructure>*>();

	PnlMat* past = pnl_mat_new();

	//on met les matrices de cours et change � la m�me taille
	retrieve_data->fillAllVectsChange(vectToFill_cours, vectToFill_change, *vect_cours, *vect_change);

	//on calcule le vector de vector des SX, on r�cup�re dans vectoFill_cours
	retrieve_data->productAllExchange(vectToFill_cours, vectToFill_change);

	int nbDevises = 4;
	PnlVect * taux_sans_risque_etranger = pnl_vect_create(nbDevises);
	LET(taux_sans_risque_etranger, 0) = 0.0068667;
	LET(taux_sans_risque_etranger, 1) = 0.0002429;
	LET(taux_sans_risque_etranger, 2) = 0.0022500;
	LET(taux_sans_risque_etranger, 3) = 0.01891;

	//on calcule le vector de vector des RX, on r�cup�re dans vectToFill_change
	retrieve_data->domesticRates(vectToFill_change, taux_sans_risque_etranger);

	//concat�nation des SX et des RX
	retrieve_data->concatVect(result_concat, vectToFill_cours, vectToFill_change);

	//on r�cup�re une PnlMat past
	retrieve_data->convertDataStructToPnl(past, result_concat);

	PnlVect* coursActifsVect = pnl_vect_create(nbActifPlusDevise);
	pnl_mat_get_row(coursActifsVect, past, past->m - 1);

	for (int i = 0; i < coursActifsVect->size; i++)
	{
		coursActifs[i] = GET(coursActifsVect, i);
	}

	PnlVect* volat = pnl_vect_create(nbActifPlusDevise);
	for (int i = 0; i < nbActifPlusDevise; i++)
	{
		LET(volat, i) = volatilites[i];
	}

	PnlMat* matCorr = pnl_mat_create(nbActifPlusDevise, nbActifPlusDevise);
	for (int i = 0; i < nbActifPlusDevise; i++)
	{
		for (int j = 0; j < nbActifPlusDevise; j++)
		{
			MLET(matCorr, i, j) = mat_corr[i][j];
		}
	}

	Computations::pricer(numJourDernierRebal, lastPartSansRisque, lastPrice, delta, volat, matCorr, past, nbDevises, taux_sans_risque_etranger, prix, ic, val_pf, val_pf_new);
}

void Computations::calculDeltaInterne(int numJourDernierRebal, double lastPartSansRisque, 
	double lastPrice, double* delta, PnlVect* volatility, PnlMat* corr, double* newDelta, 
	PnlMat* past, int nbDevises, PnlVect* taux_sans_risque_etranger, int& newNumJourDernierRebal, 
	double& newPartSansRisqueAvecRebal)
{
	PnlVect* spot = pnl_vect_new();
	pnl_mat_get_row(spot, past, 0);

	int nbTimeSteps = 16;
	int size = 30;
	double maturity = 8.0;
	double r = 0.01039; //taux sans risque domestique (EUR)
	int nbSamples = 1000;

	PnlVect * sigma_taux = pnl_vect_create(nbDevises + 1);
	PnlVect * tmp = pnl_vect_new();
	PnlVect * sigma = pnl_vect_new();
	pnl_vect_extract_subvect(sigma, volatility, 0, 30);
	pnl_vect_print(sigma);
	pnl_vect_extract_subvect(tmp, volatility, 30, 4);
	pnl_vect_print(tmp);

	for (int i = 0; i < tmp->size; i++)
	{
		LET(sigma_taux, i) = GET(tmp, i);
	}
	
	LET(sigma_taux, 4) = 0;
	pnl_vect_print(sigma_taux);
	PnlVect * spot_taux = pnl_vect_new();

	Data * data = new Data(nbSamples, nbTimeSteps, size, maturity, r, corr, spot, volatility, sigma_taux,
		taux_sans_risque_etranger, nbDevises, spot_taux);

	double I = 100;
	double fdstep = 0.01;
	Produit *produit = new KozeiChange(data, I);
	Portefeuille *pf = new Portefeuille(produit, 0, fdstep);
	double erreur_couverture;

	pf->calcul_delta_now_avec_change(numJourDernierRebal, lastPartSansRisque, lastPrice, delta, erreur_couverture, past, newDelta, newNumJourDernierRebal, newPartSansRisqueAvecRebal);
}

void Computations::calculDelta(int numJourDernierRebal, double lastPartSansRisque, double lastPrice, double* delta,
	int& newNumJourDernierRebal, double& newPartSansRisqueAvecRebal, int nbActifPlusDevise,
	RetrieveData* retrieve_data, vector<vector<DataStructure>*>* vect_cours,
	vector<vector<DataStructure>*>* vect_change, double* volatilites, double** mat_corr, double* newDelta)
{
	vector<vector<DataStructure>*> vectToFill_cours = vector<vector<DataStructure>*>();
	vector<vector<DataStructure>*> vectToFill_change = vector<vector<DataStructure>*>();
	vector<vector<DataStructure>*> result_concat = vector<vector<DataStructure>*>();

	PnlMat* past = pnl_mat_new();

	//on met les matrices de cours et change � la m�me taille
	retrieve_data->fillAllVectsChange(vectToFill_cours, vectToFill_change, *vect_cours, *vect_change);

	//on calcule le vector de vector des SX, on r�cup�re dans vectoFill_cours
	retrieve_data->productAllExchange(vectToFill_cours, vectToFill_change);

	int nbDevises = 4;
	PnlVect * taux_sans_risque_etranger = pnl_vect_create(nbDevises);
	LET(taux_sans_risque_etranger, 0) = 0.0068667;
	LET(taux_sans_risque_etranger, 1) = 0.0002429;
	LET(taux_sans_risque_etranger, 2) = 0.0022500;
	LET(taux_sans_risque_etranger, 3) = 0.01891;

	//on calcule le vector de vector des RX, on r�cup�re dans vectToFill_change
	retrieve_data->domesticRates(vectToFill_change, taux_sans_risque_etranger);

	//concat�nation des SX et des RX
	retrieve_data->concatVect(result_concat, vectToFill_cours, vectToFill_change);

	//on r�cup�re une PnlMat past
	retrieve_data->convertDataStructToPnl(past, result_concat);

	PnlVect* volat = pnl_vect_create(nbActifPlusDevise);
	
	for (int i = 0; i < nbActifPlusDevise; i++)
	{
		LET(volat, i) = volatilites[i];
	}

	PnlMat* matCorr = pnl_mat_create(nbActifPlusDevise, nbActifPlusDevise);
	for (int i = 0; i < nbActifPlusDevise; i++)
	{
		for (int j = 0; j < nbActifPlusDevise; j++)
		{
			MLET(matCorr, i, j) = mat_corr[i][j];
		}
	}

	Computations::calculDeltaInterne(numJourDernierRebal, lastPartSansRisque, lastPrice, delta, volat, matCorr, newDelta, past, nbDevises, taux_sans_risque_etranger, newNumJourDernierRebal, newPartSansRisqueAvecRebal);
}