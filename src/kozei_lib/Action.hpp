/* 
 * File:   Action.hpp
 * Author: charlene
 *
 * Created on 23 décembre 2016, 16:22
 */

#ifndef ACTION_HPP
#define	ACTION_HPP

#include <iostream>

using namespace std;

class Action
{
public:
    string name; /*!< Name of the underlying company within the share */
};


#endif	/* ACTION_HPP */

