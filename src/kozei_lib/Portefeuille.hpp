/*
 * File:   Portefeuille.hpp
 * Author: charlene
 *
 * Created on 23 décembre 2016, 15:54
 */

#ifndef PORTEFEUILLE_HPP
#define	PORTEFEUILLE_HPP

#include "Produit.hpp"
#include "CouvertureDelta.hpp"

#define DLLEXP   __declspec( dllexport )

class Portefeuille
{
public:
    Produit *produit; /*!< Product related to our pricing */
    CouvertureDelta *couvertureDelta; /*!< Hedging strategy */
    double temps; /*!< Time */
    PnlVect* val_pf; /*!< Values of the portfolio */
    int nbTimeStepH; /*!< Number of balancing dates */

    /*!
     * \brief Constructor
     * \param[in] produit : product to price  
     * \param[in] nbTimeH: number of balancing dates
     * \param[in] fdStep : finite difference step 
     */
	DLLEXP Portefeuille(Produit* produit, int nbTimeH, double fdStep);

    /*!
     * \brief Destructor
     */
    ~Portefeuille();

    //Calcule l'erreur de couverture pour une couverture en delta
	//et le prix du portefeuille à chaque instant
    
    /*!
     * \brief Method to compute our portfolio
     * \param[out] erreur_couverture : error made with our hedging strategy
     * \param[out] price : vector of prices
     * \param[out] ic : confidence range
     */
	DLLEXP void calcul_pf(double &erreur_couverture, PnlVect *price, PnlVect *ic);
    
    /*!
     * \brief Method to compute our portfolio in a deterministic way to test our standard method 
     * \param[out] erreur_couverture : error made with our hedging strategy
     * \param[out] price : vector of prices
     * \param[in] G : vector that represents the random part of our model
     */
	DLLEXP void calcul_pf_deterministe(double &erreur_couverture, PnlVect *price, PnlVect *G);
    
    /*!
     * \brief 
     * \param[in] step : 
     */
	DLLEXP void nextStep(double step);
	DLLEXP void calcul_pf_ToNow(double &erreur_couverture, PnlVect *price, PnlVect *ic, PnlMat* past, PnlVect* val_pf);
	DLLEXP void calcul_pf_ToNow_change(double &erreur_couverture, PnlVect *price, PnlVect *ic, 
		PnlMat* past_ref, PnlVect* val_pf, PnlVect* val_pfR);

	// Pour interface
	DLLEXP void calcul_prix_now_avec_change(int numJourDeltaConnu, double lastPartSansRisque,
		double lastPrice, double* delta, double &erreur_couverture, double &price, double &ic,
		PnlMat* past_ref, double &val_pf, double &val_pf_new);
	DLLEXP void calcul_delta_now_avec_change(int numJourDeltaConnu, double lastPartSansRisque,
		double lastPrice, double* delta, double &erreur_couverture,
		PnlMat* past_ref, double* newDelta, int& newNumJourDernierRebal,
		double &newPartSansRisqueAvecRebal);
	DLLEXP void calcul_delta_en_t(int nbRebal, PnlMat* past_ref, PnlVect* delta);
};

#endif	/* PORTEFEUILLE_HPP */