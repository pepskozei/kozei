/* 
 * File:   Kozei.hpp
 * Author: charlene
 *
 * Created on 23 décembre 2016, 16:20
 */

#ifndef KOZEI_HPP
#define	KOZEI_HPP

#include "pnl/pnl_random.h"

#include "Produit.hpp"

#include <vector>
#include <list>

#define DLLEXP   __declspec( dllexport )

using namespace Product;

class Kozei : public Produit
{
public:
	vector<int> constat_;
	vector<int> init_;
	
    /*!
     * \brief Constructor 
     * \param[in] data : market data used in the evaluation process
     * \param[in] I : initial investment
     */
	DLLEXP Kozei(Data* data, double I);
                
    /*!
     * \brief Destructor
     */
	~Kozei();

    /*!
     * \brief Payoff
     * \param[in] path : matrix of underlying spots to compute the payoff
     */
	DLLEXP double payoff(PnlMat* path);

	DLLEXP void init(PnlVect* cours0, PnlMat* path);
};

#endif	/* KOZEI_HPP */

