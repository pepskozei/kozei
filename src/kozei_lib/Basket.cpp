#include "Basket.hpp"

#include "pnl/pnl_random.h"

using namespace Product;

Basket::Basket(Data* data, double I, double strike, double lbd) : Produit(data, I)
{
	K_ = strike;
	lambda = pnl_vect_create_from_scalar(data->size_, lbd);
}

double Basket::payoff(PnlMat* path)
{
	double payoff = 0;
	PnlVect *ST = pnl_vect_create(data->size_);
	PnlVect *last_row = pnl_vect_create(data->size_);
	pnl_mat_get_row(last_row, path, path->m - 1);
	pnl_vect_clone(ST, last_row);

	pnl_mat_get_row(ST, path, path->m - 1);
	payoff = pnl_vect_scalar_prod(lambda, ST);

	payoff -= K_;
	if (payoff < 0)
	{
		return 0;
	}
	else
	{
		return payoff;
	}
}
