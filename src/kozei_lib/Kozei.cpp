#include "Kozei.hpp"

#include "pnl/pnl_random.h"

using namespace Product;

Kozei::Kozei(Data* data, double I) : Produit(data, I)
{
	constat_.clear();
	constat_.push_back(185);
	constat_.push_back(367);
	constat_.push_back(549);
	constat_.push_back(731);
	constat_.push_back(915);
	constat_.push_back(1096);
	constat_.push_back(1280);
	constat_.push_back(1461);
	constat_.push_back(1645);
	constat_.push_back(1826);
	constat_.push_back(2012);
	constat_.push_back(2194);
	constat_.push_back(2376);
	constat_.push_back(2558);
	constat_.push_back(2741);
	constat_.push_back(2922);

	init_.clear();
	init_.push_back(0);
	init_.push_back(4);
	init_.push_back(5);
}

Kozei::~Kozei() {}

//on considère que la matrice que l'on reçoit est exactement de taille 2923 (nbJours+1)
double Kozei::payoff(PnlMat* path)
{
	// Cours en 0 pour calculer performance
	PnlVect* cours0 = pnl_vect_new();
	
	init(cours0, path);

	PnlVect *perf_tmp = pnl_vect_create(path->n);
	double avgGlobal = 0;

	for (int i = 0; i < constat_.size(); i++)
	{
		pnl_mat_get_row(perf_tmp, path, constat_[i]);
		pnl_vect_minus_vect(perf_tmp, cours0);
		pnl_vect_div_vect_term(perf_tmp, cours0);

		// On ajoute à la performance totale, la moyenne des performances des titres a chaque date de constatation
		// Seulement si c'est positif
		if ((pnl_vect_sum(perf_tmp) / path->n) > 0)
		{
			avgGlobal += (pnl_vect_sum(perf_tmp) / path->n);
		}
	}

	avgGlobal /= constat_.size();

	double renta = 0;

	if (avgGlobal > 0)
	{
		renta = avgGlobal;
	}

	renta *= 0.6;

	pnl_vect_free(&cours0);
	pnl_vect_free(&perf_tmp);

	return (I_*(1 + renta));
}

void Kozei::init(PnlVect* cours0, PnlMat* path)
{
	pnl_vect_resize(cours0, path->n);
	pnl_vect_set_zero(cours0);

	for (int i = 0; i < init_.size(); i++)
	{
		for (int j = 0; j < path->n; j++)
		{
			LET(cours0, j) += MGET(path, init_[i], j);
		}
	}

	pnl_vect_div_scalar(cours0, 3);
}

