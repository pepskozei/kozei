/*
 * File:   CouvertureDelta.hpp
 * Author: charlene
 *
 * Created on 23 décembre 2016, 16:20
 */

#ifndef COUVERTUREDELTA_HPP
#define	COUVERTUREDELTA_HPP

#include "pnl/pnl_random.h"

#include "Data.hpp"
#include "Produit.hpp"

#include <vector>

#define DLLEXP   __declspec( dllexport )

using namespace Product;

class CouvertureDelta
{
public:
	PnlMat *shiftPlus_; /*!< Matrix used to compute deltas */
	PnlMat *shiftMoins_; /*!< Matrix used to compute deltas */
	Produit* produit_; /*!< Product related to our hedging */
	double fdStep_; /*!< Finite difference step */

	vector<int> constat_;
	vector<int> init_;

    /*!
    * \brief Constructor 
    * \param[in] fdStep : finite difference step
    * \param[in] produit : product associated with our hedging strategy
    */
	DLLEXP CouvertureDelta(double fdStep, Produit *produit);
                
    /*!
    * \brief Destructor
    */
	~CouvertureDelta();

    /*!
    * \brief Shifting matrix
    * \param[out] shiftPlus_ : matrix that has been shifted 
    * \param[in] path_ : matrix that has to be shifted
    * \param[in] actifToShift : index of the asset to shift (line)
    * \param[in] coeff : finite difference step
    * \param[in] time : time to begin the shifting operation (column)
    * \param[in] maturity : maturity
    */
	DLLEXP void shiftAsset(PnlMat* shiftPlus_, PnlMat* path_, int actifToShift,
			double coeff, double time, double maturity);

    /*!
    * \brief Delta computation
    * \param[out] delta : vector that contains the computed deltas 
    * \param[in] past : matrix of spots prices
    * \param[in] t : time to compute
    */
	DLLEXP void calculDelta(PnlVect* delta, const PnlMat *past, double t);

	DLLEXP void calculDelta_change(PnlVect* delta, const PnlMat *past, double t, int size);

    /*!
    * \brief Delta calculation (deterministic computation)
    * \param[out] delta : vector that contains the computed deltas 
    * \param[in] past : matrix of spots past prices
    * \param[in] t : time to compute
    * \param[in] vect : vector that fix the random part of our computation
    */
	DLLEXP void calculDeltaDeterministe(PnlVect* delta, const PnlMat* past,
				double t, PnlVect* vect);

    /*!
    * \brief Rebalancing decision
    * \return true if the fund owner wants to rebalance his portfolio
    */
	DLLEXP bool oracle();
	DLLEXP void init(double & dt, PnlMat* path, const PnlMat* past);
};
#endif	/* COUVERTUREDELTA_HPP */
