#include "Call.hpp"

#include "pnl/pnl_random.h"

using namespace Product;

Call::Call(Data* data, double I, double K) : Produit(data, I)
{
	K_ = K;
}

Call::~Call() {}

//On considère que le call est unidimensionnel
double Call::payoff(PnlMat* path)
{
	double ST = MGET(path, path->m-1, 0);
    
	double payoff = ST-K_;
	if (payoff <= 0)
	{
		payoff = 0;
	}	

	return payoff;
}

double Call::N(double z)
{
	/*Comme la fonction de repartition de la loi normale ne peut etre explicitée, on l'approche par une
	 approximation de Abromowitz
	 Plus de détails : http://www.lamsade.dauphine.fr/~manouvri/C++/M2ISF/SujetTP_C++VBA20092010.pdf */

	if (z > 6.0)
		return 1.0;  // éviter les valeurs illicites
	if (z < -6.0)
		return 0.0;
	double b1 = 0.31938153;
	double b2 = -0.356563782;
	double b3 = 1.781477937;
	double b4 = -1.821255978;
	double b5 = 1.330274429;
	double p = 0.2316419;
	double c2 = 0.3989423;
	double a=fabs(z);
	double t = 1.0/(1.0+a*p);
	double b = c2*exp((-z)*(z/2.0));
	double n = ((((b5*t+b4)*t+b3)*t+b2)*t+b1)*t;
	n = 1.0-b*n;
	
	if ( z < 0.0 )
		n = 1.0 - n;
	
	return n;
}


double Call::PriceCall(double t, double S_t)
{
    double u = this->data->T_ - t;
    double K = this->K_;
    double r = this->data->r_;
    double x = pnl_vect_get(this->data->spot_,0);
    double sigma = pnl_vect_get(this->data->sigma_,0);

	double d1 = (log(S_t/K)+(r+sigma*sigma/2.0)*u)/(sigma*sqrt(u));
    double d2 = d1 - sigma*sqrt(u);

    return S_t * N(d1) - K*exp(-r*u) * N(d2);
}