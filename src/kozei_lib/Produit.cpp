#include "Produit.hpp"

using namespace Product;

Produit::Produit(Data* data, double I)
{
	I_ = I;
	if (data)
	{
		this->data = data;
		path_ = pnl_mat_create_from_zero(data->nbTimeSteps_ + 1, data->size_);
	}
}

Produit::~Produit()
{
	if (path_)
	{
		pnl_mat_free(&path_);
	}
}

void Produit::price(double &prix, double &ic)
{
    double sum = 0;
    double tmp = 0;
    double sum_square = 0;
    double variance = 0;
    double payoff_res = 0;

    PnlMat *path = pnl_mat_create(data->nbTimeSteps_ + 1, data->size_);

    for (int i = 0; i < data->nbSamples_; i++)
	{
        pnl_mat_set_all(path, 0);
        data->asset(path);
        payoff_res = payoff(path);
        sum += payoff_res;
        sum_square += pow(payoff_res, 2);
    }

    pnl_mat_free(&path);

    tmp = sum;

    variance = getVariance(tmp, sum_square, 0);
    prix = getPrice(sum, 0);
    ic = getIntervalleConfiance(variance);
}

void Produit::price_change(double &prix, double &ic)
{
	double sum = 0;
	double tmp = 0;
	double sum_square = 0;
	double variance = 0;
	double payoff_res = 0;

	PnlMat *path = pnl_mat_create(data->nbTimeSteps_ + 1, data->size_);

	for (int i = 0; i < data->nbSamples_; i++)
	{
		pnl_mat_set_all(path, 0);
		data->assetTauxChange(path);
		
		payoff_res = payoff(path);

		sum += payoff_res;
		sum_square += pow(payoff_res, 2);
	}

	pnl_mat_free(&path);

	tmp = sum;

	variance = getVariance(tmp, sum_square, 0);
	prix = getPrice(sum, 0);
	ic = getIntervalleConfiance(variance);
}

void Produit::price(const PnlMat *past, double t, double &prix, double &ic)
{
    double sum = 0;
    double tmp = sum;
    double sum_square = 0;
    double variance = 0;
    double payoff_res = 0;

    PnlMat *path = pnl_mat_create(data->nbTimeSteps_ + 1, data->size_);
	
    for (int i = 0; i < data->nbSamples_; i++)
	{
        data->asset(path, t, past);
		PnlVect * vect = pnl_vect_new();
        payoff_res = payoff(path);

        sum += payoff_res;
        sum_square += pow(payoff_res, 2);
    }

    pnl_mat_free(&path);

    tmp = sum;

    variance = getVariance(tmp, sum_square, t);
    prix = getPrice(sum, t);

    ic = getIntervalleConfiance(variance);
}

void Produit::price_change(const PnlMat *past, double t, double &prix, double &ic)
{
	double sum = 0;
	double tmp = sum;
	double sum_square = 0;
	double variance = 0;
	double payoff_res = 0;

	PnlMat *path = pnl_mat_create(data->nbTimeSteps_ + 1, data->size_);

	for (int i = 0; i < data->nbSamples_; i++)
	{
		data->assetTauxChange(path, t, past);

		payoff_res = payoff(path);
		sum += payoff_res;
		sum_square += pow(payoff_res, 2);
	}

	pnl_mat_free(&path);

	tmp = sum;
	variance = getVariance(tmp, sum_square, t);
	prix = getPrice(sum, t);
	ic = getIntervalleConfiance(variance);
}


double Produit::getVariance(double sum, double sum_square, double t)
{
    sum /= data->nbSamples_;
    sum = pow(sum, 2);

    sum_square /= data->nbSamples_;

    if (t == data->T_ )
	{
        return 0;
    }
	
    return exp(-2 * data->r_ * (data->T_ - t))*(sum_square - sum);
}

double Produit::getIntervalleConfiance(double variance)
{
    return 2 * 1.96 * sqrt(variance / data->nbSamples_);
}

double Produit::getPrice(double sum, double t)
{
    sum *= exp(-data->r_ * (data->T_ - t)) / data->nbSamples_;
    return sum;
}