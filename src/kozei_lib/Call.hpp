/* 
 * File:   Call.hpp
 * Author: charlene
 *
 * Created on 23 décembre 2016, 16:20
 */

#ifndef CALL_HPP
#define CALL_HPP

#include "pnl/pnl_random.h"

#include "Produit.hpp"

#include <list>

#define DLLEXP   __declspec( dllexport )

using namespace Product;
class Call : public Produit
{
public:
	double K_;  /*! Strike of the option */

    /*!
    * \brief Constructor 
    * \param data[in] : market data used in the evaluation process
    * \param I[in] : initial investment
    * \param K[in] : option's strike
    */
	DLLEXP Call(Data* data, double I, double K);

    /*!
     * \brief Destructor
     */
    ~Call();

    /*!
     * \brief Payoff
     * \param[in] path : matrix of underlying spots to compute the payoff
     */
	DLLEXP double payoff(PnlMat *path);

    /*Cumulative Normal Distribution Function*/
    /*!
     * \brief
     * \param[in] z : the real where we want to compte ie P(X<z) where X is a normalized normal random variable
     * \return the value of the cumulative normal distribution function at point z
     */
	DLLEXP double N(double z);

    /*!
     * \brief
     * \param[in] t : time calculation of the the price
     * \param[in] S_t : value of the spot at the time t
     * \return the theoretical price of the call
     */
	DLLEXP double PriceCall(double t, double S_t);

    /*!
     * \brief
     * \param[in] t : time calculation of the the price
     * \param[in] S_t : value of the spot at the time t
     * \return
     */
	DLLEXP double DeltaCall(double t, double S_t);
};

#endif	/* CALL_HPP */

