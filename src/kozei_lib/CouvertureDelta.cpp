#include "CouvertureDelta.hpp"
#include "Simulation.hpp"

#include <iostream>

CouvertureDelta::CouvertureDelta(double fdStep, Produit *produit)
{
    fdStep_ = fdStep;
    produit_ = produit;

    shiftPlus_ = pnl_mat_new();
    shiftMoins_ = pnl_mat_new();

	constat_.clear();
	constat_.push_back(185);
	constat_.push_back(367);
	constat_.push_back(549);
	constat_.push_back(731);
	constat_.push_back(915);
	constat_.push_back(1096);
	constat_.push_back(1280);
	constat_.push_back(1461);
	constat_.push_back(1645);
	constat_.push_back(1826);
	constat_.push_back(2012);
	constat_.push_back(2194);
	constat_.push_back(2376);
	constat_.push_back(2558);
	constat_.push_back(2741);
	constat_.push_back(2922);

	init_.clear();
	init_.push_back(0);
	init_.push_back(4);
	init_.push_back(5);
}

CouvertureDelta::~CouvertureDelta()
{
	pnl_mat_free(&shiftPlus_);
	pnl_mat_free(&shiftMoins_);
}

void CouvertureDelta::shiftAsset(PnlMat* shifted_path, PnlMat* path, int actifToShift,
        double coeff, double time, double maturity)
{
    int start = ceil(time / maturity);
    double value;

    pnl_mat_clone(shifted_path, path);

    for (int k = start; k < path->m; k++)
	{
        value = MGET(path, k, actifToShift);
        MLET(shifted_path, k, actifToShift) = value * (1 + coeff);
    }
}

void CouvertureDelta::calculDelta(PnlVect* delta, const PnlMat *past, double t)
{
    Data* data = produit_->data;
    
    double payoff_res = 0;

    PnlVect *sum_square = pnl_vect_create_from_zero(delta->size);
    PnlVect *ic = pnl_vect_create_from_zero(delta->size);

    pnl_vect_set_all(delta, 0);

    // Moyenne des payoffs
    for (int j = 0; j < data->nbSamples_; j++)
	{
        // Simulation du path
        if (t == 0)
		{
            data->asset(produit_->path_);
        }
		else
		{
            data->asset(produit_->path_, t, past);
        }

        // Shift_path
        for (int i = 0; i < data->size_; i++)
		{   
            // Création des trajectoires shiftées
            this->shiftAsset(shiftPlus_, produit_->path_, i, fdStep_, t,
					data->T_ / data->nbTimeSteps_);
            this->shiftAsset(shiftMoins_, produit_->path_, i, -fdStep_, t,
					data->T_ / data->nbTimeSteps_);

            payoff_res = produit_->payoff(shiftPlus_) - produit_->payoff(shiftMoins_);
            LET(delta, i) += payoff_res;

            //pour l'intervalle de confiance
            LET(sum_square, i) += pow(payoff_res, 2);
        }
    }
    //Pour l'intervalle de confiance en 0
    if (t == 0)
	{
        for (int i = 0; i < data->size_; i++)
		{
            LET(ic, i) = sqrt((GET(sum_square, i) / data->nbSamples_) -
					(pow(GET(delta, i), 2) / pow(data->nbSamples_, 2)));
        }
    }

    double coeff = exp(-data->r_ * (data->T_ - t)) / (2 * data->nbSamples_ * fdStep_);

    pnl_vect_mult_scalar(ic, 2 * coeff * data->nbSamples_ * 1.96 / sqrt(data->nbSamples_));
    pnl_vect_mult_scalar(delta, coeff);
    PnlVect *copy = pnl_vect_create_from_zero(past->n);
    pnl_mat_get_row(copy, past, past->m - 1);

    pnl_vect_div_vect_term(delta, copy);

    pnl_vect_div_vect_term(ic, copy);

    pnl_vect_free(&copy);
    pnl_vect_free(&sum_square);
    pnl_vect_free(&ic);
}

void CouvertureDelta::init(double & dt, PnlMat* path, const PnlMat* past)
{
	double step = produit_->data->T_ / produit_->data->nbTimeSteps_;

	pnl_mat_resize(path, 1, produit_->data->size_);

	PnlVect* last_row = pnl_vect_create(past->n);
	PnlVect* tmpCours = pnl_vect_create(past->n);
	pnl_mat_get_row(last_row, past, (past->m) - 1);

	//remplissage de la ligne 0
	for (int j = 0; j < past->n; j++)
	{
		if (past->m <= init_.back())
		{
			MLET(path, 0, j) = MGET(past, 0, j);
		}
		else
		{
			for (int i = 0; i < init_.size(); i++)
			{
				MLET(path, 0, j) += MGET(past, init_[i], j);
			}
			MLET(path, 0, j) /= 3;
		}
	}

	//remplissage des dates de constatations passées
	for (int i = 0; i < constat_.size(); i++)
	{
		if (past->m > constat_[i])
		{
			for (int j = 0; j < past->n; j++)
			{
				LET(tmpCours, j) = MGET(past, constat_[i], j);
			}

			pnl_mat_add_row(path, path->m, tmpCours);

			if (past->m == (constat_[i] + 1))
			{
				dt = 0;

				pnl_vect_free(&last_row);
				pnl_vect_free(&tmpCours);
				return;
			}
		}
		else
		{
			dt = ((double)(constat_[i] - (past->m - 1))) / 365.0;
			pnl_mat_add_row(path, path->m, last_row);

			pnl_vect_free(&last_row);
			pnl_vect_free(&tmpCours);
			return;
		}
	}
}

void CouvertureDelta::calculDelta_change(PnlVect* delta, const PnlMat *past, double t, int size)
{
	Data* data = produit_->data;

	double payoff_res = 0;

	PnlVect *sum_square = pnl_vect_create_from_zero(delta->size);
	PnlVect *ic = pnl_vect_create_from_zero(delta->size);
	PnlMat * myPast = pnl_mat_new();

	pnl_vect_set_all(delta, 0);

	// Moyenne des payoffs
	for (int j = 0; j < data->nbSamples_; j++)
	{
		// Simulation du path
		if (t == 0)
		{
			data->assetTauxChange(produit_->path_);
		}
		else
		{
			double dt;
			init(dt, myPast, past);
			data->assetTauxChangeDt(produit_->path_, dt, myPast);
		}

		// Shift_path
		for (int i = 0; i < size; i++)
		{
			// Création des trajectoires shiftées
			this->shiftAsset(shiftPlus_, produit_->path_, i, fdStep_, t,
				data->T_ / data->nbTimeSteps_);
			this->shiftAsset(shiftMoins_, produit_->path_, i, -fdStep_, t,
				data->T_ / data->nbTimeSteps_);

			payoff_res = produit_->payoff(shiftPlus_) - produit_->payoff(shiftMoins_);
			LET(delta, i) += payoff_res;

			//pour l'intervalle de confiance
			LET(sum_square, i) += pow(payoff_res, 2);
		}
	}
	
	//Pour l'intervalle de confiance en 0
	if (t == 0)
	{
		for (int i = 0; i < size; i++)
		{
			LET(ic, i) = sqrt((GET(sum_square, i) / data->nbSamples_) -
				(pow(GET(delta, i), 2) / pow(data->nbSamples_, 2)));
		}
	}

	double coeff = exp(-data->r_ * (data->T_ - t)) / (2 * data->nbSamples_ * fdStep_);

	pnl_vect_mult_scalar(ic, 2 * coeff * data->nbSamples_ * 1.96 / sqrt(data->nbSamples_));
	pnl_vect_mult_scalar(delta, coeff);
	PnlVect *copy = pnl_vect_create_from_zero(past->n);
	pnl_mat_get_row(copy, past, past->m - 1);

	pnl_vect_div_vect_term(delta, copy);

	pnl_vect_div_vect_term(ic, copy);

	pnl_vect_free(&copy);
	pnl_vect_free(&sum_square);
	pnl_vect_free(&ic);
}



void CouvertureDelta::calculDeltaDeterministe(PnlVect* delta, const PnlMat *past,
		double t, PnlVect* vect)
{
	Data* data = produit_->data;

    double payoff = 0;

    pnl_vect_set_all(delta, 0);

    // Moyenne des payoffs
    for (int j = 0; j < data->nbSamples_; j++)
	{
        // Simulation du path
        if (t == 0)
		{
            data->assetDeterministe(produit_->path_,vect);

        }
		else
		{
            data->assetDeterministe(produit_->path_, t, past,vect);
        }

        // Shift_path
        for (int i = 0; i < data->size_; i++)
		{
            // Création des trajectoires shiftées
            this->shiftAsset(shiftPlus_, produit_->path_, i, fdStep_, t,
					data->T_ / data->nbTimeSteps_);
          
            this->shiftAsset(shiftMoins_, produit_->path_, i, -fdStep_, t,
					data->T_ / data->nbTimeSteps_);

            payoff = produit_->payoff(shiftPlus_) - produit_->payoff(shiftMoins_);
             
            LET(delta, i) += payoff;     
        }
    }

    double coeff = exp(-data->r_ * (data->T_ - t)) / (2 * data->nbSamples_ * fdStep_);
    
    pnl_vect_mult_scalar(delta, coeff);
    PnlVect *copy = pnl_vect_create_from_zero(past->n);
    pnl_mat_get_row(copy, past, past->m - 1);

    pnl_vect_div_vect_term(delta, copy);

    pnl_vect_free(&copy);
}

bool CouvertureDelta::oracle()
{
	return true;
}
