#include "Asiatique.hpp"

#include "pnl/pnl_random.h"

Asiatique::Asiatique(Data* data, double I, double strike, double lbd) : Produit(data, I)
{
	K_ = strike;
	lambda = pnl_vect_create_from_scalar(data->size_, lbd);
	sommeStd = pnl_vect_create(data->size_);
}

double Asiatique::payoff(PnlMat* path)
{
	double payoff = 0;
	pnl_mat_sum_vect(sommeStd, path, 'r');
	pnl_vect_div_scalar(sommeStd, (double) path->m);
	payoff = pnl_vect_scalar_prod(lambda, sommeStd);
	payoff -= K_;
	
	if (payoff < 0)
	{
		return 0;
	} 
	else
	{
		return payoff;
	}
}
