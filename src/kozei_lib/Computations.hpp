#pragma once

#define DLLEXP   __declspec( dllexport )

#include "Portefeuille.hpp"
#include "Simulation.hpp"
#include "KozeiChange.hpp"
#include "RetrieveData.hpp"


namespace Computations
{
	DLLEXP void retrieveCoursActifs(int numJourDernierRebal, double* coursActifs, double* coursActifsDernierRebal, int nbActifPlusDevise,
		RetrieveData* retrieve_data, vector<vector<DataStructure>*>* vect_cours,
		vector<vector<DataStructure>*>* vect_change);
	DLLEXP void pricer(int numJourDernierRebal, double lastPartSansRisque, double lastPrice, double* delta,
		PnlVect* volatility, PnlMat* corr, PnlMat* past, int nbDevises, PnlVect* taux_sans_risque_etranger, 
		double& price, double& ic, double& val_pf, double& val_pf_new);
	DLLEXP void pricerProduit(int numJourDernierRebal, double lastPartSansRisque, double lastPrice,
		double* delta, double& prix, double& ic, double& val_pf, double& val_pf_new, double* coursActifs,
		int nbActifPlusDevise,
		RetrieveData* retrieve_data, vector<vector<DataStructure>*>* vect_cours,
		vector<vector<DataStructure>*>* vect_change, double* volatilites, double** mat_corr);

	DLLEXP void calculDelta(int numJourDernierRebal, double lastPartSansRisque, double lastPrice, double* delta,
		int& newNumJourDernierRebal, double& newPartSansRisqueAvecRebal, int nbActifPlusDevise,
		RetrieveData* retrieve_data, vector<vector<DataStructure>*>* vect_cours,
		vector<vector<DataStructure>*>* vect_change, double* volatilites, double** mat_corr, double* newDelta);
	DLLEXP void calculDeltaInterne(int numJourDernierRebal, double lastPartSansRisque, double lastPrice,
		double* delta, PnlVect* volatility, PnlMat* corr, double* newDelta, PnlMat* past, int nbDevises, 
		PnlVect* taux_sans_risque_etranger, int& newNumJourDernierRebal, double& newPartSansRisqueAvecRebal);
}

