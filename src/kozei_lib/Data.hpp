/* 
 * File:   Data.hpp
 * Author: charlene
 *
 * Created on 23 décembre 2016, 16:22
 */

#ifndef DATA_HPP
#define DATA_HPP

#include "pnl/pnl_random.h"
#include "pnl/pnl_matrix.h"
#include "pnl/pnl_vector.h"

#include <iostream>
#include <map>

#include "DataStructure.hpp"

#define DLLEXP   __declspec( dllexport )

using namespace std;

namespace CurrentState
{
	enum class CurrentState
	{
		USD = 0,
		JPY = 1,
		GBP = 2,
		CAD = 3,
		EUR = 4
	};
}


/*! \class Data
 * \brief Data container
 * 
 * Class that contains all the market data used to evaluate our product and also attributes used to store matrix or vector during our calculation
 */
class Data
{
public:
	int nbSamples_; /*!< Number of loops in the Monte Carlo method */
	int nbTimeSteps_; /*!< Number of steps between t=0 and t=T (maturity) */
	int size_; /*!< Number of underlying assets (equals to 30 in our case) */
		
	double rho_; /*!< Correlation parameter */
	double T_; /*!< Maturity date */
		
	PnlRng *rng_; /*!< Random generator */
	PnlVect *sigma_; /*!< Pointer to a vector containing the volatility of underlying assets */
	PnlVect *spot_; /*!< Pointer to a vector containing the initial prices of the underlying assets */
	PnlVect *G_; /*!< Pointer to a Gaussian vector (used in asset methods) */

	PnlMat *mat_cholesky_; /*!< Pointer to the Cholesky matrix associated to the correlation matrix */
	PnlMat *clone_past_; /*!< Temporary variable used in asset methods */
	PnlMat *subBlock_; /*!< Temporary variable used in asset methods */

	/* attributs pour le taux de change */
	double r_; /* Interest rate taux domestique */
	PnlVect* sigma_taux_; //volatilite du taux de change, même ordre que pour le type enum Currency
	PnlVect * taux_sans_risque_etranger_; //vecteur des taux sans risques etrangers
	int nbDevises_; //nombre de devises étrangeres
	PnlVect * spot_change_;

	std::map<int, CurrentState::CurrentState> index_currencies;


    /*!
     * \brief Constructor 
     */
	DLLEXP Data(){};
                
    /*!
     * \brief Constructor 
     * 
     * \param[in] nbSamples number of MonteCarlo draws
     * \param[in] nbTimeSteps number of dates between the origin and the maturity
     * \param[in] size size of the model
     * \param[in] maturity maturity of the product
     * \param[in] r zero risk interest rate
     * \param[in] rho correlation factor
     * \param[in] spot spot prices of underlying assets
     * \param[in] sigma composition vector of the portfolio
     */
	DLLEXP Data(int nbSamples,
			int nbTimeSteps,
			int size,
			double maturity,
			double r,
			double rho,
			const PnlVect *spot,
			const PnlVect *sigma,
			const PnlVect *sigma_taux);

	DLLEXP Data(int nbSamples,
		int nbTimeSteps,
		int size,
		double maturity,
		double r,
		const PnlMat *matrixCorr,
		const PnlVect *spot,
		const PnlVect *sigma,
		const PnlVect * sigma_taux);

	//constructeur pour tenir compte du taux de change
	DLLEXP Data(int nbSamples,
		int nbTimeSteps,
		int size,
		double maturity,
		double r,
		const PnlMat *matrixCorr,
		const PnlVect *spot,
		const PnlVect *sigma,
		const PnlVect *sigma_taux, 
		const PnlVect * taux_sans_risque_etranger,
		int nbDevises,
		const PnlVect *spot_change );

	//constructeur pour tenir compte du taux de change
	DLLEXP Data(int nbSamples,
		int nbTimeSteps,
		int size,
		double maturity,
		double r,
		double rho,
		const PnlVect *spot,
		const PnlVect *sigma,
		const PnlVect *sigma_taux,
		const PnlVect * taux_sans_risque_etranger,
		int nbDevises,
		const PnlVect *spot_change);
               
    /*!
     * \brief Destructor
     */
	~Data();

	DLLEXP void getDevise(int & devise, int index);
	
	/**
	 * \brief Method that generates a trajectory of the model and stores it in path
	 *
	 * @param[out] path Contains a trajectory of the model.
	 * It is a matrix of size (N + 1) x d
	 */
	DLLEXP void asset(PnlMat *path);

	DLLEXP void assetTauxChange(PnlMat *path);

	/**
	 * \brief Method that generates the model knowing the past until date t
	 *
	 * @param[out] path : pointer to a matrix containing a trajectory of the underlying assets
	 * Given up to the instant T by the past matrix
	 * @param[in] t : date until which we know the trajectory, t is
	 * not necessarily a date of discretization
	 * @param[in] past : trajectory performed up to date t
	 */
	DLLEXP void asset(PnlMat *path, double t, const PnlMat *past);

	DLLEXP void assetTauxChange(PnlMat *path, double t, const PnlMat *past);

	DLLEXP void assetTauxChangeDt(PnlMat *path, double dt, const PnlMat *past);

    /**
     * \brief
     * @param[out] path : pointer to a matrix containing a trajectory of the underlying assets
     * @param[in] vect :
     */
	DLLEXP void assetDeterministe(PnlMat *path, PnlVect *vect);
                
    /**
     * \brief
     * @param[out] path : pointer to a matrix containing a trajectory of the underlying assets
     * @param[in] t : date until which we know the trajectory, t is
	 * not necessarily a date of discretization
     * @param[in] past : trajectory performed up to date t
     * @param[in] G : 
     */
    DLLEXP void assetDeterministe(PnlMat *path, double t, const PnlMat *past, PnlVect *G);

	DLLEXP void assetDeterministeTaux(PnlMat *path, PnlVect *vect);
};

#endif	/* DATA_HPP */