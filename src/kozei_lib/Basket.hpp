/*
 * File:   Basket.hpp
 * Author: mignotju
 *
 * Created on 12 janvier 2017, 16:20
 */

#ifndef BASKET_HPP
#define	BASKET_HPP

#include "pnl/pnl_random.h"

#include "Produit.hpp"

#include <list>

#define DLLEXP   __declspec( dllexport )

using namespace std;

using namespace Product;
class Basket : public Produit
{
	public:
		double K_; /*!< Strike of the option */
		PnlVect* lambda; /*!< Composition of the basket */

                /*!
                 * \brief Constructor 
                 * \param[in] data : market data used in the evaluation process
                 * \param[in] I : initial investment
                 * \param[in] K : option's strike
                 * \param[in] lambda : composition of the option's basket
                 */
		DLLEXP Basket(Data* data, double I, double K, double lambda);
                
                /*!
                 * \brief Destructor
                 */
		~Basket();

                /*!
                 * \brief Payoff
                 * \param[in] path : matrix of underlying spots to compute the payoff
                 */
		DLLEXP double payoff(PnlMat* path);
};

#endif	/* BASKET_HPP */
