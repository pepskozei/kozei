/*
* File:   KozeiChange.hpp
* Author: charlene
*
* Created on 23 d�cembre 2016, 16:20
*/

#ifndef KozeiChange_HPP
#define	KozeiChange_HPP

#include "pnl/pnl_random.h"

#include "Produit.hpp"

#include <vector>
#include <list>

#define DLLEXP   __declspec( dllexport )

using namespace Product;

class KozeiChange : public Produit
{
public:
	vector<int> constat_;
	vector<int> init_;
	PnlVect* cours0_;
	PnlVect* perfTmp_;

	/*!
	* \brief Constructor
	* \param[in] data : market data used in the evaluation process
	* \param[in] I : initial investment
	*/
	DLLEXP KozeiChange(Data* data, double I);

	/*!
	* \brief Destructor
	*/
	~KozeiChange();

	/*!
	* \brief Payoff
	* \param[in] path : matrix of underlying spots to compute the payoff
	*/
	DLLEXP double payoff(PnlMat* path);

	DLLEXP void init(double & dt, PnlMat* path, const PnlMat* past);

	DLLEXP void price_change(const PnlMat *past, double t, double &prix, double &ic) override;
};

#endif	/* KozeiChange_HPP */

