#include "Simulation.hpp"

Simulation::Simulation() {}

Simulation::Simulation(int nbSamples, int nbTimeSteps, int size,
		double maturity, double r, double rho, const PnlVect *spot,
		const PnlVect *sigma, const PnlVect * sigma_taux) : Data(nbSamples, nbTimeSteps, size,
			maturity, r, rho, spot, sigma, sigma_taux)
{
	//Simulation of the market
	market_ = pnl_mat_create(nbTimeSteps+1, size);
	asset(market_);
}

Simulation::~Simulation()
{
	pnl_mat_free(&market_);
}
