#include "Data.hpp"
#include "EstimationParam.hpp"

#include <ctime>
#include <iostream>

Data::Data(int nbSamples, 
		int nbTimeSteps,
		int size,
		double maturity,
		double r,
		double rho,
		const PnlVect *spot,
		const PnlVect *sigma,
		const PnlVect * sigma_taux)
{
    nbSamples_ = nbSamples;
    nbTimeSteps_ = nbTimeSteps;
    size_ = size;
    T_ = maturity;
    r_ = r;
    rho_ = rho;
	sigma_taux_ = pnl_vect_copy(sigma_taux);

    spot_ = pnl_vect_copy(spot);
    sigma_ = pnl_vect_copy(sigma);

	//Computation of the Cholesky matrix of the correlation matrix
    mat_cholesky_ = pnl_mat_create_from_scalar(size_, size_, rho_);
    pnl_mat_set_diag(mat_cholesky_, 1, 0);
    pnl_mat_chol(mat_cholesky_);

    // Allocation of vectors used in asset
    rng_ = pnl_rng_create(PNL_RNG_MERSENNE);
    pnl_rng_sseed(rng_, time(NULL));
    G_ = pnl_vect_create(size_);
    clone_past_ = pnl_mat_new();
    subBlock_ = pnl_mat_new();
}

Data::Data(int nbSamples,
	int nbTimeSteps,
	int size,
	double maturity,
	double r,
	const PnlMat *matrixCorr,
	const PnlVect *spot,
	const PnlVect *sigma,
	const PnlVect * sigma_taux)
{
	nbSamples_ = nbSamples;
	nbTimeSteps_ = nbTimeSteps;
	size_ = size;
	T_ = maturity;
	r_ = r;
	sigma_taux_ = pnl_vect_copy(sigma_taux);

	spot_ = pnl_vect_copy(spot);
	sigma_ = pnl_vect_copy(sigma);

	//Computation of the Cholesky matrix of the correlation matrix
	mat_cholesky_ = pnl_mat_copy(matrixCorr);
	pnl_mat_chol(mat_cholesky_);

	// Allocation of vectors used in asset
	rng_ = pnl_rng_create(PNL_RNG_MERSENNE);
	pnl_rng_sseed(rng_, time(NULL));
	G_ = pnl_vect_create(size_);
	clone_past_ = pnl_mat_new();
	subBlock_ = pnl_mat_new();
}

Data::Data(int nbSamples,
	int nbTimeSteps,
	int size,
	double maturity,
	double r,
	double rho,
	const PnlVect *spot,
	const PnlVect *sigma,
	const PnlVect *sigma_taux,
	const PnlVect * taux_sans_risque_etranger,
	int nbDevises,
	const PnlVect *spot_change) : Data(nbSamples, nbTimeSteps, size + nbDevises, maturity, r, rho, spot, sigma, sigma_taux)
{
	nbDevises_ = nbDevises;
	taux_sans_risque_etranger_ = pnl_vect_copy(taux_sans_risque_etranger);
	spot_change_ = pnl_vect_copy(spot_change);

	for (int i = 0; i < size_ - nbDevises_; i++)
	{
		if (i < 22)
		{
			index_currencies.insert(std::pair<int, CurrentState::CurrentState>(i, CurrentState::CurrentState::USD));
		}

		if (i == 22)
			index_currencies.insert(std::pair<int, CurrentState::CurrentState>(i, CurrentState::CurrentState::JPY));
		if (i == 23)
			index_currencies.insert(std::pair<int, CurrentState::CurrentState>(i, CurrentState::CurrentState::GBP));
		if (i == 24)
			index_currencies.insert(std::pair<int, CurrentState::CurrentState>(i, CurrentState::CurrentState::CAD));
		if (i> 24)
			index_currencies.insert(std::pair<int, CurrentState::CurrentState>(i, CurrentState::CurrentState::EUR));
	}
}

Data::Data(int nbSamples,
	int nbTimeSteps,
	int size,
	double maturity,
	double r,
	const PnlMat *matrixCorr,
	const PnlVect *spot,
	const PnlVect *sigma,
	const PnlVect *sigma_taux,
	const PnlVect * taux_sans_risque_etranger,
	int nbDevises,
	const PnlVect * spot_change) : Data(nbSamples, nbTimeSteps, size + nbDevises, maturity, r, matrixCorr, spot, sigma, sigma_taux)
{
	nbDevises_ = nbDevises;
	taux_sans_risque_etranger_ = pnl_vect_copy(taux_sans_risque_etranger);
	spot_change_ = pnl_vect_copy(spot_change);

	for (int i = 0; i < size_- nbDevises_; i++)
	{
		if (i < 22){
			index_currencies.insert(std::pair<int, CurrentState::CurrentState>(i, CurrentState::CurrentState::USD));
		}
		if (i==22)
			index_currencies.insert(std::pair<int, CurrentState::CurrentState>(i, CurrentState::CurrentState::JPY));
		if (i==23)
			index_currencies.insert(std::pair<int, CurrentState::CurrentState>(i, CurrentState::CurrentState::GBP));
		if (i==24)
			index_currencies.insert(std::pair<int, CurrentState::CurrentState>(i, CurrentState::CurrentState::CAD));
		if (i> 24)
			index_currencies.insert(std::pair<int, CurrentState::CurrentState>(i, CurrentState::CurrentState::EUR));
	}
}

Data::~Data()
{
	pnl_vect_free(&G_);
    pnl_vect_free(&sigma_);
    pnl_vect_free(&spot_);

	pnl_rng_free(&rng_);

	pnl_mat_free(&mat_cholesky_);
	pnl_mat_free(&clone_past_);
	pnl_mat_free(&subBlock_);
}

void Data::asset(PnlMat *path)
{    
    PnlVect *cours_date = pnl_vect_new();
    pnl_vect_clone(cours_date, spot_);

    PnlVect *mat_chol_row = pnl_vect_create(size_);

    for (int date = 0; date < path->m; date++)
	{
        if (date == 0)
		{
            pnl_mat_set_row(path, cours_date, date);
        }
		else
		{
            pnl_vect_rng_normal(G_, size_, rng_);

            for (int i = 0; i < size_; i++)
			{
                pnl_mat_get_row(mat_chol_row, mat_cholesky_, i);
				double sigma_i = GET(sigma_, i);
				double sigma_taux_i = GET(sigma_taux_, i);
                LET(cours_date, i) = GET(cours_date, i) *
					exp((r_ - (sigma_i + sigma_taux_i)*(sigma_i + sigma_taux_i) / 2) * (T_ / (path->m - 1)) +
                        (sigma_i + sigma_taux_i) * sqrt(T_ / (path->m - 1)) *
						pnl_vect_scalar_prod(mat_chol_row, G_));
            }

            pnl_mat_set_row(path, cours_date, date);
        }      
    }

    pnl_vect_free(&cours_date);
    pnl_vect_free(&mat_chol_row);
}

void Data::assetTauxChange(PnlMat *path)
{
	double step = T_ / nbTimeSteps_;
	PnlVect *cours_date = pnl_vect_new();
	pnl_vect_clone(cours_date, spot_);

	PnlVect *mat_chol_row = pnl_vect_create(size_);

	for (int date = 0; date < path->m; date++)
	{
		if (date == 0)
		{
			pnl_mat_set_row(path, cours_date, date);
		}
		else
		{
			pnl_vect_rng_normal(G_, size_, rng_);

			for (int i = 0; i < size_; i++)
			{
				pnl_mat_get_row(mat_chol_row, mat_cholesky_, i);

				if (i < 30) //Si l'actif i est une action
				{ 
					std::map<int, CurrentState::CurrentState>::iterator it;
					it = index_currencies.find(i);
					int devise = (int)it->second;
					double sigma_i = GET(sigma_, i);
					double sigma_taux_i = GET(sigma_taux_, devise);
					double scal_prod = pnl_vect_scalar_prod(mat_chol_row, G_);
					LET(cours_date, i) = GET(cours_date, i) *
						exp((r_ - (sigma_i + sigma_taux_i)*(sigma_i + sigma_taux_i) / 2) * step +
						(sigma_i + sigma_taux_i) * sqrt(step) *
						pnl_vect_scalar_prod(mat_chol_row, G_));
				}
				else //ajout des exp(rf t) Xt
				{ 
					double sigma_taux;
					if (i == 30)
						sigma_taux = GET(sigma_taux_, 0);
					else if (i == 31)
						sigma_taux = GET(sigma_taux_, 1);
					else if (i == 32)
						sigma_taux = GET(sigma_taux_, 2);
					else if (i == 33)
						sigma_taux = GET(sigma_taux_, 3);
					LET(cours_date, i) = GET(cours_date, i) *
						exp((r_ - (sigma_taux*sigma_taux) / 2) * (step)+
						sigma_taux * sqrt(step) *
						pnl_vect_scalar_prod(mat_chol_row, G_));
				}
			}

			pnl_mat_set_row(path, cours_date, date);
		}
	}

	pnl_vect_free(&cours_date);
	pnl_vect_free(&mat_chol_row);
}

void Data::asset(PnlMat *path, double t, const PnlMat *past)
{
    double step = T_ / nbTimeSteps_;

    pnl_mat_clone(clone_past_, past);

    if (fmod(t, T_ / nbTimeSteps_) != 0)
	{
        pnl_mat_del_row(clone_past_, clone_past_->m - 1);
    }

    pnl_mat_set_subblock(path, clone_past_, 0, 0);

    PnlVect *last_row = pnl_vect_create(past->n);
    pnl_mat_get_row(last_row, past, (past->m) - 1);

    PnlVect *cours_date = pnl_vect_new();
    pnl_vect_clone(cours_date, last_row);

    PnlVect *mat_chol_row = pnl_vect_new();

	double sigma_i;
	double sigma_taux_i;

    for (int date = clone_past_->m; date < nbTimeSteps_ + 1; date++)
	{
        pnl_vect_rng_normal(G_, size_, rng_);

        for (int i = 0; i < size_; i++)
		{
            pnl_mat_get_row(mat_chol_row, mat_cholesky_, i);

			sigma_i = GET(sigma_, i);
			sigma_taux_i = GET(sigma_taux_, i);
			LET(cours_date, i) = GET(cours_date, i) *
				exp((r_ - (sigma_i + sigma_taux_i)*(sigma_i + sigma_taux_i) / 2) * step +
				(sigma_i + sigma_taux_i) * sqrt(step) *
				pnl_vect_scalar_prod(mat_chol_row, G_));
        }
        
        pnl_mat_set_row(path, cours_date, date);
    }
	
    pnl_vect_free(&last_row);
    pnl_vect_free(&cours_date);
    pnl_vect_free(&mat_chol_row);
}


void Data::assetTauxChange(PnlMat *path, double t, const PnlMat *past)
{
	double step = T_ / nbTimeSteps_;

	pnl_mat_clone(clone_past_, past);

	if (fmod(t, T_ / nbTimeSteps_) != 0)
	{
		pnl_mat_del_row(clone_past_, clone_past_->m - 1);
	}

	pnl_mat_set_subblock(path, clone_past_, 0, 0);

	PnlVect *last_row = pnl_vect_create(past->n);
	pnl_mat_get_row(last_row, past, (past->m) - 1);

	PnlVect *cours_date = pnl_vect_new();
	pnl_vect_clone(cours_date, last_row);

	PnlVect *mat_chol_row = pnl_vect_new();

	double sigma_i;
	double sigma_taux_i;

	for (int date = clone_past_->m; date < nbTimeSteps_ + 1; date++)
	{
		pnl_vect_rng_normal(G_, size_, rng_);

		for (int i = 0; i < size_; i++)
		{
			pnl_mat_get_row(mat_chol_row, mat_cholesky_, i);

			if (i < 30) //Si l'actif i est une action
			{ 
				int devise;
				getDevise(devise, i);

				double sigma_i = GET(sigma_, i);
				double sigma_taux_i = GET(sigma_taux_, devise);

				LET(cours_date, i) = GET(cours_date, i) *
					exp((r_ - (sigma_i + sigma_taux_i)*(sigma_i + sigma_taux_i) / 2) * step +
					(sigma_i + sigma_taux_i) * sqrt(step) *
					pnl_vect_scalar_prod(mat_chol_row, G_));
			}
			else //ajout des exp(rf t) Xt
			{ 
				double sigma_taux;
				if (i == 30)
					sigma_taux = GET(sigma_taux_, 0);
				else if (i == 31)
					sigma_taux = GET(sigma_taux_, 1);
				else if (i == 32)
					sigma_taux = GET(sigma_taux_, 2);
				else if (i == 33)
					sigma_taux = GET(sigma_taux_, 3);
				LET(cours_date, i) = GET(cours_date, i) *
					exp((r_ - (sigma_taux*sigma_taux) / 2) * (step)+
					sigma_taux * sqrt(step) *
					pnl_vect_scalar_prod(mat_chol_row, G_));
			}

		}	
		pnl_mat_set_row(path, cours_date, date);
	}
	
	pnl_vect_free(&last_row);
	pnl_vect_free(&cours_date);
	pnl_vect_free(&mat_chol_row);
}

void Data::assetTauxChangeDt(PnlMat *path, double dt, const PnlMat *past) 
{
	double stepProduit = T_ / nbTimeSteps_;
	double step;

	pnl_mat_clone(clone_past_, past);

	if (dt != 0)
	{
		pnl_mat_del_row(clone_past_, clone_past_->m - 1);
	}

	pnl_mat_set_subblock(path, clone_past_, 0, 0);

	PnlVect *last_row = pnl_vect_create(past->n);
	pnl_mat_get_row(last_row, past, (past->m) - 1);

	PnlVect *cours_date = pnl_vect_new();
	pnl_vect_clone(cours_date, last_row);

	PnlVect *mat_chol_row = pnl_vect_new();

	double sigma_i;
	double sigma_taux_i;

	for (int date = clone_past_->m; date < nbTimeSteps_ + 1; date++)
	{
		pnl_vect_rng_normal(G_, size_, rng_);

		if ((date == clone_past_->m) && (dt != 0))
		{
			step = dt;
		}
		else
		{
			step = stepProduit;
		}

		for (int i = 0; i < size_; i++)
		{
			pnl_mat_get_row(mat_chol_row, mat_cholesky_, i);

			if (i < 30) //Si l'actif i est une action
			{ 
				int devise;
				getDevise(devise, i);

				double sigma_i = GET(sigma_, i);
				double sigma_taux_i = GET(sigma_taux_, devise);
				//double scal_prod = pnl_vect_scalar_prod(mat_chol_row, G_);
				LET(cours_date, i) = GET(cours_date, i) *
					exp((r_ - (sigma_i + sigma_taux_i)*(sigma_i + sigma_taux_i) / 2) * step +
					(sigma_i + sigma_taux_i) * sqrt(step) *
					pnl_vect_scalar_prod(mat_chol_row, G_));
			}
			else //ajout des exp(rf t) Xt
			{ 
				double sigma_taux;
				if (i == 30)
					sigma_taux = GET(sigma_taux_, 0);
				else if (i == 31)
					sigma_taux = GET(sigma_taux_, 1);
				else if (i == 32)
					sigma_taux = GET(sigma_taux_, 2);
				else if (i == 33)
					sigma_taux = GET(sigma_taux_, 3);
				LET(cours_date, i) = GET(cours_date, i) *
					exp((r_ - (sigma_taux*sigma_taux) / 2) * (step)+
					sigma_taux * sqrt(step) *
					pnl_vect_scalar_prod(mat_chol_row, G_));
			}
		}

		pnl_mat_set_row(path, cours_date, date);
	}

	pnl_vect_free(&last_row);
	pnl_vect_free(&cours_date);
	pnl_vect_free(&mat_chol_row);
}



void Data::assetDeterministe(PnlMat *path, PnlVect *vect)
{
    PnlVect *cours_date = pnl_vect_new();
    pnl_vect_clone(cours_date, spot_);
    PnlVect *mat_chol_row = pnl_vect_new();

    pnl_vect_clone(G_, vect);
    for (int date = 0; date < nbTimeSteps_ + 1; date++)
	{
        if (date == 0)
		{	
            pnl_mat_set_row(path, cours_date, date);
        }
		else
		{
            for (int i = 0; i < size_; i++)
			{
				pnl_mat_get_row(mat_chol_row, mat_cholesky_, i);
				double sigma_i = GET(sigma_, i);
				double sigma_taux_i = GET(sigma_taux_, i);
				std::cout << "sigma_i : " << sigma_taux_i<<std::endl;
				LET(cours_date, i) = GET(cours_date, i) *
					exp(( r_ - (sigma_i + sigma_taux_i)*(sigma_i + sigma_taux_i) / 2) * (T_ / (path->m - 1)) +
					(sigma_i + sigma_taux_i) * sqrt(T_ / (path->m - 1)) *
					pnl_vect_scalar_prod(mat_chol_row, G_));
            }

            pnl_mat_set_row(path, cours_date, date);
        }
    }

    pnl_vect_free(&cours_date);
    pnl_vect_free(&mat_chol_row);  
}

void Data::assetDeterministe(PnlMat *path, double t, const PnlMat *past, PnlVect *G)
{
    double step = T_ / nbTimeSteps_;

    pnl_mat_clone(clone_past_, past);

    if (fmod(t, T_ / nbTimeSteps_) != 0)
	{
        pnl_mat_del_row(clone_past_, clone_past_->m - 1);
    }

    pnl_mat_set_subblock(path, clone_past_, 0, 0);

    PnlVect *last_row = pnl_vect_create(past->n);
    pnl_mat_get_row(last_row, past, (past->m) - 1);

    PnlVect *cours_date = pnl_vect_new();
    pnl_vect_clone(cours_date, last_row);

    PnlVect *mat_chol_row = pnl_vect_new();

	double sigma_i;
	double sigma_taux_i;

    for (int date = clone_past_->m; date < nbTimeSteps_ + 1; date++)
	{
        if (date == clone_past_->m && fmod(t, step) != 0)
		{
            for (int i = 0; i < size_; i++)
			{
                pnl_mat_get_row(mat_chol_row, mat_cholesky_, i);

				sigma_i = GET(sigma_, i);
				sigma_taux_i = GET(sigma_taux_, i);

				LET(cours_date, i) = GET(cours_date, i) *
					exp((r_ - (sigma_i + sigma_taux_i)*(sigma_i + sigma_taux_i) / 2) * ((past->m - 1) * step - t) +
					(sigma_i + sigma_taux_i) * sqrt(((past->m - 1) * step - t)) *
					pnl_vect_scalar_prod(mat_chol_row, G));  
            }
        }
		else
		{
            for (int i = 0; i < size_; i++)
			{
                pnl_mat_get_row(mat_chol_row, mat_cholesky_, i);
				sigma_i = GET(sigma_, i);
				sigma_taux_i = GET(sigma_taux_, i);

				LET(cours_date, i) = GET(cours_date, i) *
					exp((r_ - (sigma_i + sigma_taux_i)*(sigma_i + sigma_taux_i) / 2) * step +
					(sigma_i + sigma_taux_i) * sqrt(step) *
					pnl_vect_scalar_prod(mat_chol_row, G));
            }
        }
		
        pnl_mat_set_row(path, cours_date, date);
    }
	
    pnl_vect_free(&last_row);
    pnl_vect_free(&cours_date);
    pnl_vect_free(&mat_chol_row);
}


void Data::assetDeterministeTaux(PnlMat *path, PnlVect *vect)
{
	pnl_mat_resize(path, nbTimeSteps_ + 1, size_);
	PnlVect *cours_date = pnl_vect_create(size_);
	
	for (int i = 0; i < size_; i++)
	{
		if (i < 30)
			LET(cours_date, i) = GET(spot_, i);
		else
			LET(cours_date, i) = GET(spot_change_, i-30);
	}

	PnlVect *mat_chol_row = pnl_vect_new();

	pnl_vect_clone(G_, vect);
	for (int date = 0; date < nbTimeSteps_ + 1; date++)
	{
		if (date == 0)
		{
			pnl_mat_set_row(path, cours_date, date);
		}
		else
		{
			for (int i = 0; i < size_; i++)
			{
				pnl_mat_get_row(mat_chol_row, mat_cholesky_, i);

				if (i < 30) //Si l'actif i est une action
				{ 
					std::map<int, CurrentState::CurrentState>::iterator it;
					it = index_currencies.find(i);
					int devise = (int)it->second;
					double sigma_i = GET(sigma_, i);
					double sigma_taux_i = GET(sigma_taux_, devise);
					double scal_prod = pnl_vect_scalar_prod(mat_chol_row, G_);
					
					LET(cours_date, i) = GET(cours_date, i) *
						exp((r_ - (sigma_i + sigma_taux_i)*(sigma_i + sigma_taux_i) / 2) * (T_ / (path->m - 1)) +
						(sigma_i + sigma_taux_i) * sqrt(T_ / (path->m - 1)) *
						pnl_vect_scalar_prod(mat_chol_row, G_));
				}
				else //ajout des exp(rf t) Xt
				{
					double sigma_taux;
					if (i == 30)
						sigma_taux = GET(sigma_taux_, 0);
					else if (i == 31)
						sigma_taux = GET(sigma_taux_, 1);
					else if (i == 32)
						sigma_taux = GET(sigma_taux_, 2);
					else if (i == 33)
						sigma_taux = GET(sigma_taux_, 3);
					
					LET(cours_date, i) = GET(cours_date, i) *
						exp((r_ - (sigma_taux*sigma_taux)/ 2) * (T_ / (path->m - 1)) +
						 sigma_taux * sqrt(T_ / (path->m - 1)) *
						pnl_vect_scalar_prod(mat_chol_row, G_));
				}
			}

			pnl_mat_set_row(path, cours_date, date);
		}
	}

	pnl_vect_free(&cours_date);
	pnl_vect_free(&mat_chol_row);
}

void Data::getDevise(int & devise, int index)
{
	if (index < 22)
	{
		devise = 0;
	}
	else if (index == 22)
	{
		devise = 1;
	}
	else if (index == 23)
	{
		devise = 2;
	}
	else if (index == 24)
	{
		devise = 3;
	}
	else
	{
		devise = 4;
	}
}