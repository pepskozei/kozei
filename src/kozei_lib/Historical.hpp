/*
* File:   Simulation.hpp
* Author: charlene
*
* Created on 23 d�cembre 2016, 16:23
*/

#ifndef HISTORICAL_HPP
#define HISTORICAL_HPP

#include "Data.hpp"
#include <vector>

#define DLLEXP   __declspec( dllexport )

using namespace std;

class Historical : public Data
{
public:
	/**
	* \brief Default constructor
	*/
	DLLEXP Historical();

	/**
	* \brief Constructor with parameters
	*
	* \param[in] nbSamples number of MonteCarlo draws
	* @param[in] nbTimeSteps  number of steps between t=0 and t=T (maturity)
	* @param[in] size  number of underlying assets (equel to 30 in our case)
	* @param[in] maturity  maturity date
	* @param[in] r interest rate
	* @param[in] rho  correlation parameter
	* @param[in] spot pointer to a vector containing the initial prices of the underlying assets
	* @param[in] sigma pointer to a vector containing the volatility of underlying assets
	*/
	DLLEXP Historical(int nbSamples,
		int nbTimeSteps,
		int size,
		double maturity,
		double r,
		double rho,
		const PnlVect *spot,
		const PnlVect *sigma,
		const PnlVect * sigma_taux);

	DLLEXP vector<PnlVect *> getHistoricalData();

	DLLEXP void getHistoricalExchangeRate(PnlMat * exchange_mat);

	/**
	* \brief Destructor
	*/
	~Historical();

private:
	PnlMat* market_; /*! Market's spots */
};

#endif	/* SIMULATION_HPP */

