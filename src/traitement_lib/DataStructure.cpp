#include "DataStructure.hpp"

Date::Date()
{
	day_ = 1;
	month_ = 1;
	year_ = 1;
}

Date::~Date(){}

Date::Date(int day, int month, int year) :
day_(day),
month_(month),
year_(year)
{}

DataStructure::DataStructure(int day, int month, int year, double cours, string name, Currency curr) :
cours_(cours),
action_name_(name),
curr_(curr)
{
	Date date = Date(day, month, year);
	date_ = date;
}

DataStructure::DataStructure(Date date, double cours, string name, Currency curr) :
date_(date),
cours_(cours),
action_name_(name),
curr_(curr)
{}

DataStructure::~DataStructure(){}

bool Date::operator==(const Date & d) const
{
	return ((this->day_ == d.day_) && (this->month_ == d.month_) && (this->year_ == d.year_));
}

bool Date::operator>=(const Date & d) const
{
	if (this->year_ != d.year_)
	{
		return (this->year_ > d.year_);
	}
	else if (this->month_ != d.month_)
	{
		return (this->month_ > d.month_);
	}
	else
	{
		return (this->day_ >= d.day_);
	}
}
