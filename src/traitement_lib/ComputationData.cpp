#include "ComputationData.hpp"

void ComputationData::calculParams(vector<vector<DataStructure>*> & result_concat, double* volatilities, double** mat_corr)
{
	EstimationParam *estim = new EstimationParam();
	double NbDays = 252;
	int windowSize = 200;
	PnlMat* mat_covariance = pnl_mat_new();
	PnlMat* corr = pnl_mat_new();
	PnlVect* volatility = pnl_vect_new();

	estim->getCovMatrix(mat_covariance, result_concat, windowSize, NbDays);
	estim->getVolatility(volatility, mat_covariance);
	estim->getCorrelationMatrix(corr, mat_covariance);

	int taille = volatility->size;
	for (int i = 0; i < taille; i++)
	{
		volatilities[i] = GET(volatility, i);
		for (int j = 0; j < taille; j++)
		{
			mat_corr[i][j] = MGET(corr, i, j);
		}
	}

}