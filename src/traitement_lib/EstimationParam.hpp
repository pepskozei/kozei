#ifndef ESTIMATIONPARAM_HPP
#define	ESTIMATIONPARAM_HPP

#include "pnl/pnl_random.h"

#include <vector>

#include "DataStructure.hpp"

#define DLLEXP   __declspec( dllexport )

using namespace std;

class EstimationParam
{
public:
	DLLEXP EstimationParam();
	DLLEXP void getVolatility(PnlVect* vol, PnlMat* data);
	DLLEXP void getCovMatrix(PnlMat* covMatrix, const vector<vector<DataStructure>*> & data, int windowSize, double nbDays);
	DLLEXP void getCorrelationMatrix(PnlMat* corrMatrix, PnlMat* data);
	DLLEXP void getVolatilityIndex(double &vol, PnlMat* data, int indexI);
	DLLEXP void getCorrelationMatrixIndex(double &correlation, PnlMat* data, int indexI, int indexJ);
private:
};

#endif