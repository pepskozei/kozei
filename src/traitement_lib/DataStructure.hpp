#ifndef DATASTRUCTURE_HPP
#define	DATASTRUCTURE_HPP

#include <string>
#include <iostream>

using namespace std;

#define DLLEXP   __declspec( dllexport )

enum class Currency
{
	USD = 0,
	JPY = 1,
	GBP = 2,
	CAD = 3,
	EUR = 4
};

class Date
{
public:
	int day_;
	int month_;
	int year_;

	DLLEXP Date();
	DLLEXP Date(int day, int month, int year);
	DLLEXP ~Date();

	DLLEXP bool operator==(const Date & d) const;
	DLLEXP bool operator>=(const Date & d) const;
};


class DataStructure
{
public:
	Date date_;
	double cours_;
	string action_name_;
	Currency curr_;

	DLLEXP DataStructure(int day, int month, int year, double cours, string name, Currency curr);
	DLLEXP DataStructure(Date date, double cours, string name, Currency curr);
	DLLEXP ~DataStructure();
};

#endif	/* DATASTRUCTURE_HPP */