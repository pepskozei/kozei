#include <iostream>
#include <fstream>

#include "EstimationParam.hpp"

EstimationParam::EstimationParam() {}

void EstimationParam::getVolatility(PnlVect* vol, PnlMat* data)
{
	pnl_vect_resize(vol, data->m);
	for (int i = 0; i < data->m; i++)
	{
		LET(vol, i) = sqrt(MGET(data, i, i));
	}
}

void EstimationParam::getCorrelationMatrix(PnlMat* corrMatrix, PnlMat* data)
{
	pnl_mat_resize(corrMatrix, data->m, data->n);
	for (int i = 0; i < data->m; i++)
	{
		for (int j = 0; j <= i; j++)
		{
			MLET(corrMatrix, i, j) = MGET(data, i, j) / (sqrt(MGET(data, i, i) * MGET(data, j, j)));
			MLET(corrMatrix, j, i) = MGET(corrMatrix, i, j);
		}
	}
}

void EstimationParam::getCovMatrix(PnlMat* covMatrix, const vector<vector<DataStructure>*> & data, int windowSize, double nbDays)
{
	pnl_mat_resize(covMatrix, data.size(), data.size());
	double cov_i_j(0.0), moy_i(0.0), moy_j(0.0);

	for (int i = 0; i < data.size(); i++)
	{
		for (int j = 0; j <= i; j++)
		{
			vector<DataStructure>* current_i = new vector<DataStructure>();
			vector<DataStructure>* current_j = new vector<DataStructure>();
			*current_i = *data[i];
			*current_j = *data[j];

			int index_i = current_i->size() - windowSize ;
			int index_j = current_j->size() - windowSize ;
			
			vector<DataStructure>* subCurrent_i = new vector<DataStructure>();
			vector<DataStructure>* subCurrent_j = new vector<DataStructure>();

			for (int k = index_i; k < current_i->size(); k++)
			{
				subCurrent_i->push_back((*current_i)[k]);
			}
			
			for (int k = index_j; k < current_j->size(); k++)
			{
				subCurrent_j->push_back((*current_j)[k]);
			}
			
			//Calcul de la moyenne pour les deux actifs i et j 
			for (int u = 1; u < subCurrent_i->size(); u++)
			{
				moy_i += log((*subCurrent_i)[u].cours_ / (*subCurrent_i)[u - 1].cours_);
			}
			moy_i /= (windowSize-1) ;

			for (int u = 1; u < subCurrent_j->size(); u++)
			{
				moy_j += log((*subCurrent_j)[u].cours_ / (*subCurrent_j)[u - 1].cours_);
			}
			moy_j /= (windowSize-1) ;

			//Calcul de la cov 
			for (int v = 1; v < windowSize; v++)
			{
				cov_i_j += (log((*subCurrent_j)[v].cours_ / (*subCurrent_j)[v - 1].cours_) - moy_j)*(log((*subCurrent_i)[v].cours_ / (*subCurrent_i)[v - 1].cours_) - moy_i);
			}
			cov_i_j /= (windowSize - 2);
			
			MLET(covMatrix, i, j) = cov_i_j*nbDays;
			MLET(covMatrix, j, i) = cov_i_j*nbDays;

		}
	}
}

void EstimationParam::getVolatilityIndex(double &vol, PnlMat* data, int indexI)
{
	vol = sqrt(MGET(data, indexI, indexI));
}

void EstimationParam::getCorrelationMatrixIndex(double &correlation, PnlMat* data, int indexI, int indexJ)
{
	correlation = MGET(data, indexI, indexJ) / (sqrt(MGET(data, indexI, indexI) * MGET(data, indexJ, indexJ)));
}