#pragma once

#include "RetrieveData.hpp"
#include "EstimationParam.hpp"

#include <iostream>
#include <time.h>

#define DLLEXP   __declspec( dllexport )

namespace ComputationData
{
	DLLEXP void calculParams(vector<vector<DataStructure>*> & result_concat, double* volatilities, double** mat_corr);
}