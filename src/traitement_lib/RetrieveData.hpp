#ifndef RETRIEVEDATA_HPP
#define	RETRIEVEDATA_HPP

#include "pnl/pnl_random.h"

#include "DataStructure.hpp"

#include <vector>
#include <map>

using namespace std;

#define DLLEXP   __declspec( dllexport )

class RetrieveData
{
public:
	DLLEXP RetrieveData();
	DLLEXP ~RetrieveData();

	DLLEXP void getData(vector<vector<DataStructure>*> & spots);
	DLLEXP void getExchangeRate_simple(vector<DataStructure>* exchange_rate, ifstream *fichier, Currency curr);
	DLLEXP void getExchangeRate(vector<vector<DataStructure>*> & exchange_rate);
	DLLEXP void fillAllVects(vector<vector<DataStructure>*> & vectsFilled, const vector<vector<DataStructure>*> & vects);
	DLLEXP void fillAllVectsChange(vector<vector<DataStructure>*> & spotsFilled, vector<vector<DataStructure>*> & ratesFilled,
		const vector<vector<DataStructure>*> & spots, const vector<vector<DataStructure>*> & rates);
	DLLEXP void fillVect(vector<DataStructure> & vectFilled, const vector<DataStructure> & vect, const Date & lastDay);
	DLLEXP void productAllExchange(vector<vector<DataStructure>*> & spotsF, const vector<vector<DataStructure>*> & exRates);
	DLLEXP void productExchange(vector<DataStructure> & spotsF, const vector<DataStructure> & exRate);
	DLLEXP void domesticRates(vector<vector<DataStructure>*> & X, const PnlVect* R);

	DLLEXP void convertDataStructToPnl(PnlMat* mat, const vector<vector<DataStructure>*> & matData);
	DLLEXP void convertDataStructToInterface(double* cours, Date* dates, const vector<vector<DataStructure>*> & matData);
	DLLEXP void getSize(int & size, const vector<vector<DataStructure>*> & matData);

	//utilitaire
	DLLEXP bool findByDate(double & res, const Date & date, const vector<DataStructure> & vect);
	DLLEXP void dateSuiv(Date & date);
	DLLEXP void lastDate(Date & last, const vector<vector<DataStructure>*> & vects);
	DLLEXP void concatVect(vector<vector<DataStructure>*> & concat, const vector<vector<DataStructure>*> & vect1,
		const vector<vector<DataStructure>*> &vect2);
	DLLEXP void createVectDataStrucFromArrays(int type, int taille, int codes[], int** dates, double cours[], vector<vector<DataStructure>*> & spots);

	std::map<string, Currency> actions_devises;
	std::map<int, string> actions_id_code;
	std::map<int, string> actions_id_devise;
};

#endif
