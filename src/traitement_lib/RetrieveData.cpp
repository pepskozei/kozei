#include "RetrieveData.hpp"

#include <iostream>
#include <sstream>
#include <fstream>
#include <algorithm>
#include <vector>
#include <ctime>
#include <chrono>
#include <string>

using namespace std;

RetrieveData::RetrieveData()
{  
	actions_devises.insert(std::pair<string, Currency>("DHR", Currency::USD));
	actions_devises.insert(std::pair<string, Currency>("A", Currency::USD)); 
	actions_devises.insert(std::pair<string, Currency>("ECL", Currency::USD));
	actions_devises.insert(std::pair<string, Currency>("PNR", Currency::USD));
	actions_devises.insert(std::pair<string, Currency>("FLS", Currency::USD));
	actions_devises.insert(std::pair<string, Currency>("ROP", Currency::USD));
	actions_devises.insert(std::pair<string, Currency>("XYL", Currency::USD));
	actions_devises.insert(std::pair<string, Currency>("TMO", Currency::USD));
	actions_devises.insert(std::pair<string, Currency>("EMR", Currency::USD));
	actions_devises.insert(std::pair<string, Currency>("ETN", Currency::USD));
	actions_devises.insert(std::pair<string, Currency>("CMI", Currency::USD));
	actions_devises.insert(std::pair<string, Currency>("CSX", Currency::USD));
	actions_devises.insert(std::pair<string, Currency>("GWR", Currency::USD));
	actions_devises.insert(std::pair<string, Currency>("BWA", Currency::USD));
	actions_devises.insert(std::pair<string, Currency>("DOW", Currency::USD));
	actions_devises.insert(std::pair<string, Currency>("MDLZ", Currency::USD));
	actions_devises.insert(std::pair<string, Currency>("DE", Currency::USD));
	actions_devises.insert(std::pair<string, Currency>("AGCO", Currency::USD));
	actions_devises.insert(std::pair<string, Currency>("K", Currency::USD));
	actions_devises.insert(std::pair<string, Currency>("MOS", Currency::USD));
	actions_devises.insert(std::pair<string, Currency>("CF", Currency::USD));
	actions_devises.insert(std::pair<string, Currency>("P5Y.BE", Currency::USD));

	actions_devises.insert(std::pair<string, Currency>("TYT.L", Currency::JPY));

	actions_devises.insert(std::pair<string, Currency>("UU.L", Currency::GBP));

	actions_devises.insert(std::pair<string, Currency>("CNR.TO", Currency::CAD));

	actions_devises.insert(std::pair<string, Currency>("VIE.PA", Currency::EUR));
	actions_devises.insert(std::pair<string, Currency>("SIE.DE", Currency::EUR));
	actions_devises.insert(std::pair<string, Currency>("SU.PA", Currency::EUR));
	actions_devises.insert(std::pair<string, Currency>("LIN.DE", Currency::EUR));
	actions_devises.insert(std::pair<string, Currency>("VPK.AS", Currency::EUR));

	//Id code pour la creation de vector<vector<DataStructure>*> avec recup des donnees depuis le C#

	actions_id_code.insert(std::pair<int, string>(1,"DHR"));
	actions_id_code.insert(std::pair<int, string>(2, "A"));
	actions_id_code.insert(std::pair<int, string>(3,"ECL"));
	actions_id_code.insert(std::pair<int, string>(4,"PNR"));
	actions_id_code.insert(std::pair<int, string>(5,"FLS"));
	actions_id_code.insert(std::pair<int, string>(6,"ROP"));
	actions_id_code.insert(std::pair<int, string>(7,"XYL"));
	actions_id_code.insert(std::pair<int, string>(8,"TMO"));
	actions_id_code.insert(std::pair<int, string>(9,"EMR"));
	actions_id_code.insert(std::pair<int, string>(10,"ETN"));
	actions_id_code.insert(std::pair<int, string>(11,"CMI"));
	actions_id_code.insert(std::pair<int, string>(12,"CSX"));
	actions_id_code.insert(std::pair<int, string>(13,"GWR"));
	actions_id_code.insert(std::pair<int, string>(14,"BWA"));
	actions_id_code.insert(std::pair<int, string>(15,"DOW"));
	actions_id_code.insert(std::pair<int, string>(16,"MDLZ"));
	actions_id_code.insert(std::pair<int, string>(17,"DE"));
	actions_id_code.insert(std::pair<int, string>(18,"AGCO"));
	actions_id_code.insert(std::pair<int, string>(19,"K"));
	actions_id_code.insert(std::pair<int, string>(20,"MOS"));
	actions_id_code.insert(std::pair<int, string>(21,"CF"));
	actions_id_code.insert(std::pair<int, string>(22,"P5Y.BE"));
	actions_id_code.insert(std::pair<int, string>(23,"TYT.L"));
	actions_id_code.insert(std::pair<int, string>(24,"UU.L"));
	actions_id_code.insert(std::pair<int, string>(25,"CNR.TO"));
	actions_id_code.insert(std::pair<int, string>(26,"VIE.PA"));
	actions_id_code.insert(std::pair<int, string>(27,"SIE.DE"));
	actions_id_code.insert(std::pair<int, string>(28,"SU.PA"));
	actions_id_code.insert(std::pair<int, string>(29,"LIN.DE"));
	actions_id_code.insert(std::pair<int, string>(30,"VPK.AS"));


	actions_id_devise.insert(std::pair<int, string>(1, "USD"));
	actions_id_devise.insert(std::pair<int, string>(2, "JPY"));
	actions_id_devise.insert(std::pair<int, string>(3, "GBP"));
	actions_id_devise.insert(std::pair<int, string>(4, "CAD"));
}


void RetrieveData::getData(vector<vector<DataStructure>*> & spots)
{
	spots.clear();
	vector<DataStructure*> listPrices = vector<DataStructure*>();

	cout << "loading..." << endl;
	ifstream fichier("..\\..\\peps\\RecupData\\bin\\x64\\Debug\\Test_yahoo\\cours.csv", ios::in);  // on ouvre le fichier en lecture

	if (fichier)  // si l'ouverture a réussi
	{
		string contenu;  // déclaration d'une chaîne qui contiendra la ligne lue

		string cours_string;
		string date_string;

		getline(fichier, contenu);

		int i = 0;
		int j = 1;
		vector<DataStructure>* vector_data = new vector<DataStructure>();
		
		string cours_name = "DHR";
		while (getline(fichier, contenu))
		{
			istringstream iss(contenu);

			getline(iss, cours_string, ';');

			if (cours_string != cours_name){
				j++;
				std::reverse(vector_data->begin(), vector_data->end());
				spots.push_back(vector_data);
				vector_data = new vector<DataStructure>();
			}

			cours_name = cours_string;
			int day;
			int month;
			int year;
			double cours;
			Currency curr = actions_devises[cours_name];

			//parcours de la date
			getline(iss, cours_string, ';');

			istringstream iss_date(cours_string);
			getline(iss_date, date_string, '/');
			day = stoi(date_string);
			getline(iss_date, date_string, '/');
			month = stoi(date_string);
			getline(iss_date, date_string, '/');
			year = stoi(date_string);

			while (getline(iss, cours_string, ';'))
			{
				replace(cours_string.begin(), cours_string.end(), ',', '.');
			    cours = stod(cours_string);
			}

			vector_data->push_back(DataStructure(day, month, year, cours, cours_name, curr));
		}
		
		spots.push_back(vector_data);
		fichier.close();  // on ferme le fichier
		
	}
	else  // sinon
	{
		cerr << "Impossible d'ouvrir le fichier !" << endl;
	}
}


void RetrieveData::getExchangeRate_simple(vector<DataStructure>* vector_data, ifstream *fichier, Currency curr)
{
	cout << "loading..." << endl;

	if (*fichier)  // si l'ouverture a réussi
	{
		string contenu;  // déclaration d'une chaîne qui contiendra la ligne lue

		string cours_string;
		string cours_name;
		string date_string;

		getline(*fichier, contenu);

		int i = 0;
		int j = 1;

		while (getline(*fichier, contenu))
		{
			istringstream iss(contenu);

			getline(iss, cours_string, ';');
			cours_name = cours_string;

			int day;
			int month;
			int year;
			double cours;

			//parcours de la date
			getline(iss, cours_string, ';');

			istringstream iss_date(cours_string);
			getline(iss_date, date_string, '/');
			day = stoi(date_string);
			getline(iss_date, date_string, '/');
			month = stoi(date_string);
			getline(iss_date, date_string, '/');
			year = stoi(date_string);

			getline(iss, cours_string, ';');
			replace(cours_string.begin(), cours_string.end(), ',', '.');
			cours = stod(cours_string);
			
			vector_data->push_back(DataStructure(day, month, year, cours, cours_name, curr));
		}

		std::reverse(vector_data->begin(), vector_data->end());
		(*fichier).close();  // on ferme le fichier
	}
	else  // sinon
	{
		cerr << "Impossible d'ouvrir le fichier !" << endl;
	}
}

void RetrieveData::getExchangeRate(vector<vector<DataStructure>*> & exchange_rate)
{
	ifstream file_USD_2014("..\\..\\peps\\RecupData\\bin\\x64\\Debug\\Test_yahoo\\recupFxTop_USD_2014.csv", ios::in);
	ifstream file_USD_2015("..\\..\\peps\\RecupData\\bin\\x64\\Debug\\Test_yahoo\\recupFxTop_USD_2015.csv", ios::in);
	ifstream file_USD_2016("..\\..\\peps\\RecupData\\bin\\x64\\Debug\\Test_yahoo\\recupFxTop_USD_2016.csv", ios::in);

	vector<DataStructure>* vector_data_2014 = new vector<DataStructure>();
	vector<DataStructure>* vector_data_2015 = new vector<DataStructure>();
	vector<DataStructure>* vector_data_2016 = new vector<DataStructure>();
	
	getExchangeRate_simple(vector_data_2014, &file_USD_2014, Currency::USD);
	getExchangeRate_simple(vector_data_2015, &file_USD_2015, Currency::USD);
	getExchangeRate_simple(vector_data_2016, &file_USD_2016, Currency::USD);

	(*vector_data_2014).insert((*vector_data_2014).end(), (*vector_data_2015).begin(), (*vector_data_2015).end());
	(*vector_data_2014).insert((*vector_data_2014).end(), (*vector_data_2016).begin(), (*vector_data_2016).end());

	exchange_rate.push_back(vector_data_2014);

	ifstream file_JPY_2014("..\\..\\peps\\RecupData\\bin\\x64\\Debug\\Test_yahoo\\recupFxTop_JPY_2014.csv", ios::in);
	ifstream file_JPY_2015("..\\..\\peps\\RecupData\\bin\\x64\\Debug\\Test_yahoo\\recupFxTop_JPY_2015.csv", ios::in);
	ifstream file_JPY_2016("..\\..\\peps\\RecupData\\bin\\x64\\Debug\\Test_yahoo\\recupFxTop_JPY_2016.csv", ios::in);

	vector<DataStructure>* vector_data_2014_JPY = new vector<DataStructure>();
	vector<DataStructure>* vector_data_2015_JPY = new vector<DataStructure>();
	vector<DataStructure>* vector_data_2016_JPY = new vector<DataStructure>();

	getExchangeRate_simple(vector_data_2014_JPY, &file_JPY_2014, Currency::JPY);
	getExchangeRate_simple(vector_data_2015_JPY, &file_JPY_2015, Currency::JPY);
	getExchangeRate_simple(vector_data_2016_JPY, &file_JPY_2016, Currency::JPY);

	(*vector_data_2014_JPY).insert((*vector_data_2014_JPY).end(), (*vector_data_2015_JPY).begin(), (*vector_data_2015_JPY).end());
	(*vector_data_2014_JPY).insert((*vector_data_2014_JPY).end(), (*vector_data_2016_JPY).begin(), (*vector_data_2016_JPY).end());

	exchange_rate.push_back(vector_data_2014_JPY);

	ifstream file_GBP_2014("..\\..\\peps\\RecupData\\bin\\x64\\Debug\\Test_yahoo\\recupFxTop_GBP_2014.csv", ios::in);
	ifstream file_GBP_2015("..\\..\\peps\\RecupData\\bin\\x64\\Debug\\Test_yahoo\\recupFxTop_GBP_2015.csv", ios::in);
	ifstream file_GBP_2016("..\\..\\peps\\RecupData\\bin\\x64\\Debug\\Test_yahoo\\recupFxTop_GBP_2016.csv", ios::in);

	vector<DataStructure>* vector_data_2014_GBP = new vector<DataStructure>();
	vector<DataStructure>* vector_data_2015_GBP = new vector<DataStructure>();
	vector<DataStructure>* vector_data_2016_GBP = new vector<DataStructure>();

	getExchangeRate_simple(vector_data_2014_GBP, &file_GBP_2014, Currency::GBP);
	getExchangeRate_simple(vector_data_2015_GBP, &file_GBP_2015, Currency::GBP);
	getExchangeRate_simple(vector_data_2016_GBP, &file_GBP_2016, Currency::GBP);

	(*vector_data_2014_GBP).insert((*vector_data_2014_GBP).end(), (*vector_data_2015_GBP).begin(), (*vector_data_2015_GBP).end());
	(*vector_data_2014_GBP).insert((*vector_data_2014_GBP).end(), (*vector_data_2016_GBP).begin(), (*vector_data_2016_GBP).end());

	exchange_rate.push_back(vector_data_2014_GBP);

	ifstream file_CAD_2014("..\\..\\peps\\RecupData\\bin\\x64\\Debug\\Test_yahoo\\recupFxTop_CAD_2014.csv", ios::in);
	ifstream file_CAD_2015("..\\..\\peps\\RecupData\\bin\\x64\\Debug\\Test_yahoo\\recupFxTop_CAD_2015.csv", ios::in);
	ifstream file_CAD_2016("..\\..\\peps\\RecupData\\bin\\x64\\Debug\\Test_yahoo\\recupFxTop_CAD_2016.csv", ios::in);

	vector<DataStructure>* vector_data_2014_CAD = new vector<DataStructure>();
	vector<DataStructure>* vector_data_2015_CAD = new vector<DataStructure>();
	vector<DataStructure>* vector_data_2016_CAD = new vector<DataStructure>();

	getExchangeRate_simple(vector_data_2014_CAD, &file_CAD_2014, Currency::CAD);
	getExchangeRate_simple(vector_data_2015_CAD, &file_CAD_2015, Currency::CAD);
	getExchangeRate_simple(vector_data_2016_CAD, &file_CAD_2016, Currency::CAD);

	(*vector_data_2014_CAD).insert((*vector_data_2014_CAD).end(), (*vector_data_2015_CAD).begin(), (*vector_data_2015_CAD).end());
	(*vector_data_2014_CAD).insert((*vector_data_2014_CAD).end(), (*vector_data_2016_CAD).begin(), (*vector_data_2016_CAD).end());

	exchange_rate.push_back(vector_data_2014_CAD);
}


void RetrieveData::productAllExchange(vector<vector<DataStructure>*> & spotsF, const vector<vector<DataStructure>*> & exRates)
{
	for (int i = 0; i < spotsF.size(); i++)
	{
		Currency curr = spotsF[i]->at(0).curr_;
		int ind;
		
		if (curr != Currency::EUR)
		{
			switch (curr)
			{
			case Currency::USD:
				ind = 0;
				break;
			case Currency::JPY:
				ind = 1;
				break;
			case Currency::GBP:
				ind = 2;
				break;
			case Currency::CAD:
				ind = 3;
				break;
			}

			productExchange(*(spotsF[i]), *(exRates[ind]));
		}
	}
}

void RetrieveData::productExchange(vector<DataStructure> & spotsF, const vector<DataStructure> & exRate)
{
	for (int i = 0; i < spotsF.size(); i++)
	{
		spotsF[i].cours_ *= exRate[i].cours_;
	}
}

void RetrieveData::domesticRates(vector<vector<DataStructure>*> & X, const PnlVect* R)
{
	double step = 8.0 / 2922.0;
	

	for (int j = 0; j < X.size(); j++)
	{
		double t = 0;

		for (int i = 0; i < X[j]->size(); i++)
		{
			X[j]->at(i).cours_ *= exp(GET(R, j)*t);
			t += step;
		}
		
	}
}

void RetrieveData::fillAllVects(vector<vector<DataStructure>*> & vectsFilled, const vector<vector<DataStructure>*> & vects)
{
	vectsFilled.clear();

	Date last;
	lastDate(last, vects);

	for (int i = 0; i < vects.size(); i++)
	{
		vector<DataStructure>* toFill = new vector<DataStructure>();
		fillVect(*toFill, *(vects[i]), last);
		vectsFilled.push_back(toFill);
	}
}

void RetrieveData::fillAllVectsChange(vector<vector<DataStructure>*> & spotsFilled, vector<vector<DataStructure>*> & ratesFilled,
	const vector<vector<DataStructure>*> & spots, const vector<vector<DataStructure>*> & rates)
{
	spotsFilled.clear();
	ratesFilled.clear();

	Date last;
	Date last2;

	lastDate(last, spots);
	lastDate(last2, rates);

	if (last2 >= last)
	{
		last = last2;
	}

	for (int i = 0; i < spots.size(); i++)
	{
		vector<DataStructure>* toFill = new vector<DataStructure>();
		fillVect(*toFill, *(spots[i]), last);
		spotsFilled.push_back(toFill);
	}

	for (int i = 0; i < rates.size(); i++)
	{
		vector<DataStructure>* toFill = new vector<DataStructure>();
		fillVect(*toFill, *(rates[i]), last);
		ratesFilled.push_back(toFill);
	}
}

void RetrieveData::fillVect(vector<DataStructure> & vectFilled, const vector<DataStructure> & vect, const Date & lastDay)
{
	vectFilled.clear();

	Date cour = Date(11, 7, 2014);
	string name = vect[0].action_name_;
	Currency curr = vect[0].curr_;

	while (lastDay >= cour)
	{
		//si la date est dans le vecteur, on la push
		//sinon, on prend celle d'avant
		double tmp;
		
		if (!findByDate(tmp, cour, vect))
		{
			tmp = vectFilled.back().cours_;
		}
		vectFilled.push_back(DataStructure(cour, tmp, name, curr));

		dateSuiv(cour);
	}
}

bool RetrieveData::findByDate(double & res, const Date & date, const vector<DataStructure> & vect)
{
	for (int i = 0; i < vect.size(); i++)
	{
		if (vect.at(i).date_ == date)
		{
			res = vect.at(i).cours_;
			return true;
		}
	}
	return false;
}

void RetrieveData::dateSuiv(Date & date)
{
	if (date.day_ == 31)
	{
		if (date.month_ == 12)
		{
			date.year_++;
			date.month_ = 1;
		}
		else
		{
			date.month_++;
		}
		date.day_ = 1;
	}
	else if (date.day_ == 30)
	{
		if ((date.month_ == 4) || (date.month_ == 6) || (date.month_ == 9) || (date.month_ == 11))
		{
			date.month_++;
			date.day_ = 1;
		}
		else
		{
			date.day_++;
		}
	}
	else if ((date.day_ == 29) && (date.month_ == 2))
	{
		date.month_++;
		date.day_ = 1;
	}
	else if ((date.day_ == 28) && (date.month_ == 2))
	{
		bool bissextile = (((date.year_ % 4 == 0) && (date.year_ % 100 != 0)) || (date.year_ % 400 == 0));
		if (bissextile)
		{
			date.day_++;
		}
		else
		{
			date.month_++;
			date.day_ = 1;
		}
	}
	else
	{
		date.day_++;
	}
}

void RetrieveData::lastDate(Date & last, const vector<vector<DataStructure>*> & vects)
{
	Date courLast = Date();

	for (int i = 0; i < vects.size(); i++)
	{
		if (vects[i]->back().date_ >= courLast)
		{
			courLast = vects[i]->back().date_;
		}
	}

	last = courLast;
}

void RetrieveData::convertDataStructToPnl(PnlMat* mat, const vector<vector<DataStructure>*> & matData)
{
	pnl_mat_resize(mat, matData[0]->size(), matData.size());

	for (int j = 0; j < matData.size(); j++)
	{
		for (int i = 0; i < matData[j]->size(); i++)
		{
			MLET(mat, i, j) = matData[j]->at(i).cours_;
		}
	}
}

void RetrieveData::getSize(int & size, const vector<vector<DataStructure>*> & matData) 
{
	size = matData[0]->size();
}

void RetrieveData::convertDataStructToInterface(double* cours, Date* dates, const vector<vector<DataStructure>*> & matData)
{
	cours = (double*) malloc(sizeof(double)*matData[0]->size());
	dates = (Date*)malloc(sizeof(Date)*matData[0]->size());

	for (int i = 0; i < matData[0]->size(); i++)
	{
		cours[i] = matData[0]->at(i).cours_;
		dates[i] = matData[0]->at(i).date_;
	}
}

void RetrieveData::concatVect(vector<vector<DataStructure>*> & concat, const vector<vector<DataStructure>*> & vect1,
	const vector<vector<DataStructure>*> &vect2)
{
	concat.clear();

	concat.insert(concat.end(), vect1.begin(), vect1.end());
	concat.insert(concat.end(), vect2.begin(), vect2.end());
}

// le parametre type différencie la creation de DataStructure de cours ou de taux
// cours -> type = 0
// taux -> type = 1
void RetrieveData::createVectDataStrucFromArrays(int type, int taille, int codes[], int** dates, double cours[], vector<vector<DataStructure>*> & spots)
{
	spots.clear();
	vector<DataStructure>* vector_data = new vector<DataStructure>();
	string code_cour = "";
	if (type == 0) {
		code_cour = "DHR";
	}
	else {
		code_cour = "USD";
	}
	
	for (int i = 0; i < taille; i++)
	{
		string code = "";
		Currency curr;
		if (type == 0) {
			code = actions_id_code[codes[i]];
			curr = actions_devises[code];
		}
		else {
			code = actions_id_devise[codes[i]];
			switch (codes[i])
			{
			case 1: {
						curr = Currency::USD;
						break;
			}
			case 2:
			{
					  curr = Currency::JPY;
					  break;
			}
			case 3:
			{
					  curr = Currency::GBP;
					  break;
			}
			case 4:
			{
					  curr = Currency::CAD;
					  break;
			}
			}
		}
		if (code != code_cour)
		{
			spots.push_back(vector_data);
			vector_data = new vector<DataStructure>();
			code_cour = code;
		}
		
		vector_data->push_back(DataStructure(dates[i][0], dates[i][1], dates[i][2], cours[i], code, curr));
	}
	spots.push_back(vector_data);	
}

RetrieveData::~RetrieveData() {}
