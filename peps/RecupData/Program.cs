﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using APIFiMag;
using APIFiMag.Datas;
using APIFiMag.Importer;
using APIFiMag.Exporter;

namespace RecupData
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Currency> curr = new List<Currency>();
            curr.Add(Currency.EUR);
            //curr.Add(Currency.USD);
            //curr.Add(Currency.JPY);
            //curr.Add(Currency.CAD);
            //curr.Add(Currency.BRL);
            //curr.Add(Currency.GBP);

            DataFXTop xchange_USD_2014 = new DataFXTop(Currency.USD,
                                  curr,
                                  new DateTime(2014, 07, 11),
                                  new DateTime(2015, 07, 11),
                                  Frequency.Daily);

            DataFXTop xchange_USD_2015 = new DataFXTop(Currency.USD,
                      curr,
                      new DateTime(2015, 07, 12),
                      new DateTime(2016, 07, 12),
                      Frequency.Daily);

            DataFXTop xchange_USD_2016 = new DataFXTop(Currency.USD,
                      curr,
                      new DateTime(2016, 07, 13),
                      DateTime.Now,
                      Frequency.Daily);

            DataFXTop xchange_GBP_2014 = new DataFXTop(Currency.GBP,
                      curr,
                      new DateTime(2014, 07, 11),
                      new DateTime(2015, 07, 11),
                      Frequency.Daily);

            DataFXTop xchange_GBP_2015 = new DataFXTop(Currency.GBP,
                      curr,
                      new DateTime(2015, 07, 12),
                      new DateTime(2016, 07, 12),
                      Frequency.Daily);

            DataFXTop xchange_GBP_2016 = new DataFXTop(Currency.GBP,
                      curr,
                      new DateTime(2016, 07, 13),
                      DateTime.Now,
                      Frequency.Daily);


            DataFXTop xchange_JPY_2014 = new DataFXTop(Currency.JPY,
                                  curr,
                                  new DateTime(2014, 07, 11),
                                  new DateTime(2015, 07, 11),
                                  Frequency.Daily);

            DataFXTop xchange_JPY_2015 = new DataFXTop(Currency.JPY,
                      curr,
                      new DateTime(2015, 07, 12),
                      new DateTime(2016, 07, 12),
                      Frequency.Daily);

            DataFXTop xchange_JPY_2016 = new DataFXTop(Currency.JPY,
                      curr,
                      new DateTime(2016, 07, 13),
                      DateTime.Now,
                      Frequency.Daily);

            DataFXTop xchange_CAD_2014 = new DataFXTop(Currency.CAD,
                      curr,
                      new DateTime(2014, 07, 11),
                      new DateTime(2015, 07, 11),
                      Frequency.Daily);

            DataFXTop xchange_CAD_2015 = new DataFXTop(Currency.CAD,
                      curr,
                      new DateTime(2015, 07, 12),
                      new DateTime(2016, 07, 12),
                      Frequency.Daily);

            DataFXTop xchange_CAD_2016 = new DataFXTop(Currency.CAD,
                      curr,
                      new DateTime(2016, 07, 13),
                      DateTime.Now,
                      Frequency.Daily);

            xchange_USD_2014.ImportData(new ParserFXTop());
            xchange_USD_2015.ImportData(new ParserFXTop());
            xchange_USD_2016.ImportData(new ParserFXTop());

            xchange_GBP_2014.ImportData(new ParserFXTop());
            xchange_GBP_2015.ImportData(new ParserFXTop());
            xchange_GBP_2016.ImportData(new ParserFXTop());

            xchange_JPY_2014.ImportData(new ParserFXTop());
            xchange_JPY_2015.ImportData(new ParserFXTop());
            xchange_JPY_2016.ImportData(new ParserFXTop());


            xchange_CAD_2014.ImportData(new ParserFXTop());
            xchange_CAD_2015.ImportData(new ParserFXTop());
            xchange_CAD_2016.ImportData(new ParserFXTop());


            xchange_USD_2014.Export(new ExportCSV("Test_yahoo/recupFxTop_USD_2014.csv"));
            xchange_USD_2015.Export(new ExportCSV("Test_yahoo/recupFxTop_USD_2015.csv"));
            xchange_USD_2016.Export(new ExportCSV("Test_yahoo/recupFxTop_USD_2016.csv"));


            xchange_GBP_2014.Export(new ExportCSV("Test_yahoo/recupFxTop_GBP_2014.csv"));
            xchange_GBP_2015.Export(new ExportCSV("Test_yahoo/recupFxTop_GBP_2015.csv"));
            xchange_GBP_2016.Export(new ExportCSV("Test_yahoo/recupFxTop_GBP_2016.csv"));


            xchange_JPY_2014.Export(new ExportCSV("Test_yahoo/recupFxTop_JPY_2014.csv"));
            xchange_JPY_2015.Export(new ExportCSV("Test_yahoo/recupFxTop_JPY_2015.csv"));
            xchange_JPY_2016.Export(new ExportCSV("Test_yahoo/recupFxTop_JPY_2016.csv"));


            xchange_CAD_2014.Export(new ExportCSV("Test_yahoo/recupFxTop_CAD_2014.csv"));
            xchange_CAD_2015.Export(new ExportCSV("Test_yahoo/recupFxTop_CAD_2015.csv"));
            xchange_CAD_2016.Export(new ExportCSV("Test_yahoo/recupFxTop_CAD_2016.csv"));


            List<string> listActions = new List<string>();
            // USD : 22
            listActions.Add("DHR"); //Etats unis
            listActions.Add("A");
            listActions.Add("ECL");
            listActions.Add("PNR"); // suisse PENTAIR remplacement par PNR en USD
            listActions.Add("FLS");
            listActions.Add("ROP");
            listActions.Add("XYL");
            listActions.Add("TMO");
            listActions.Add("EMR");
            listActions.Add("ETN"); // Irlande en USD sur NYSE 
            listActions.Add("CMI");
            listActions.Add("CSX");
            listActions.Add("GWR"); //East Japan Railway
            listActions.Add("BWA");
            listActions.Add("DOW");
            listActions.Add("MDLZ");
            listActions.Add("DE");
            listActions.Add("AGCO");
            listActions.Add("K");
            listActions.Add("MOS");
            listActions.Add("CF");
            listActions.Add("P5Y.BE"); // Brésil -> dollar

            // JPY : 1
            listActions.Add("TYT.L"); // Japon

            // GBP : 1
            listActions.Add("UU.L"); // Royaume -Uni

            // CAD : 1
            listActions.Add("CNR.TO"); //Canada   

            //EUR : 5
            listActions.Add("VIE.PA"); //France- - 
            listActions.Add("SIE.DE"); //Allemagne
            listActions.Add("SU.PA"); //Schneider 
            listActions.Add("LIN.DE");//LINDE
            listActions.Add("VPK.AS"); //Pays Bas -


            List<HistoricalColumn> columns = new List<HistoricalColumn>();
            columns.Add(HistoricalColumn.Close);

            DataActif actif = new DataActif(listActions, columns, new DateTime(2014, 07, 11), DateTime.Now);

            actif.ImportData(new ImportYahoo());
            actif.Export(new ExportCSV("Test_yahoo/cours.csv"));
        }
    }
}
