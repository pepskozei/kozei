﻿using SitePEPS.Controllers;
using SiteWeb.Models;
using SiteWeb.ViewModels;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Wrapper;

using System.Text;
using APIFiMag;
using APIFiMag.Datas;
using APIFiMag.Importer;
using APIFiMag.Exporter;


namespace SiteWeb.Controllers
{
    public class PricingController : Controller
    {

        public ActionResult Index()
        {
            //Recuperation des cours et transformation en tableaux
            //pour pouvoir les passer en parametres d'une fonction du wrapper
            HomeController.RecuperationDonnees();
            int tailleCours = 0;
            foreach (KeyValuePair<string, Actif> entry in Actif.mapNomActif)
            {
                tailleCours += entry.Value.mapCoursDate.Count;
            }
            int[] codes = new int[tailleCours];
            int[][] dates = new int[tailleCours][];
            for (int i = 0; i < tailleCours; i++)
            {
                dates[i] = new int[3];
            }
            double[] cours = new double[tailleCours];
            int compt = 0;
            foreach (KeyValuePair<string, Actif> actif in Actif.mapNomActif)
            {
                foreach (KeyValuePair<DateTime, double> valeur in actif.Value.mapCoursDate)
                {
                    codes[compt] = actif.Value.id;
                    dates[compt][0] = valeur.Key.Day;
                    dates[compt][1] = valeur.Key.Month;
                    dates[compt][2] = valeur.Key.Year;
                    cours[compt] = valeur.Value;
                    compt++;
                }
            }

            //Recuperation des taux de change et transformation en tableaux
            //pour pouvoir les passer en parametres d'une fonction du wrapper
            HomeController.RecuperationTauxChange();
            int tailleTaux = 0;
            foreach (KeyValuePair<string, Devise> entry in Devise.mapNomDevise)
            {
                tailleTaux += entry.Value.mapTauxDate.Count;
            }
            int[] ids_taux = new int[tailleTaux];
            int[][] dates_taux = new int[tailleTaux][];
            for (int i = 0; i < tailleTaux; i++)
            {
                dates_taux[i] = new int[3];
            }
            double[] taux = new double[tailleTaux];
            int compteur = 0;
            foreach (KeyValuePair<string, Devise> devise in Devise.mapNomDevise)
            {
                foreach (KeyValuePair<DateTime, double> valeur in devise.Value.mapTauxDate)
                {
                    ids_taux[compteur] = devise.Value.id;
                    dates_taux[compteur][0] = valeur.Key.Day;
                    dates_taux[compteur][1] = valeur.Key.Month;
                    dates_taux[compteur][2] = valeur.Key.Year;
                    taux[compteur] = valeur.Value;
                    compteur++;
                }
            }

            DonneesDernierRebalancement lastData = new DonneesDernierRebalancement();

            double[] coursActifsCourants = new double[Kozei.nbActifsPlusDevise];
            double[] coursActifsDernierRebal = new double[Kozei.nbActifsPlusDevise];

            WrapperClass wrap = new WrapperClass();

            wrap.tailleCours = tailleCours;
            wrap.tailleTaux = tailleTaux;
            wrap.codes_cours = codes;
            wrap.dates_cours = dates;
            wrap.cours = cours;
            wrap.ids_taux = ids_taux;
            wrap.dates_taux = dates_taux;
            wrap.taux = taux;
            wrap.numJourDernierRebal = lastData.numJourDernierRebal;

            wrap.retrieveCoursActifs(coursActifsCourants, coursActifsDernierRebal);




            double produit_scal_delta_lastSpots = 0;
            for (int i = 0; i < Kozei.nbActifsPlusDevise; i++)
            {
                produit_scal_delta_lastSpots += lastData.delta[i] * coursActifsDernierRebal[i];
            }

            double val_pf = lastData.partSansRisque + produit_scal_delta_lastSpots;

            lastData.lastValPf = val_pf;
            Kozei.lastValPf = val_pf;



            // J'ai calculé mes parametres (volatilité, corrélation) maintenant je les stocke dans chaque Actif
            foreach (KeyValuePair<string, Actif> entry in Actif.mapNomActif)
            {
                entry.Value.cours_courant = coursActifsCourants[entry.Value.id - 1];
                entry.Value.quantité = lastData.delta[entry.Value.id - 1];
            }

            // chaque taux d'interet
            foreach (KeyValuePair<string, ActifSansRisqueEtranger> entry in ActifSansRisqueEtranger.mapNomActifSansRisqueEtranger)
            {
                entry.Value.cours = coursActifsCourants[Kozei.nbActifs + entry.Value.id - 1];
                entry.Value.quantité = lastData.delta[Kozei.nbActifs + entry.Value.id - 1];
            }

            using (IDal dal = new Dal())
            {
                SiteVM siteVM = new SiteVM("", "current_page_item", "", "");
                DateTime depart = new DateTime(2014, 7, 11);
                double nbJour = (double)lastData.numJourDernierRebal;
                // + 1 car on prend les cours de fermeture, on a les données jusqu'au jour nbJour, mais on est le jour nbJour + 1
                DateTime dateDernierRebal = depart.AddDays(nbJour+1);
                siteVM.jourDernierRebal = dateDernierRebal;
                return View(siteVM);
            }
        }


        public ActionResult Price()
        {
            //Recuperation des cours et transformation en tableaux
            //pour pouvoir les passer en parametres d'une fonction du wrapper
            HomeController.RecuperationDonnees();
            int tailleCours = 0;
            foreach (KeyValuePair<string, Actif> entry in Actif.mapNomActif)
            {
                tailleCours += entry.Value.mapCoursDate.Count;
            }
            int[] codes = new int[tailleCours];
            int[][] dates = new int[tailleCours][];
            for (int i = 0; i < tailleCours; i++)
            {
                dates[i] = new int[3];
            }
            double[] cours = new double[tailleCours];
            int compt = 0;
            foreach (KeyValuePair<string, Actif> actif in Actif.mapNomActif)
            {
                foreach (KeyValuePair<DateTime, double> valeur in actif.Value.mapCoursDate)
                {
                    codes[compt] = actif.Value.id;
                    dates[compt][0] = valeur.Key.Day;
                    dates[compt][1] = valeur.Key.Month;
                    dates[compt][2] = valeur.Key.Year;
                    cours[compt] = valeur.Value;
                    compt++;
                }
            }

            //Recuperation des taux de change et transformation en tableaux
            //pour pouvoir les passer en parametres d'une fonction du wrapper
            HomeController.RecuperationTauxChange();
            int tailleTaux = 0;
            foreach (KeyValuePair<string, Devise> entry in Devise.mapNomDevise)
            {
                tailleTaux += entry.Value.mapTauxDate.Count;
            }
            int[] ids_taux = new int[tailleTaux];
            int[][] dates_taux = new int[tailleTaux][];
            for (int i = 0; i < tailleTaux; i++)
            {
                dates_taux[i] = new int[3];
            }
            double[] taux = new double[tailleTaux];
            int compteur = 0;
            foreach (KeyValuePair<string, Devise> devise in Devise.mapNomDevise)
            {
                foreach (KeyValuePair<DateTime, double> valeur in devise.Value.mapTauxDate)
                {
                    ids_taux[compteur] = devise.Value.id;
                    dates_taux[compteur][0] = valeur.Key.Day;
                    dates_taux[compteur][1] = valeur.Key.Month;
                    dates_taux[compteur][2] = valeur.Key.Year;
                    taux[compteur] = valeur.Value;
                    compteur++;
                }
            }


            WrapperClass wrap = new WrapperClass();

            //creation de tableaux qui vont contenir les parametres 
            //estimés apres l'appel de la fonction CalculParam
            double[] vols = new double[Kozei.nbActifsPlusDevise];
            double[][] mat_corr = new double[Kozei.nbActifsPlusDevise][];
            for (int i = 0; i < Kozei.nbActifsPlusDevise; i++)
            {
                mat_corr[i] = new double[Kozei.nbActifsPlusDevise];
            }

            DonneesDernierRebalancement lastData = new DonneesDernierRebalancement();

            wrap.tailleCours = tailleCours;
            wrap.tailleTaux = tailleTaux;
            wrap.codes_cours = codes;
            wrap.dates_cours = dates;
            wrap.cours = cours;
            wrap.ids_taux = ids_taux;
            wrap.dates_taux = dates_taux;
            wrap.taux = taux;


            wrap.getEstimationParam(vols, mat_corr);
            Actif.paramEstimes = true;

            wrap.lastPartSansRisque = lastData.partSansRisque;
            wrap.lastPrice = lastData.lastPrice;
            wrap.lastDelta = lastData.delta;
            wrap.numJourDernierRebal = lastData.numJourDernierRebal;

            double[] coursActifs = new double[Kozei.nbActifsPlusDevise];

            wrap.calculPrice(vols, mat_corr, coursActifs);

            lastData.lastValPf = wrap.getLastValPf();
            Kozei.lastValPf = wrap.getLastValPf();

            DonneesNouveauPricing newData = new DonneesNouveauPricing();
            newData.newPrice = wrap.getPrice();
            newData.newIC = wrap.getIC();
            newData.newValPf = wrap.getNewValPf();


            // J'ai calculé mes parametres (volatilité, corrélation) maintenant je les stocke dans chaque Actif
            foreach (KeyValuePair<string, Actif> entry in Actif.mapNomActif)
            {
                entry.Value.cours_courant = coursActifs[entry.Value.id - 1];
                entry.Value.volatilite = vols[entry.Value.id - 1];
                entry.Value.quantité = lastData.delta[entry.Value.id - 1];
                for (int i = 0; i < Kozei.nbActifsPlusDevise; i++)
                {
                    entry.Value.correlation[i] = mat_corr[entry.Value.id - 1][i];

                }
            }

            // chaque taux d'interet
            foreach (KeyValuePair<string, ActifSansRisqueEtranger> entry in ActifSansRisqueEtranger.mapNomActifSansRisqueEtranger)
            {
                for (int i = 0; i < Kozei.nbActifsPlusDevise; i++)
                {
                    entry.Value.correlation[i] = mat_corr[Kozei.nbActifs + entry.Value.id - 1][i];

                }
                entry.Value.cours = coursActifs[Kozei.nbActifs + entry.Value.id - 1];
                entry.Value.quantité = lastData.delta[Kozei.nbActifs + entry.Value.id - 1];
                entry.Value.volatilite = vols[Kozei.nbActifs + entry.Value.id - 1];
            }

            using (IDal dal = new Dal())
            {
                SiteVM siteVM = new SiteVM("", "current_page_item", "", "");
                DateTime depart = new DateTime(2014, 7, 11);
                double nbJour = (double)lastData.numJourDernierRebal;
                // + 1 car on prend les cours de fermeture, on a les données jusqu'au jour nbJour, mais on est le jour nbJour + 1
                DateTime dateDernierRebal = depart.AddDays(nbJour+1);
                siteVM.jourDernierRebal = dateDernierRebal;
                return View(siteVM);
            }   
        }



        




        public ActionResult Rebalancer()
        {
            //Recuperation des cours et transformation en tableaux
            //pour pouvoir les passer en parametres d'une fonction du wrapper
            HomeController.RecuperationDonnees();
            int tailleCours = 0;
            foreach (KeyValuePair<string, Actif> entry in Actif.mapNomActif)
            {
                tailleCours += entry.Value.mapCoursDate.Count;
            }
            int[] codes = new int[tailleCours];
            int[][] dates = new int[tailleCours][];
            for (int i = 0; i < tailleCours; i++)
            {
                dates[i] = new int[3];
            }
            double[] cours = new double[tailleCours];
            int compt = 0;
            foreach (KeyValuePair<string, Actif> actif in Actif.mapNomActif)
            {
                foreach (KeyValuePair<DateTime, double> valeur in actif.Value.mapCoursDate)
                {
                    codes[compt] = actif.Value.id;
                    dates[compt][0] = valeur.Key.Day;
                    dates[compt][1] = valeur.Key.Month;
                    dates[compt][2] = valeur.Key.Year;
                    cours[compt] = valeur.Value;
                    compt++;
                }
            }

            //Recuperation des taux de change et transformation en tableaux
            //pour pouvoir les passer en parametres d'une fonction du wrapper
            HomeController.RecuperationTauxChange();
            int tailleTaux = 0;
            foreach (KeyValuePair<string, Devise> entry in Devise.mapNomDevise)
            {
                tailleTaux += entry.Value.mapTauxDate.Count;
            }
            int[] ids_taux = new int[tailleTaux];
            int[][] dates_taux = new int[tailleTaux][];
            for (int i = 0; i < tailleTaux; i++)
            {
                dates_taux[i] = new int[3];
            }
            double[] taux = new double[tailleTaux];
            int compteur = 0;
            foreach (KeyValuePair<string, Devise> devise in Devise.mapNomDevise)
            {
                foreach (KeyValuePair<DateTime, double> valeur in devise.Value.mapTauxDate)
                {
                    ids_taux[compteur] = devise.Value.id;
                    dates_taux[compteur][0] = valeur.Key.Day;
                    dates_taux[compteur][1] = valeur.Key.Month;
                    dates_taux[compteur][2] = valeur.Key.Year;
                    taux[compteur] = valeur.Value;
                    compteur++;
                }
            }


            WrapperClass wrap = new WrapperClass();

            //creation de tableaux qui vont contenir les parametres 
            //estimés apres l'appel de la fonction CalculParam
            double[] vols = new double[Kozei.nbActifsPlusDevise];
            double[][] mat_corr = new double[Kozei.nbActifsPlusDevise][];
            for (int i = 0; i < Kozei.nbActifsPlusDevise; i++)
            {
                mat_corr[i] = new double[Kozei.nbActifsPlusDevise];
            }


            double[] newDelta = new double[Kozei.nbActifsPlusDevise];

            DonneesDernierRebalancement lastData = DonneesDernierRebalancement.lastData;

            wrap.tailleCours = tailleCours;
            wrap.tailleTaux = tailleTaux;
            wrap.codes_cours = codes;
            wrap.dates_cours = dates;
            wrap.cours = cours;
            wrap.ids_taux = ids_taux;
            wrap.dates_taux = dates_taux;
            wrap.taux = taux;


            wrap.getEstimationParam(vols, mat_corr);
            Actif.paramEstimes = true;

            wrap.lastPartSansRisque = lastData.partSansRisque;
            wrap.lastPrice = lastData.lastPrice;
            wrap.lastDelta = lastData.delta;
            wrap.numJourDernierRebal = lastData.numJourDernierRebal;
            lastData.lastValPf = Kozei.lastValPf;

            wrap.calculNouvelleCompo(vols, mat_corr, newDelta);

            DonneesNouveauPricing newData = DonneesNouveauPricing.data;
            newData.newDelta = newDelta;
            newData.newNumDernierJourRebal = wrap.newNumDernierJourRebal;
            newData.newPartSansRisqueAvecRebal = wrap.newPartSansRisqueAvecRebal;

            // J'ai calculé mes parametres (volatilité, corrélation) maintenant je les stocke dans chaque Actif
            foreach (KeyValuePair<string, Actif> entry in Actif.mapNomActif)
            {
                entry.Value.volatilite = vols[entry.Value.id - 1];
                entry.Value.quantité = lastData.delta[entry.Value.id - 1];
                entry.Value.quantité_rebal = newData.newDelta[entry.Value.id - 1];
                entry.Value.variation_delta = entry.Value.quantité_rebal - entry.Value.quantité;
                for (int i = 0; i < Kozei.nbActifs; i++)
                {
                    entry.Value.correlation[i] = mat_corr[entry.Value.id - 1][i];

                }
            }

            // chaque taux d'interet
            foreach (KeyValuePair<string, ActifSansRisqueEtranger> entry in ActifSansRisqueEtranger.mapNomActifSansRisqueEtranger)
            {
                entry.Value.quantité = lastData.delta[Kozei.nbActifs + entry.Value.id - 1];
                entry.Value.quantité_rebal = newData.newDelta[Kozei.nbActifs + entry.Value.id - 1];
                entry.Value.variation_delta = entry.Value.quantité_rebal - entry.Value.quantité;
                for (int i = 0; i < Kozei.nbActifsPlusDevise; i++)
                {
                    entry.Value.correlation[i] = mat_corr[Kozei.nbActifs + entry.Value.id - 1][i];

                }
            }

            using (IDal dal = new Dal())
            {
                SiteVM siteVM = new SiteVM("", "current_page_item", "", "");
                DateTime depart = new DateTime(2014, 7, 11);
                double nbJour = (double)lastData.numJourDernierRebal;
                // + 1 car on prend les cours de fermeture, on a les données jusqu'au jour nbJour, mais on est le jour nbJour + 1
                DateTime dateDernierRebal = depart.AddDays(nbJour+1);
                siteVM.jourDernierRebal = dateDernierRebal;
                return View("Couverture",siteVM);
            }
        }



        public ActionResult SauvegarderNouvelleCompo()
        {
            string delta = "";
            for (int i = 0; i < DonneesNouveauPricing.data.newDelta.Count(); i++)
            {
                delta += DonneesNouveauPricing.data.newDelta[i].ToString()+";";
            }
            string line2 = DonneesNouveauPricing.data.newNumDernierJourRebal.ToString() + ";" +
                DonneesNouveauPricing.data.newPartSansRisqueAvecRebal.ToString() + ";" 
                + DonneesNouveauPricing.data.newPrice.ToString() + ";";
            string[] lines = { delta, line2 };

            String path = System.Web.HttpContext.Current.Server.MapPath("download");
            path = Path.Combine(path, "../../Fichiers_Donnees/dataDernierRebalancement.csv");
            System.IO.File.WriteAllLines(path, lines);

            using (IDal dal = new Dal())
            {
                SiteVM siteVM = new SiteVM("", "current_page_item", "", "");
                siteVM.message = "Votre nouvelle composition a bien été sauvegardée.";
                DateTime depart = new DateTime(2014, 7, 11);
                DonneesDernierRebalancement lastData = new DonneesDernierRebalancement();
                double nbJour = (double)lastData.numJourDernierRebal;
                // + 1 car on prend les cours de fermeture, on a les données jusqu'au jour nbJour, mais on est le jour nbJour + 1
                DateTime dateDernierRebal = depart.AddDays(nbJour+1);
                siteVM.jourDernierRebal = dateDernierRebal;
                return View("NouvelleCouverture", siteVM);
            }
        }


        public ActionResult RecupData()
        {

            String path = System.Web.HttpContext.Current.Server.MapPath("download");


            List<Currency> curr = new List<Currency>();
            curr.Add(Currency.EUR);

            DataFXTop xchange_USD_2014 = new DataFXTop(Currency.USD,
                                  curr,
                                  new DateTime(2014, 07, 11),
                                  new DateTime(2014, 12, 31),
                                  Frequency.Daily);

            DataFXTop xchange_USD_2015 = new DataFXTop(Currency.USD,
                                  curr,
                                  new DateTime(2015, 01, 01),
                                  new DateTime(2015, 12, 31),
                                  Frequency.Daily);

            DataFXTop xchange_USD_2016 = new DataFXTop(Currency.USD,
                      curr,
                      new DateTime(2016, 01, 01),
                      new DateTime(2016, 12, 31),
                      Frequency.Daily);

            DataFXTop xchange_USD_2017 = new DataFXTop(Currency.USD,
                      curr,
                      new DateTime(2017, 01, 01),
                      DateTime.Now,
                      Frequency.Daily);

            DataFXTop xchange_GBP_2014 = new DataFXTop(Currency.GBP,
                      curr,
                      new DateTime(2014, 07, 11),
                      new DateTime(2014, 12, 31),
                      Frequency.Daily);

            DataFXTop xchange_GBP_2015 = new DataFXTop(Currency.GBP,
                      curr,
                      new DateTime(2015, 01, 01),
                      new DateTime(2015, 12, 31),
                      Frequency.Daily);

            DataFXTop xchange_GBP_2016 = new DataFXTop(Currency.GBP,
                      curr,
                      new DateTime(2016, 01, 01),
                      new DateTime(2016, 12, 31),
                      Frequency.Daily);

            DataFXTop xchange_GBP_2017 = new DataFXTop(Currency.GBP,
                      curr,
                      new DateTime(2017, 01, 01),
                      DateTime.Now,
                      Frequency.Daily);

            DataFXTop xchange_JPY_2014 = new DataFXTop(Currency.JPY,
                                  curr,
                                  new DateTime(2014, 07, 11),
                                  new DateTime(2014, 12, 31),
                                  Frequency.Daily);

            DataFXTop xchange_JPY_2015 = new DataFXTop(Currency.JPY,
                      curr,
                      new DateTime(2015, 01, 01),
                      new DateTime(2015, 12, 31),
                      Frequency.Daily);

            DataFXTop xchange_JPY_2016 = new DataFXTop(Currency.JPY,
                      curr,
                      new DateTime(2016, 01, 01),
                      new DateTime(2016, 12, 31),
                      Frequency.Daily);

            DataFXTop xchange_JPY_2017 = new DataFXTop(Currency.JPY,
                      curr,
                      new DateTime(2017, 01, 01),
                      DateTime.Now,
                      Frequency.Daily);

            DataFXTop xchange_CAD_2014 = new DataFXTop(Currency.CAD,
                      curr,
                      new DateTime(2014, 07, 11),
                      new DateTime(2014, 12, 31),
                      Frequency.Daily);

            DataFXTop xchange_CAD_2015 = new DataFXTop(Currency.CAD,
                      curr,
                      new DateTime(2015, 01, 01),
                      new DateTime(2015, 12, 31),
                      Frequency.Daily);

            DataFXTop xchange_CAD_2016 = new DataFXTop(Currency.CAD,
                      curr,
                      new DateTime(2016, 01, 01),
                      new DateTime(2016, 12, 31),
                      Frequency.Daily);

            DataFXTop xchange_CAD_2017 = new DataFXTop(Currency.CAD,
                      curr,
                      new DateTime(2017, 01, 01),
                      DateTime.Now,
                      Frequency.Daily);

            xchange_USD_2014.ImportData(new ParserFXTop());
            xchange_USD_2015.ImportData(new ParserFXTop());
            xchange_USD_2016.ImportData(new ParserFXTop());
            xchange_USD_2017.ImportData(new ParserFXTop());

            xchange_GBP_2014.ImportData(new ParserFXTop());
            xchange_GBP_2015.ImportData(new ParserFXTop());
            xchange_GBP_2016.ImportData(new ParserFXTop());
            xchange_GBP_2017.ImportData(new ParserFXTop());

            xchange_JPY_2014.ImportData(new ParserFXTop());
            xchange_JPY_2015.ImportData(new ParserFXTop());
            xchange_JPY_2016.ImportData(new ParserFXTop());
            xchange_JPY_2017.ImportData(new ParserFXTop());

            xchange_CAD_2014.ImportData(new ParserFXTop());
            xchange_CAD_2015.ImportData(new ParserFXTop());
            xchange_CAD_2016.ImportData(new ParserFXTop());
            xchange_CAD_2017.ImportData(new ParserFXTop());

            xchange_USD_2014.Export(new ExportCSV(Path.Combine(path, "../../Fichiers_Donnees/recupFxTop_USD_2014.csv")));
            xchange_USD_2015.Export(new ExportCSV(Path.Combine(path, "../../Fichiers_Donnees/recupFxTop_USD_2015.csv")));
            xchange_USD_2016.Export(new ExportCSV(Path.Combine(path, "../../Fichiers_Donnees/recupFxTop_USD_2016.csv")));
            xchange_USD_2017.Export(new ExportCSV(Path.Combine(path, "../../Fichiers_Donnees/recupFxTop_USD_2017.csv")));

            xchange_GBP_2014.Export(new ExportCSV(Path.Combine(path, "../../Fichiers_Donnees/recupFxTop_GBP_2014.csv")));
            xchange_GBP_2015.Export(new ExportCSV(Path.Combine(path, "../../Fichiers_Donnees/recupFxTop_GBP_2015.csv")));
            xchange_GBP_2016.Export(new ExportCSV(Path.Combine(path, "../../Fichiers_Donnees/recupFxTop_GBP_2016.csv")));
            xchange_GBP_2017.Export(new ExportCSV(Path.Combine(path, "../../Fichiers_Donnees/recupFxTop_GBP_2017.csv")));

            xchange_JPY_2014.Export(new ExportCSV(Path.Combine(path, "../../Fichiers_Donnees/recupFxTop_JPY_2014.csv")));
            xchange_JPY_2015.Export(new ExportCSV(Path.Combine(path, "../../Fichiers_Donnees/recupFxTop_JPY_2015.csv")));
            xchange_JPY_2016.Export(new ExportCSV(Path.Combine(path, "../../Fichiers_Donnees/recupFxTop_JPY_2016.csv")));
            xchange_JPY_2017.Export(new ExportCSV(Path.Combine(path, "../../Fichiers_Donnees/recupFxTop_JPY_2017.csv")));

            xchange_CAD_2014.Export(new ExportCSV(Path.Combine(path, "../../Fichiers_Donnees/recupFxTop_CAD_2014.csv")));
            xchange_CAD_2015.Export(new ExportCSV(Path.Combine(path, "../../Fichiers_Donnees/recupFxTop_CAD_2015.csv")));
            xchange_CAD_2016.Export(new ExportCSV(Path.Combine(path, "../../Fichiers_Donnees/recupFxTop_CAD_2016.csv")));
            xchange_CAD_2017.Export(new ExportCSV(Path.Combine(path, "../../Fichiers_Donnees/recupFxTop_CAD_2017.csv")));

            List<string> listActions = new List<string>();
            // USD : 22
            listActions.Add("DHR"); //Etats unis
            listActions.Add("A");
            listActions.Add("ECL");
            listActions.Add("PNR"); // suisse PENTAIR remplacement par PNR en USD
            listActions.Add("FLS");
            listActions.Add("ROP");
            listActions.Add("XYL");
            listActions.Add("TMO");
            listActions.Add("EMR");
            listActions.Add("ETN"); // Irlande en USD sur NYSE 
            listActions.Add("CMI");
            listActions.Add("CSX");
            listActions.Add("GWR"); //East Japan Railway
            listActions.Add("BWA");
            listActions.Add("DOW");
            listActions.Add("MDLZ");
            listActions.Add("DE");
            listActions.Add("AGCO");
            listActions.Add("K");
            listActions.Add("MOS");
            listActions.Add("CF");
            listActions.Add("P5Y.BE"); // Brésil -> dollar

            // JPY : 1
            listActions.Add("TYT.L"); // Japon

            // GBP : 1
            listActions.Add("UU.L"); // Royaume -Uni

            // CAD : 1
            listActions.Add("CNR.TO"); //Canada   

            //EUR : 5
            listActions.Add("VIE.PA"); //France- - 
            listActions.Add("SIE.DE"); //Allemagne
            listActions.Add("SU.PA"); //Schneider 
            listActions.Add("LIN.DE");//LINDE
            listActions.Add("VPK.AS"); //Pays Bas -


            List<HistoricalColumn> columns = new List<HistoricalColumn>();
            columns.Add(HistoricalColumn.Close);

            DataActif actif = new DataActif(listActions, columns, new DateTime(2014, 07, 11), DateTime.Now);

            actif.ImportData(new ImportYahoo());
            actif.Export(new ExportCSV(Path.Combine(path, "../../Fichiers_Donnees/cours.csv")));


            // Relecture des fichiers
            ActifSansRisqueEtranger.rempli = false;
            Actif.rempli = false;
            Devise.rempli = false;
            HomeController.RecuperationDonnees();
            int tailleCours = 0;
            foreach (KeyValuePair<string, Actif> entry in Actif.mapNomActif)
            {
                tailleCours += entry.Value.mapCoursDate.Count;
            }
            int[] codes = new int[tailleCours];
            int[][] dates = new int[tailleCours][];
            for (int i = 0; i < tailleCours; i++)
            {
                dates[i] = new int[3];
            }
            double[] cours = new double[tailleCours];
            int compt = 0;
            foreach (KeyValuePair<string, Actif> actifs in Actif.mapNomActif)
            {
                foreach (KeyValuePair<DateTime, double> valeur in actifs.Value.mapCoursDate)
                {
                    codes[compt] = actifs.Value.id;
                    dates[compt][0] = valeur.Key.Day;
                    dates[compt][1] = valeur.Key.Month;
                    dates[compt][2] = valeur.Key.Year;
                    cours[compt] = valeur.Value;
                    compt++;
                }
            }

            //Recuperation des taux de change et transformation en tableaux
            //pour pouvoir les passer en parametres d'une fonction du wrapper
            HomeController.RecuperationTauxChange();
            int tailleTaux = 0;
            foreach (KeyValuePair<string, Devise> entry in Devise.mapNomDevise)
            {
                tailleTaux += entry.Value.mapTauxDate.Count;
            }
            int[] ids_taux = new int[tailleTaux];
            int[][] dates_taux = new int[tailleTaux][];
            for (int i = 0; i < tailleTaux; i++)
            {
                dates_taux[i] = new int[3];
            }
            double[] taux = new double[tailleTaux];
            int compteur = 0;
            foreach (KeyValuePair<string, Devise> devise in Devise.mapNomDevise)
            {
                foreach (KeyValuePair<DateTime, double> valeur in devise.Value.mapTauxDate)
                {
                    ids_taux[compteur] = devise.Value.id;
                    dates_taux[compteur][0] = valeur.Key.Day;
                    dates_taux[compteur][1] = valeur.Key.Month;
                    dates_taux[compteur][2] = valeur.Key.Year;
                    taux[compteur] = valeur.Value;
                    compteur++;
                }
            }

            DonneesDernierRebalancement lastData = new DonneesDernierRebalancement();

            double[] coursActifsCourants = new double[Kozei.nbActifsPlusDevise];
            double[] coursActifsDernierRebal = new double[Kozei.nbActifsPlusDevise];

            WrapperClass wrap = new WrapperClass();

            wrap.tailleCours = tailleCours;
            wrap.tailleTaux = tailleTaux;
            wrap.codes_cours = codes;
            wrap.dates_cours = dates;
            wrap.cours = cours;
            wrap.ids_taux = ids_taux;
            wrap.dates_taux = dates_taux;
            wrap.taux = taux;
            wrap.numJourDernierRebal = lastData.numJourDernierRebal;

            wrap.retrieveCoursActifs(coursActifsCourants, coursActifsDernierRebal);




            double produit_scal_delta_lastSpots = 0;
            for (int i = 0; i < Kozei.nbActifsPlusDevise; i++)
            {
                produit_scal_delta_lastSpots += lastData.delta[i] * coursActifsDernierRebal[i];
            }

            double val_pf = lastData.partSansRisque + produit_scal_delta_lastSpots;

            lastData.lastValPf = val_pf;



            // J'ai calculé mes parametres (volatilité, corrélation) maintenant je les stocke dans chaque Actif
            foreach (KeyValuePair<string, Actif> entry in Actif.mapNomActif)
            {
                entry.Value.cours_courant = coursActifsCourants[entry.Value.id - 1];
                entry.Value.quantité = lastData.delta[entry.Value.id - 1];
            }

            // chaque taux d'interet
            foreach (KeyValuePair<string, ActifSansRisqueEtranger> entry in ActifSansRisqueEtranger.mapNomActifSansRisqueEtranger)
            {
                entry.Value.cours = coursActifsCourants[Kozei.nbActifs + entry.Value.id - 1];
                entry.Value.quantité = lastData.delta[Kozei.nbActifs + entry.Value.id - 1];
            }

            using (IDal dal = new Dal())
            {

                SiteVM siteVM = new SiteVM("", "current_page_item", "", "");
                siteVM.message = "Vos données ont bien été récupérées.";
                siteVM.coursRecuperes = true;
                DateTime depart = new DateTime(2014, 7, 11);
                double nbJour = (double)lastData.numJourDernierRebal;
                DateTime dateDernierRebal = depart.AddDays(nbJour+1);
                siteVM.jourDernierRebal = dateDernierRebal;
                return View("Index", siteVM);
            }
        }

        public ActionResult NoRecup()
        {
            String path = Path.Combine(Kozei.path, "..\\Fichiers_Donnees\\cours.csv");
            string[] lines = System.IO.File.ReadAllLines(path);
            string[] tab = lines[1].Split(new string[] { ";" }, StringSplitOptions.None);
            string[] dateDecoupee = tab[1].Split(new string[] { "/" }, StringSplitOptions.None);
            DateTime d = new DateTime(Convert.ToInt16(dateDecoupee[2]), Convert.ToInt16(dateDecoupee[1]), Convert.ToInt16(dateDecoupee[0]));
            using (IDal dal = new Dal())
            {
                SiteVM siteVM = new SiteVM("", "current_page_item", "", "");
                DonneesDernierRebalancement lastData = new DonneesDernierRebalancement();
                lastData.lastValPf = Kozei.lastValPf;
                siteVM.coursRecuperes = true;
                DateTime depart = new DateTime(2014, 7, 11);
                double nbJour = (double)lastData.numJourDernierRebal;
                DateTime dateDernierRebal = depart.AddDays(nbJour);
                siteVM.jourDernierRebal = dateDernierRebal;
                siteVM.message = "Vous avez passé la récupération de données, vous travaillez avec des données allant jusqu'au "+ d.ToString(("dd MMMM yyyy"));
                return View("Index", siteVM);
            }
        }
	}

           
}