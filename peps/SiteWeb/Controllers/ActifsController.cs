﻿using SitePEPS.Controllers;
using SiteWeb.Models;
using SiteWeb.ViewModels;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SiteWeb.Controllers
{
    public class ActifsController : Controller
    {
        //
        // GET: /Actifs/
        public ActionResult Index()
        {
            HomeController.RecuperationDonnees();
            using (IDal dal = new Dal())
            {
                List<Kozei> fonds = dal.getAllKozei();
                SiteVM siteVM = new SiteVM(fonds[0],"", "", "current_page_item", "");
                siteVM.remplirListeActifs();
                ViewBag.ListeDesActifs = new SelectList(siteVM.listeActifs, "code", "nom", "DHR");
                return View(siteVM);
            }
        }

        

        public ActionResult GetSerie()
        {

            HomeController.RecuperationDonnees();
            string nomActif;
            using (IDal dal = new Dal())
            {
                List<Kozei> fonds = dal.getAllKozei();
                nomActif = fonds[0].nomActif;
            }
            var initial = new DateTime(1970, 1, 1);
            var date_cour = new DateTime(1970, 1, 1);
            var value = "";
            var result = "[";
            bool first = true;
            Actif a = Actif.mapNomActif[nomActif];
            foreach (KeyValuePair<DateTime, double> entry in a.mapCoursDate)
            {
                if (first)
                {
                    date_cour = entry.Key;
                    value = entry.Value.ToString(CultureInfo.GetCultureInfo("en-GB"));
                    double millisec = date_cour.ToUniversalTime().Subtract(initial).TotalMilliseconds;
                    result += "[" + millisec + "," + value + "]";
                    first = false;
                }
                else
                {
                    date_cour = entry.Key;
                    value = entry.Value.ToString(CultureInfo.GetCultureInfo("en-GB"));
                    double millisec = date_cour.Subtract(initial).TotalMilliseconds;
                    result += ",[" + millisec + "," + value + "]";
                }

            }
            result += "]";

            return Content(result, "application/json");
        }


        

        public ActionResult AfficherCours()
        {
            using (IDal dal = new Dal())
            {
                if (Request.HttpMethod == "POST")
                {
                    string nom = Request.Form["cd-dropdown"];
                    dal.modifierActifChoisi(1, nom);
                }
                return RedirectToAction("Index");
            }
        }
	}

    
}