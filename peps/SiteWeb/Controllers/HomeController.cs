﻿using System.Web;
using System.Web.Mvc;
using Wrapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.IO;
using System.Net;
using SiteWeb.ViewModels;
using SiteWeb.Models;
using System.Globalization;
using System.Web.UI.HtmlControls;


namespace SitePEPS.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/
        public ActionResult Index()
        {
            using (IDal dal = new Dal())
            {
                Kozei.path = System.Web.HttpContext.Current.Server.MapPath("download");
                SiteVM siteVM = new SiteVM("current_page_item", "", "", "");
                return View(siteVM);
            }           
        }

        public static void creationActifs()
        {
            Actif.mapNomActif = new Dictionary<string, Actif>();
            new Actif(1,"DANAHER", "DHR");
            new Actif(2,"AGILENT TECHNOLOGIES", "A");
            new Actif(3,"ECOLAB", "ECL");
            new Actif(4,"PENTAIR", "PNR");
            new Actif(5,"FLOWSERVE", "FLS");
            new Actif(6,"ROPER INDUSTRIES", "ROP");
            new Actif(7,"XYLEM", "XYL");
            new Actif(8,"THERMO FISHER", "TMO");
            new Actif(9,"EMERSON ELECTRIC", "EMR");
            new Actif(10,"EATON", "ETN");
            new Actif(11,"CUMMINS", "CMI");
            new Actif(12,"CSX", "CSX");
            new Actif(13,"GENESEE & WYOMING INC", "GWR");
            new Actif(14,"BORGWARNER", "BWA");
            new Actif(15,"DOW CHEMICAL", "DOW");
            new Actif(16,"MONDELEZ", "MDLZ");
            new Actif(17,"DEERE & CO", "DE");
            new Actif(18,"AGCO", "AGCO");
            new Actif(19,"KELLOGG", "K");
            new Actif(20,"MOSAIC", "MOS");
            new Actif(21,"CF INDUSTRIES", "CF");
            new Actif(22,"BRF SA", "P5Y.BE");
            new Actif(23, "TOYOTA MOTOR", "TYT.L");
            new Actif(24, "UNITED UTILITIES GROUP", "UU.L");
            new Actif(25, "CANADIAN NATIONAL RAILWAY", "CNR.TO");
            new Actif(26,"VEOLIA ENVIRONNEMENT", "VIE.PA");
            new Actif(27,"SIEMENS", "SIE.DE");
            new Actif(28, "SCHNEIDER ELECTRIC", "SU.PA");   
            new Actif(29,"LINDE", "LIN.DE");
            new Actif(30,"KONINKLIJKE", "VPK.AS");           
        }

        public static void creationDevises()
        {
            Devise.mapNomDevise = new Dictionary<string, Devise>();
            new Devise(1, "USD");
            new Devise(2, "JPY");
            new Devise(3, "GBP");
            new Devise(4, "CAD");
        }

        public static void creationTauxInterets()
        {
            ActifSansRisqueEtranger.mapNomActifSansRisqueEtranger = new Dictionary<string, ActifSansRisqueEtranger>();
            new ActifSansRisqueEtranger(1,"Actif sans risque Etats-Unis",0.0068667);
            new ActifSansRisqueEtranger(2,"Actif sans risque Japon",0.0002429);
            new ActifSansRisqueEtranger(3,"Actif sans risque Grande Bretagne",0.0022500);
            new ActifSansRisqueEtranger(4,"Actif sans risque Canada",0.01891);
        }

        public static void RecuperationDonnees()
        {
            if (!Actif.rempli)
            {
                if (!ActifSansRisqueEtranger.rempli)
                {
                    ActifSansRisqueEtranger.rempli = true;
                    creationTauxInterets();
                }
                // Recupération des cours 
                Actif.rempli = true;
                creationActifs();
                String path = Path.Combine(Kozei.path, "..\\Fichiers_Donnees\\cours.csv");
                string[] lines = System.IO.File.ReadAllLines(path);
                int compteur = 0;

                foreach (string line in lines)
                {
                    if (compteur == 0)
                    {
                        compteur++;
                    }
                    else
                    {
                        // Use a tab to indent each line of the file.
                        string[] tab = line.Split(new string[] { ";" }, StringSplitOptions.None);
                        string[] dateDecoupee = tab[1].Split(new string[] { "/" }, StringSplitOptions.None);
                        DateTime d = new DateTime(Convert.ToInt16(dateDecoupee[2]), Convert.ToInt16(dateDecoupee[1]), Convert.ToInt16(dateDecoupee[0]));
                        string code = tab[0];                        
                        double cours_ = Convert.ToDouble(tab[2]);
                        Actif.mapNomActif[code].ajouterCours(cours_, d);
                    }
                }
            }
        }

        public static void RecuperationTauxChange()
        {
            if (!Devise.rempli)
            {
                // Recupération des cours 
                Devise.rempli = true;
                creationDevises();
                foreach (KeyValuePair<string, Devise> devise in Devise.mapNomDevise)
                {
                    for (int i = 4; i < 8; i++)
                    {
                        String path = System.Web.HttpContext.Current.Server.MapPath("download");
                        path = Path.Combine(path, "../../Fichiers_Donnees/recupFxTop_" + devise.Key + "_201" + i + ".csv");
                        string[] lines = System.IO.File.ReadAllLines(path);
                        int compteur = 0;
                        foreach (string line in lines)
                        {
                            if (compteur == 0)
                            {
                                compteur++;
                            }
                            else
                            {
                                // Use a tab to indent each line of the file.
                                string[] tab = line.Split(new string[] { ";" }, StringSplitOptions.None);
                                string[] dateDecoupee = tab[1].Split(new string[] { "/" }, StringSplitOptions.None);
                                DateTime d = new DateTime(Convert.ToInt16(dateDecoupee[2]), Convert.ToInt16(dateDecoupee[1]), Convert.ToInt16(dateDecoupee[0]));
                                string nom = tab[0];  
                                double taux = Convert.ToDouble(tab[2]);
                                Devise.mapNomDevise[nom].ajouterTaux(taux, d);
                            }
                        }
                    }
                }

                
                
            }
        }

        

        
    }
}