﻿using SitePEPS.Controllers;
using SiteWeb.Models;
using SiteWeb.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Wrapper;

namespace SiteWeb.Controllers
{
    public class DonneesFinancieresController : Controller
    {
        //
        // GET: /DonneesFinancieres/
        public ActionResult Index()
        {
            if (Actif.paramEstimes)
            {
                using (IDal dal = new Dal())
                {
                    List<Kozei> fonds = dal.getAllKozei();
                    SiteVM siteVM = new SiteVM(fonds[0], "", "", "", "current_page_item");
                    siteVM.remplirListeActifs();
                    ViewBag.ListeDesActifs = new SelectList(siteVM.listeActifs, "code", "nom", "DHR");
                    return View(siteVM);
                }
            } else {
                //Recuperation des cours et transformation en tableaux
                //pour pouvoir les passer en parametres d'une fonction du wrapper
                HomeController.RecuperationDonnees();
                int tailleCours = 0;
                foreach (KeyValuePair<string, Actif> entry in Actif.mapNomActif)
                {
                    tailleCours += entry.Value.mapCoursDate.Count;
                }
                int[] codes = new int[tailleCours];
                int[][] dates = new int[tailleCours][];
                for (int i = 0; i < tailleCours; i++)
                {
                    dates[i] = new int[3];
                }
                double[] cours = new double[tailleCours];
                int compt = 0;
                foreach (KeyValuePair<string, Actif> actif in Actif.mapNomActif)
                {
                    foreach (KeyValuePair<DateTime, double> valeur in actif.Value.mapCoursDate)
                    {
                        codes[compt] = actif.Value.id;
                        dates[compt][0] = valeur.Key.Day;
                        dates[compt][1] = valeur.Key.Month;
                        dates[compt][2] = valeur.Key.Year;
                        cours[compt] = valeur.Value;
                        compt++;
                    }
                }

                //Recuperation des taux de change et transformation en tableaux
                //pour pouvoir les passer en parametres d'une fonction du wrapper
                HomeController.RecuperationTauxChange();
                int tailleTaux = 0;
                foreach (KeyValuePair<string, Devise> entry in Devise.mapNomDevise)
                {
                    tailleTaux += entry.Value.mapTauxDate.Count;
                }
                int[] ids_taux = new int[tailleTaux];
                int[][] dates_taux = new int[tailleTaux][];
                for (int i = 0; i < tailleTaux; i++)
                {
                    dates_taux[i] = new int[3];
                }
                double[] taux = new double[tailleTaux];
                int compteur = 0;
                foreach (KeyValuePair<string, Devise> devise in Devise.mapNomDevise)
                {
                    foreach (KeyValuePair<DateTime, double> valeur in devise.Value.mapTauxDate)
                    {
                        ids_taux[compteur] = devise.Value.id;
                        dates_taux[compteur][0] = valeur.Key.Day;
                        dates_taux[compteur][1] = valeur.Key.Month;
                        dates_taux[compteur][2] = valeur.Key.Year;
                        taux[compteur] = valeur.Value;
                        compteur++;
                    }
                }


                WrapperClass wrap = new WrapperClass();

                //creation de tableaux qui vont contenir les parametres 
                //estimés apres l'appel de la fonction CalculParam
                double[] vols = new double[Kozei.nbActifsPlusDevise];
                double[][] mat_corr = new double[Kozei.nbActifsPlusDevise][];
                for (int i = 0; i < Kozei.nbActifsPlusDevise; i++)
                {
                    mat_corr[i] = new double[Kozei.nbActifsPlusDevise];
                }

                DonneesDernierRebalancement lastData = new DonneesDernierRebalancement();

                double[] coursActifsCourants = new double[Kozei.nbActifsPlusDevise];
                double[] coursActifsDernierRebal = new double[Kozei.nbActifsPlusDevise];

                wrap.tailleCours = tailleCours;
                wrap.tailleTaux = tailleTaux;
                wrap.codes_cours = codes;
                wrap.dates_cours = dates;
                wrap.cours = cours;
                wrap.ids_taux = ids_taux;
                wrap.dates_taux = dates_taux;
                wrap.taux = taux;
                wrap.numJourDernierRebal = lastData.numJourDernierRebal;

                wrap.retrieveCoursActifs(coursActifsCourants, coursActifsDernierRebal);

                wrap.getEstimationParam(vols, mat_corr);
                Actif.paramEstimes = true;

                foreach (KeyValuePair<string, Actif> entry in Actif.mapNomActif)
                {
                    entry.Value.cours_courant = coursActifsCourants[entry.Value.id - 1];
                    entry.Value.volatilite = vols[entry.Value.id - 1];
                    for (int i = 0; i < Kozei.nbActifsPlusDevise; i++)
                    {
                        entry.Value.correlation[i] = mat_corr[entry.Value.id - 1][i];

                    }
                }

                // chaque taux d'interet
                foreach (KeyValuePair<string, ActifSansRisqueEtranger> entry in ActifSansRisqueEtranger.mapNomActifSansRisqueEtranger)
                {                    
                    entry.Value.cours = coursActifsCourants[Kozei.nbActifs + entry.Value.id - 1];
                    entry.Value.volatilite = vols[Kozei.nbActifs + entry.Value.id - 1];
                    for (int i = 0; i < Kozei.nbActifsPlusDevise; i++)
                    {
                        entry.Value.correlation[i] = mat_corr[Kozei.nbActifs + entry.Value.id - 1][i];

                    }
                }

                using (IDal dal = new Dal())
                {
                    List<Kozei> fonds = dal.getAllKozei();
                    SiteVM siteVM = new SiteVM(fonds[0], "", "", "", "current_page_item");
                    siteVM.remplirListeActifs();
                    ViewBag.ListeDesActifs = new SelectList(siteVM.listeActifs, "code", "nom", "DHR");
                    return View(siteVM);
                }
            }
            
        }

        public ActionResult Correlation()
        {
            using (IDal dal = new Dal())
            {
                if (Request.HttpMethod == "POST")
                {
                    string nom1 = Request.Form["cd-dropdown1"];
                    string nom2 = Request.Form["cd-dropdown2"];
                    dal.calculerCorrel(1, nom1, nom2);
                }
                return RedirectToAction("Index");
            }
        }
	}
}