﻿using SiteWeb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SiteWeb.ViewModels
{
    public class ActifVM
    {
        public Dictionary<string, Actif> dico { get; set; }
        public List<Models.Actif> listeActifs { get; set; }

        public ActifVM()
        {
            dico = Actif.mapNomActif;
            foreach (var a in dico)
            {
                listeActifs.Add(a.Value);
            }
        }
    }

}