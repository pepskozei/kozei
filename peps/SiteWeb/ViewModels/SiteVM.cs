﻿using SiteWeb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SiteWeb.ViewModels
{
    public class SiteVM
    {
        public Models.Kozei k { get; set; }

        public Models.Menu menu { get; set; }

        public List<Models.Actif> listeActifs { get; set; }

        public string message { get; set; }

        public bool coursRecuperes { get; set; }

        public DateTime jourDernierRebal { get; set; }

        public SiteVM(Kozei koz, string class1, string class2, string class3, string class4)
        {
            k = koz;
            menu = new Menu(class1, class2, class3, class4);
            coursRecuperes = false;
        }

        public SiteVM(string class1, string class2, string class3, string class4)
        {
            menu = new Menu(class1, class2, class3, class4);
            coursRecuperes = false;
        }

        public SiteVM()
        {
        }

        public void remplirListeActifs() 
        {
            listeActifs = new List<Actif>();
            foreach (var a in Actif.mapNomActif)
            {
                listeActifs.Add(a.Value);
            }
        }

    }
}