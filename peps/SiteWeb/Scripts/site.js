﻿function chart_addSerie(chart, data) {
    "use strict";
    var serie = { name: '', data: data };
    serie.marker = {
        enabled: true,
        radius: 2
    };
    chart.addSeries(serie);
}

function chartInitialize(id, url) {

    Highcharts.setOptions({
        lang: {
            months: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
            weekdays: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
            decimalPoint: ',',
            thousandsSep: ''
        },
        credits: { enabled: false }
    });

    $("#" + id).highcharts('StockChart', {
        rangeSelector: { selected: 1 },
        series: null,

        yAxis: [
           { title: { text: "Cours" } },
           {
               gridLineWidth: 0,
               plotLines: [{ color: '#006400', width: 2, value: 0 }]
           },
        ],
        tooltip: {
            crosshairs: { color: 'blue', dashStyle: 'solid' },
        }
    });

    // get and display data
    $.get(url,
        null,
        function (result) {
            chart_addSerie(Highcharts.charts[0], result);
        },
        "json");
}