﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace SiteWeb.Models
{
    public class Actif
    {
        public static bool paramEstimes = false;
        public static bool rempli = false;
        public static Dictionary<string, Actif> mapNomActif { get; set; }

        public Actif()
        {
        }

        public Actif(int i, string n, string c)
        {
            id = i;
            nom = n;
            code = c;
            mapCoursDate = new SortedDictionary<DateTime, double>();
            mapNomActif.Add(c, this);
            correlation = new double[34];
        }

        [Key]
        public string code { get; set; }

        public int id { get; set; }

        public string nom { get; set; }

        public double quantité { get; set; }

        public double quantité_rebal { get; set; }

        public double variation_delta { get; set; }

        public double cours_courant { get; set; }

        public double volatilite { get; set; }

        public double[] correlation { get; set; }

        public SortedDictionary<DateTime, double> mapCoursDate { get; set; }

        public void ajouterCours(double cours, DateTime date)
        {
            mapCoursDate.Add(date, cours);
        }
    }
}