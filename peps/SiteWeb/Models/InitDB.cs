﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web;

namespace SiteWeb.Models
{
    public class InitDB : DropCreateDatabaseAlways<BddContext>
    {
        protected override void Seed(BddContext context)
        {
            context.Fonds.Add(new Kozei(1,100,"DHR"));
            base.Seed(context);
        }
    }
}