﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SiteWeb.Models
{
    public class ActifSansRisqueEtranger
    {
        public static bool rempli = false;
        public static Dictionary<string, ActifSansRisqueEtranger> mapNomActifSansRisqueEtranger { get; set; }

        public ActifSansRisqueEtranger(int i, string d, double v)
        {
            id = i;
            devise = d;
            tauxSansRisque = v;
            mapNomActifSansRisqueEtranger.Add(d, this);
            correlation = new double[34];
        }

        public int id { get; set; }

        public string devise { get; set; }

        public double quantité { get; set; }

        public double quantité_rebal { get; set; }

        public double variation_delta { get; set; }

        public double volatilite { get; set; }

        public double[] correlation { get; set; }

        public double tauxSansRisque { get; set; }

        public double cours { get; set; }
    }
}