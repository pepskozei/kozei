﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SiteWeb.Models
{
    public class Devise
    {
        public static bool rempli = false;
        public static Dictionary<string, Devise> mapNomDevise { get; set; }

        public Devise(int i, string n)
        {
            id = i;
            nom = n;
            mapTauxDate = new SortedDictionary<DateTime, double>();
            mapNomDevise.Add(n, this);
        }

        public int id { get; set; }

        public string nom { get; set; }

        public SortedDictionary<DateTime, double> mapTauxDate { get; set; }

        public void ajouterTaux(double taux, DateTime date)
        {
            mapTauxDate.Add(date, taux);
        }
    
    }
}