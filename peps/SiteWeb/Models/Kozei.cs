﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Web;

namespace SiteWeb.Models
{
    public class Kozei
    {
        public Kozei()
        {
        }

        public Kozei(int idI, double v, string n)
        {
            id = idI;
            valeur_investie = v;
            nomActif = n;
            nbActifs = 30;
            nbActifsPlusDevise = 34;
            correl = 0;
        }

        [Key]
        public int id { get; set; }

        public double valeur_investie { get; set; }

        public string nomActif { get; set; }

        public static int nbActifs { get; set; }

        public static int nbActifsPlusDevise { get; set; }

        public static double lastValPf { get; set; }

        public string correlNom1 { get; set; }

        public string correlNom2 { get; set; }

        public double correl { get; set; }

        public static string path { get; set; }
    }
}