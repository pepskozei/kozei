﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SiteWeb.Models
{
    public class Dal : IDal
    {
        private BddContext bdd;

        public Dal()
        {
            bdd = new BddContext();
        }

        public void CreerKozei(int id, double valI)
        {
            bdd.Fonds.Add(new Kozei(id, valI,""));
            bdd.SaveChanges();
        }

        public List<Kozei> getAllKozei()
        {
            return bdd.Fonds.ToList();
        }


        public void modifierActifChoisi(int idI, string nom)
        {
            Kozei kozeiPremier = bdd.Fonds.FirstOrDefault(k => k.id == idI);
            if (kozeiPremier != null)
            {
                kozeiPremier.nomActif = nom;
                bdd.SaveChanges();
            }
        }

        public void calculerCorrel(int idI, string nom1, string nom2)
        {
            Kozei kozeiPremier = bdd.Fonds.FirstOrDefault(k => k.id == idI);
            if (kozeiPremier != null)
            {
                kozeiPremier.correlNom1 = nom1;
                kozeiPremier.correlNom2 = nom2;
                int id2 = Actif.mapNomActif[nom2].id;
                kozeiPremier.correl = Actif.mapNomActif[nom1].correlation[id2 - 1];
                bdd.SaveChanges();
            }
        }

        public void Dispose()
        {
            bdd.Dispose();
        }
    }
}