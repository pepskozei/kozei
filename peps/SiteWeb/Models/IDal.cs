﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SiteWeb.Models
{
    public interface IDal : IDisposable
    {
        void CreerKozei(int id, double valI);
        List<Kozei> getAllKozei();
        void modifierActifChoisi(int idI, string nom);
        void calculerCorrel(int idI, string nom1, string nom2);
    }
}
