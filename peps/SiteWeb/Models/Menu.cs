﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SiteWeb.Models
{
    public class Menu
    {
        public string classHome { get; set; }
        public string classPricing { get; set; }
        public string classActifs { get; set; }
        public string classDonneesFinancieres { get; set; }

        public Menu(string class1, string class2, string class3, string class4)
        {
            classHome = class1;
            classPricing = class2;
            classActifs = class3;
            classDonneesFinancieres = class4;
        }
    }
}