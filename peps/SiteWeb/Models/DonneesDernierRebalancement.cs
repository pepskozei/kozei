﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace SiteWeb.Models
{
    public class DonneesDernierRebalancement
    {
        public static DonneesDernierRebalancement lastData;

        public DonneesDernierRebalancement() {
            nbActifs = 30;
            nbActifsPlusDevises = 34;
            retrieveLastData();
            lastData = this;
        }

        public int nbActifs  { get; set; }

        public int nbActifsPlusDevises { get; set; }

        public double[] delta { get; set; }

        public double partSansRisque { get; set; }

        public double lastPrice { get; set; }

        public double lastValPf { get; set; }

        public int numJourDernierRebal { get; set; }

        public void retrieveLastData()
        {
            delta = new double[nbActifsPlusDevises];
            String path = System.Web.HttpContext.Current.Server.MapPath("download");
            path = Path.Combine(path, "../../Fichiers_Donnees/dataDernierRebalancement.csv");
            string[] lines = System.IO.File.ReadAllLines(path);
            string[] tab_delta = lines[0].Split(new string[] { ";" }, StringSplitOptions.None);
            for (int i = 0; i < delta.Count(); i++)
            {
                delta[i] = Convert.ToDouble(tab_delta[i]);
            }
            string[] line2 = lines[1].Split(new string[] { ";" }, StringSplitOptions.None);
            numJourDernierRebal = Convert.ToInt16(line2[0]);
            partSansRisque = Convert.ToDouble(line2[1]);
            lastPrice = Convert.ToDouble(line2[2]);
        }
    }
}