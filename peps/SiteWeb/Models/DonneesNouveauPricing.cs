﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SiteWeb.Models
{
    public class DonneesNouveauPricing
    {

        public static DonneesNouveauPricing data;

        public DonneesNouveauPricing()
        {
            data = this;
            newDelta = new double[Kozei.nbActifsPlusDevise];
        }

        public double[] delta { get; set; }

        public double newPandL { get; set; }

        public double newPrice { get; set; }

        public double newIC { get; set; }

        public double newValPf { get; set; }

        public double newNumDernierJourRebal { get; set; }

        public double newPartSansRisqueAvecRebal { get; set; }

        public double[] newDelta { get; set; }
    }
}