﻿using System.Web;
using System.Web.Optimization;

namespace SiteWeb
{
    public class BundleConfig
    {

        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/js").Include(
              "~/Scripts/jquery-2.1.4.min.js",
              "~/Scripts/highstock.js",
              "~/Scripts/site.js",
              "~/Scripts/jquery.dropotron-1.0.js"));

            bundles.Add(new StyleBundle("~/bundles/css").Include(
              "~/Content/style.css",
              "~/Content/styleActifs.css",
              "~/Content/stylePricing.css",
              "~/Content/styleDonneesFinancieres.css"));
        }
    }
}