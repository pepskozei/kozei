﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SiteWeb.Models;
using System.Collections.Generic;

namespace SiteWeb.Tests
{
    [TestClass]
    public class DalTests
    {
        [TestMethod]
        public void CreerKozei()
        {
            using (IDal dal = new Dal())
            {
                dal.CreerKozei(1,10);
                List<Kozei> fonds = dal.getAllKozei();

                Assert.IsNotNull(fonds);
                Assert.AreEqual(1, fonds.Count);
                Assert.AreEqual(10, fonds[0].valeur_investie);
            }
        }
    }
}
