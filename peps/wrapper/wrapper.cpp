#include "stdafx.h"

#include "Wrapper.h"


namespace Wrapper {

	vector<vector<DataStructure>*> WrapperClass::transformDataToVectorDataStruc(RetrieveData* retrieve_data, int type, int taille, array<int>^ codes, array<array<int>^>^ dates, array<double>^ cours)
	{
		vector<vector<DataStructure>*> vect = vector<vector<DataStructure>*>();

		int* codes_ = new int[taille];
		int** dates_ = new int*[taille];
		for (int i = 0; i < taille; i++)
		{
			dates_[i] = new int[3];
		}
		double* cours_ = new double[taille];
		for (int i = 0; i < taille; i++)
		{
			codes_[i] = codes[i];
			for (int j = 0; j < 3; j++)
			{
				dates_[i][j] = dates[i][j];
			}
			cours_[i] = cours[i];
		}

		retrieve_data->createVectDataStrucFromArrays(type, taille, codes_, dates_, cours_, vect);

		return vect;

	}

	void WrapperClass::getEstimationParam(array<double>^ volatilites,
		array<array<double>^>^ mat_corr)
	{
		int nbActifPlusDevise = 34;
		RetrieveData* retrieve_data = new RetrieveData();

		vector<vector<DataStructure>*> vect_cours = transformDataToVectorDataStruc(retrieve_data, 0, tailleCours, codes_cours, dates_cours, cours);
		vector<vector<DataStructure>*> vect_change = transformDataToVectorDataStruc(retrieve_data, 1, tailleTaux, ids_taux, dates_taux, taux);
		vector<vector<DataStructure>*> result_concat = vector<vector<DataStructure>*>();
		retrieve_data->concatVect(result_concat, vect_cours, vect_change);

		double* vols = new double[nbActifPlusDevise];
		double** corr = new double*[nbActifPlusDevise];
		for (int i = 0; i < nbActifPlusDevise; i++)
		{
			corr[i] = new double[nbActifPlusDevise];
		}

		ComputationData::calculParams(result_concat, vols, corr);
		for (int i = 0; i < nbActifPlusDevise; i++) {
			volatilites[i] = vols[i];
			for (int j = 0; j < nbActifPlusDevise; j++) {
				mat_corr[i][j] = corr[i][j];
			}
		}
	}


	void WrapperClass::retrieveCoursActifs(array<double>^ coursActifsCourants, array<double>^ coursActifsDernierRebal)
	{
		int nbActifPlusDevise = 34;
		RetrieveData* retrieve_data = new RetrieveData();

		vector<vector<DataStructure>*> vect_cours = transformDataToVectorDataStruc(retrieve_data, 0, tailleCours, codes_cours, dates_cours, cours);
		vector<vector<DataStructure>*> vect_change = transformDataToVectorDataStruc(retrieve_data, 1, tailleTaux, ids_taux, dates_taux, taux);
		vector<vector<DataStructure>*> result_concat = vector<vector<DataStructure>*>();
		retrieve_data->concatVect(result_concat, vect_cours, vect_change);

		double* coursActifsCour_ = new double[nbActifPlusDevise];
		double* coursActifsDernierReb_ = new double[nbActifPlusDevise];

		Computations::retrieveCoursActifs(numJourDernierRebal, coursActifsCour_, coursActifsDernierReb_, nbActifPlusDevise,
			retrieve_data, &vect_cours, &vect_change);

		for (int i = 0; i < nbActifPlusDevise; i++)
		{
			coursActifsCourants[i] = coursActifsCour_[i];
			coursActifsDernierRebal[i] = coursActifsDernierReb_[i];
		}
	}


	void WrapperClass::calculPrice(array<double>^ vol, array<array<double>^>^ corr, array<double>^ coursActifs)
	{
		int nbActifPlusDevise = 34;
		RetrieveData* retrieve_data = new RetrieveData();

		vector<vector<DataStructure>*> vect_cours = transformDataToVectorDataStruc(retrieve_data, 0, tailleCours, codes_cours, dates_cours, cours);
		vector<vector<DataStructure>*> vect_change = transformDataToVectorDataStruc(retrieve_data, 1, tailleTaux, ids_taux, dates_taux, taux);
		vector<vector<DataStructure>*> result_concat = vector<vector<DataStructure>*>();
		retrieve_data->concatVect(result_concat, vect_cours, vect_change);

		double* vols = new double[nbActifPlusDevise];
		double** corrs = new double*[nbActifPlusDevise];
		for (int i = 0; i < nbActifPlusDevise; i++)
		{
			corrs[i] = new double[nbActifPlusDevise];
		}

		for (int i = 0; i < nbActifPlusDevise; i++)
		{
			vols[i] = vol[i];
			for (int j = 0; j < nbActifPlusDevise; j++) {
				corrs[i][j] = corr[i][j];
			}
		}


		double* deltas = new double[nbActifPlusDevise];
		for (int i = 0; i < nbActifPlusDevise; i++) {
			deltas[i] = lastDelta[i];
		}

		double* coursActifs_ = new double[nbActifPlusDevise];

		double prix_;
		double ic_;
		double val_pf_;
		double val_pf_new_;

		Computations::pricerProduit(numJourDernierRebal, this->getLastPartSansRisque(), this->getLastPrice(), deltas, prix_, ic_,
			val_pf_, val_pf_new_, coursActifs_, nbActifPlusDevise,
			retrieve_data, &vect_cours, &vect_change, vols, corrs);
		this->price = prix_;
		this->confidenceInterval = ic_;
		this->last_val_pf = val_pf_;
		this->val_pf = val_pf_new_;

		for (int i = 0; i < nbActifPlusDevise; i++)
		{
			coursActifs[i] = coursActifs_[i];
		}
	}

	void WrapperClass::calculNouvelleCompo(array<double>^ vol, array<array<double>^>^ corr, array<double>^ newDelta)
	{
		int nbActifPlusDevise = 34;
		RetrieveData* retrieve_data = new RetrieveData();

		vector<vector<DataStructure>*> vect_cours = transformDataToVectorDataStruc(retrieve_data, 0, tailleCours, codes_cours, dates_cours, cours);
		vector<vector<DataStructure>*> vect_change = transformDataToVectorDataStruc(retrieve_data, 1, tailleTaux, ids_taux, dates_taux, taux);
		vector<vector<DataStructure>*> result_concat = vector<vector<DataStructure>*>();
		retrieve_data->concatVect(result_concat, vect_cours, vect_change);

		double* vols = new double[nbActifPlusDevise];
		double** corrs = new double*[nbActifPlusDevise];
		for (int i = 0; i < nbActifPlusDevise; i++)
		{
			corrs[i] = new double[nbActifPlusDevise];
		}

		for (int i = 0; i < nbActifPlusDevise; i++)
		{
			vols[i] = vol[i];
			for (int j = 0; j < nbActifPlusDevise; j++) {
				corrs[i][j] = corr[i][j];
			}
		}


		double* deltas = new double[nbActifPlusDevise];
		for (int i = 0; i < nbActifPlusDevise; i++) {
			deltas[i] = lastDelta[i];
		}

		double* newDelta_ = new double[nbActifPlusDevise];

		//double prix_;
		//double ic_;
		//double val_pf_;
		//double val_pf_new_;
		double newPartSansRisqueAvecRebal;
		int newNumDernierJourRebal;
		//double* delta;
		Computations::calculDelta(numJourDernierRebal, this->getLastPartSansRisque(), this->getLastPrice(), deltas,
			 newNumDernierJourRebal, newPartSansRisqueAvecRebal, nbActifPlusDevise,
			retrieve_data, &vect_cours, &vect_change, vols, corrs, newDelta_);
		//this->price = prix_;
		//this->confidenceInterval = ic_;
		//this->last_val_pf = val_pf_;
		//this->val_pf = val_pf_new_;
		this->newNumDernierJourRebal = newNumDernierJourRebal;
		this->newPartSansRisqueAvecRebal = newPartSansRisqueAvecRebal;
		//this->delta = delta;

		for (int i = 0; i < nbActifPlusDevise; i++)
		{
			newDelta[i] = newDelta_[i];
		}
	}

	

}