#pragma once
#include "Computations.hpp"
#include "ComputationData.hpp"

using namespace System;

namespace Wrapper {

	public ref class WrapperClass
	{
	public:
		double confidenceInterval;
		double price;
		double last_val_pf;
		double val_pf;
		int tailleCours;
		int tailleTaux;
		array<int>^ codes_cours;
		array<array<int>^>^ dates_cours;
		array<double>^ cours;
		array<int>^ ids_taux;
		array<array<int>^>^ dates_taux;
		array<double>^ taux;
		array<double>^ lastDelta;
		double newNumDernierJourRebal;
		double newPartSansRisqueAvecRebal;
		int numJourDernierRebal;
		double lastPrice;
		double lastPartSansRisque;
		int nbActifPlusDevise = 34;

		WrapperClass() { confidenceInterval = price = 0; };
		vector<vector<DataStructure>*> transformDataToVectorDataStruc(RetrieveData* retrieve_data, int type, int taille, array<int>^ codes, array<array<int>^>^ dates, array<double>^ cours);
		void getEstimationParam(array<double>^ volatilites, array<array<double>^>^ mat_corr);
		void retrieveCoursActifs(array<double>^ coursActifsCourants, array<double>^ coursActifsDernierRebal);
		void calculPrice(array<double>^ vol, array<array<double>^>^ corr, array<double>^ coursActifs);
		void calculNouvelleCompo(array<double>^ vol, array<array<double>^>^ corr, array<double>^ newDelta);

		double getPrice() { return price; };
		double getIC() { return confidenceInterval; };
		double getLastValPf() { return last_val_pf; };
		double getNewValPf() { return val_pf; };
		double getLastPrice() { return lastPrice; };
		double getLastPartSansRisque() { return lastPartSansRisque; };
	};
}