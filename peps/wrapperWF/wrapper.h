// wrapper.h

#pragma once

#include "Produit.hpp"

//using namespace System;

namespace wrapper {

	public ref class WrapperClass {

	private:
		double confidenceInterval;
		double prix;
	public:
		WrapperClass() { confidenceInterval = prix = 0; };
		void getPriceCallEuro();
		double getPrix() { return prix; };
		double getIC() { return confidenceInterval; };
		void getSpots();
	};
}
