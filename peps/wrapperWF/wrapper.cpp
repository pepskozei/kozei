// This is the main DLL file.

#include "stdafx.h"
#include "wrapper.h"
#include <iostream>

#include "Kozei.hpp"

//using namespace Sim;
//using namespace Dat;
//using namespace Callth;
//using namespace ProduitNamespace;
using namespace Product;

namespace wrapper {
	void WrapperClass::getPriceCallEuro() {
		double sum = 6;
		double t = 2;
		const PnlMat *past = pnl_mat_create_from_scalar(1, 1, 100);
		double I = 10;
		double nbSamples = 5;
		double nbTimeSteps = 5;
		int size = 1; 
		double maturity = 2;
		double r = 0.05;
		double rho = 0;
		PnlVect * spot = pnl_vect_create(size); 
		PnlVect * sigma = pnl_vect_create(size); 
		Data * d = new Data(nbSamples, nbTimeSteps, size, maturity, r, rho, spot, sigma);
		Produit *pr = new Kozei(d, I);
		double p = pr->getPrice(sum, t);
		this->prix = p;
	}

	void WrapperClass::getSpots(){
		
	}

}
